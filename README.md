# BasicCubicPlatformer

### What is this repository for?

This is a basic platformer made with cubes.

### What is the current state?

- Have a bunch of helper functions for 2D platformer.
- Have a bunch of helper functions brough over from the old Kameosa package.

### How do I get set up?

1. Download Unity and run Unity.
2. Update project to latest Unity version if necessary.
3. Fix compilation errors which might show up in future Unity version.
4. Start coding.