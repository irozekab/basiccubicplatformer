﻿using System;

namespace Libraries
{
    namespace Gameplay
    {
        namespace Interfaces
        {
            public interface IGameplayController
            {
                #region Actions
                event Action OnGameStart;
                event Action OnGamePause;
                event Action OnGameResume;
                event Action OnGameWon;
                event Action OnGameLose;
                event Action OnGameEnd;
                event Action OnGameRestart;
                event Action OnGameQuit;
                #endregion
            }
        }
    }
}
