﻿using System.Collections;
using System.Collections.Generic;
using Libraries.Gameplay.Interfaces;
using UnityEngine;

namespace Libraries
{
    namespace Gameplay
    {
        namespace Abstracts
        {
            public abstract class GameplayEventEnabler : MonoBehaviour
            {
                #region Inspector Variables
                [Header("References")]
                [SerializeField]
                protected IGameplayController gameplayController;
                #endregion

                #region MonoBehaviour Functions
                protected virtual void Awake()
                {
                    if (this.gameplayController == null)
                    {
                        GameObject gameplayGameObject = GameObject.Find("Gameplay");

                        if (gameplayGameObject != null)
                        {
                            this.gameplayController = gameplayGameObject.GetComponent<IGameplayController>();
                        }
                    }
                }

                protected virtual void Start()
                {
                    if (this.gameplayController != null)
                    {
                        this.gameplayController.OnGameStart += OnGameStart;
                        this.gameplayController.OnGamePause += OnGamePause;
                        this.gameplayController.OnGameResume += OnGameResume;
                        this.gameplayController.OnGameWon += OnGameWon;
                        this.gameplayController.OnGameLose += OnGameLose;
                        this.gameplayController.OnGameEnd += OnGameEnd;
                        this.gameplayController.OnGameRestart += OnGameRestart;
                        this.gameplayController.OnGameQuit += OnGameQuit;
                    }

                    //this.enabled = false;
                }

                protected virtual void OnDestroy()
                {
                    if (this.gameplayController != null)
                    {
                        this.gameplayController.OnGameStart -= OnGameStart;
                        this.gameplayController.OnGamePause -= OnGamePause;
                        this.gameplayController.OnGameResume -= OnGameResume;
                        this.gameplayController.OnGameWon -= OnGameWon;
                        this.gameplayController.OnGameLose -= OnGameLose;
                        this.gameplayController.OnGameEnd -= OnGameEnd;
                        this.gameplayController.OnGameRestart -= OnGameRestart;
                        this.gameplayController.OnGameQuit -= OnGameQuit;
                    }
                }
                #endregion

                #region Listeners
                protected virtual void OnGameStart()
                {
                    this.enabled = true;
                }

                protected virtual void OnGamePause()
                {
                    this.enabled = false;
                }

                protected virtual void OnGameResume()
                {
                    this.enabled = true;
                }

                protected virtual void OnGameWon()
                {
                }

                protected virtual void OnGameLose()
                {
                }

                protected virtual void OnGameEnd()
                {
                    this.enabled = false;
                }

                protected virtual void OnGameRestart()
                {
                }

                protected virtual void OnGameQuit()
                {
                    this.enabled = false;
                }
                #endregion
            }
        }
    }
}
