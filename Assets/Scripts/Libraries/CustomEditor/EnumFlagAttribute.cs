﻿using UnityEngine;

namespace Libraries
{
    namespace CustomEditor
    {
        public class EnumFlagAttribute : PropertyAttribute
        {
            public string enumName;

            public EnumFlagAttribute()
            {
            }

            public EnumFlagAttribute(string enumName)
            {
                this.enumName = enumName;
            }
        }
    }
}