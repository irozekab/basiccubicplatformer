﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Libraries
{
    namespace CustomEditor
    {
        [CustomPropertyDrawer(typeof(EnumFlagAttribute))]
        public class EnumFlagDrawer : PropertyDrawer
        {
            public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                EnumFlagAttribute flagSettings = attribute as EnumFlagAttribute;
                Enum targetEnum = GetBaseProperty<Enum>(property);

                string propertyName = flagSettings.enumName;

                if (string.IsNullOrEmpty(propertyName))
                {
                    propertyName = property.name;
                }

                EditorGUI.BeginProperty(position, label, property);
                Enum enumNew = EditorGUI.EnumFlagsField(position, propertyName, targetEnum);
                property.intValue = (int)Convert.ChangeType(enumNew, targetEnum.GetType());
                EditorGUI.EndProperty();
            }

            static T GetBaseProperty<T>(SerializedProperty property)
            {
                /// Separate the steps it takes to get to this property
                string[] separatedPaths = property.propertyPath.Split('.');

                /// Go down to the root of this serialized property
                System.Object reflectionTarget = property.serializedObject.targetObject as object;

                /// Walk down the path to get the target object
                foreach (var path in separatedPaths)
                {
                    FieldInfo fieldInfo = reflectionTarget.GetType().GetField(path, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    reflectionTarget = fieldInfo.GetValue(reflectionTarget);
                }

                return (T)reflectionTarget;
            }
        }
    }
}
