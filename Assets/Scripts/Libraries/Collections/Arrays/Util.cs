﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Libraries
{
    namespace Collections
    {
        namespace Arrays
        {
            public static class Util
            {
                /// <summary>
                /// Same as Array.Exists but allow user to set the start and end index.
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="array"></param>
                /// <param name="match"></param>
                /// <param name="start"></param>
                /// <param name="end">i</param>
                /// <returns></returns>
                public static bool Exists<T>(T[] array, Predicate<T> match, int start, int end)
                {
                    for (int i = start; i < end; i++)
                    {
                        if (match(array[i]))
                        {
                            return true;
                        }
                    }

                    return false;
                }
            }
        }
    }
}
