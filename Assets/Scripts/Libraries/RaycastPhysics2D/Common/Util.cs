﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Common
        {
            public static class Util
            {
                /// <summary>
                /// Casts a box into the Scene, returning colliders that contact with it into the provided results array.
                /// This is a wrapper around Physics2D.BoxCastNonAlloc() that will automatically increase size of the vector provided when the size exceeds what was initially allocated.
                /// The size will be set to double the hitCounto.
                /// </summary>
                /// <param name="origin"></param>
                /// <param name="size"></param>
                /// <param name="angle"></param>
                /// <param name="direction"></param>
                /// <param name="results"></param>
                /// <param name="distance"></param>
                /// <param name="layerMask"></param>
                /// <param name="minDepth"></param>
                /// <param name="maxDepth"></param>
                /// <returns></returns>
                public static int BoxCastNonAllocAutoResize(UnityEngine.Vector2 origin, UnityEngine.Vector2 size, float angle, UnityEngine.Vector2 direction, RaycastHit2D[] results, float distance = Mathf.Infinity, int layerMask = Physics.DefaultRaycastLayers, float minDepth = -Mathf.Infinity, float maxDepth = Mathf.Infinity)
                {
                    int hitCount = Physics2D.BoxCastNonAlloc(origin, size, angle, direction, results, distance, layerMask, minDepth, maxDepth);

                    if (hitCount > results.Length)
                    {
                        results = new RaycastHit2D[(int)(hitCount * 2)];

                        hitCount = Physics2D.BoxCastNonAlloc(origin, size, angle, direction, results, distance, layerMask, minDepth, maxDepth);
                    }

                    return hitCount;
                }

                public static int RaycastNonAllocHitsAutoResize(UnityEngine.Vector2 origin, UnityEngine.Vector2 direction, RaycastHit2D[] results, float distance = Mathf.Infinity, int layerMask = Physics2D.DefaultRaycastLayers, float minDepth = -Mathf.Infinity, float maxDepth = Mathf.Infinity)
                {
                    int hitCount = Physics2D.RaycastNonAlloc(origin, direction, results, distance, layerMask, minDepth, maxDepth);

                    if (hitCount > results.Length)
                    {
                        results = new RaycastHit2D[(int)(hitCount * 2)];

                        hitCount = Physics2D.RaycastNonAlloc(origin, direction, results, distance, layerMask, minDepth, maxDepth);
                    }

                    return hitCount;
                }

                public static int OverlapAreaNonAllocAutoResize(UnityEngine.Vector2 pointA, UnityEngine.Vector2 pointB, Collider2D[] results, int layerMask = Physics2D.DefaultRaycastLayers, float minDepth = -Mathf.Infinity, float maxDepth = Mathf.Infinity)
                {
                    int hitCount = Physics2D.OverlapAreaNonAlloc(pointA, pointB, results, layerMask, minDepth, maxDepth);

                    if (hitCount > results.Length)
                    {
                        results = new Collider2D[(int)(hitCount * 2)];

                        hitCount = Physics2D.OverlapAreaNonAlloc(pointA, pointB, results, layerMask, minDepth, maxDepth);
                    }

                    return hitCount;
                }
            }
        }
    }
}
