﻿using System;
using Libraries.RaycastPhysics2D.Enumerables;
using UnityEngine;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace States
        {
            [Serializable]
            public struct JumpState
            {
                #region Private Variables
                [SerializeField]
                private bool isPressed;
                private bool isHeld;
                private int airJumpCount;

                private bool isForce;
                private float height;

                private float heightJumped;

                private JumpType currentJumpType;

                private int currentJumpTypeAllowedGraceFramesRemaining;
                private int currentJumpTypeAllowedGraceFramesRemainingResetValue;
                private int jumpActivatedGraceFramesRemaining;
                private int jumpActivatedGraceFramesRemainingResetValue;
                private int maxJumpHeightFramesRemaining;
                private int noMovementAfterJumpGraceFramesRemaining;
                private int noMovementAfterJumpGraceFramesRemainingResetValue;
                #endregion

                #region Properties
                /// <summary>
                /// On down. The initial activationg.
                /// </summary>
                public bool IsPressed
                {
                    get
                    {
                        return this.isPressed;
                    }

                    set
                    {
                        this.isPressed = value;
                    }
                }

                /// <summary>
                /// Means holding the button.
                /// </summary>
                public bool IsHeld
                {
                    get
                    {
                        return this.isHeld;
                    }

                    set
                    {
                        this.isHeld = value;
                    }
                }

                public int AirJumpCount
                {
                    get
                    {
                        return this.airJumpCount;
                    }

                    set
                    {
                        this.airJumpCount = value;
                    }
                }

                public bool IsForce
                {
                    get
                    {
                        return this.isForce;
                    }

                    set
                    {
                        this.isForce = value;
                    }
                }

                public float Height
                {
                    get
                    {
                        return this.height;
                    }

                    set
                    {
                        this.height = value;
                    }
                }

                public JumpType CurrentJumpType
                {
                    get
                    {
                        return this.currentJumpType;
                    }

                    set
                    {
                        if (value != JumpType.None)
                        {
                            this.currentJumpTypeAllowedGraceFramesRemaining = this.currentJumpTypeAllowedGraceFramesRemainingResetValue;
                        }
                        else
                        {
                            this.currentJumpTypeAllowedGraceFramesRemaining = -1;
                        }

                        this.currentJumpType = value;
                    }
                }

                public float HeightJumped
                {
                    get
                    {
                        return this.heightJumped;
                    }

                    set
                    {
                        this.heightJumped = value;
                    }
                }

                /// <summary>
                /// The grace period once the motor is told to jump where it will jump.
                /// How long the motor should remember that Jump() was called and activate a jump if it becomes valid in that time. 
                /// This means that a player could press jump before they actually hit the ground and the motor will allow it to occur.
                /// </summary>
                public int JumpActivatedGraceFramesRemaining
                {
                    get
                    {
                        return this.jumpActivatedGraceFramesRemaining;
                    }

                    set
                    {
                        this.jumpActivatedGraceFramesRemaining = value;
                    }
                }

                public int JumpActivatedGraceFramesRemainingResetValue
                {
                    get
                    {
                        return this.jumpActivatedGraceFramesRemainingResetValue;
                    }

                    set
                    {
                        this.jumpActivatedGraceFramesRemainingResetValue = value;
                    }
                }

                /// <summary>
                /// The amount of time once the motor has left an environment that a jump will be allowed.
                /// </summary>
                public int CurrentJumpTypeAllowedGraceFramesRemaining
                {
                    get
                    {
                        return this.currentJumpTypeAllowedGraceFramesRemaining;
                    }

                    set
                    {
                        this.currentJumpTypeAllowedGraceFramesRemaining = value;
                    }
                }

                public int CurrentJumpTypeAllowedGraceFramesRemainingResetValue
                {
                    get
                    {
                        return this.currentJumpTypeAllowedGraceFramesRemainingResetValue;
                    }

                    set
                    {
                        this.currentJumpTypeAllowedGraceFramesRemainingResetValue = value;
                    }
                }

                public int MaxJumpHeightFramesRemaining
                {
                    get
                    {
                        return this.maxJumpHeightFramesRemaining;
                    }

                    set
                    {
                        this.maxJumpHeightFramesRemaining = value;
                    }
                }

                public int NoMovementAfterJumpGraceFramesRemaining
                {
                    get
                    {
                        return this.noMovementAfterJumpGraceFramesRemaining;
                    }

                    set
                    {
                        this.noMovementAfterJumpGraceFramesRemaining = value;
                    }
                }

                public int NoMovementAfterJumpGraceFramesRemainingResetValue
                {
                    get
                    {
                        return this.noMovementAfterJumpGraceFramesRemainingResetValue;
                    }

                    set
                    {
                        this.noMovementAfterJumpGraceFramesRemainingResetValue = value;
                    }
                }
                #endregion
            }
        }
    }
}
