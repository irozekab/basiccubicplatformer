﻿using Libraries.RaycastPhysics2D.Components;
using Libraries.RaycastPhysics2D.Enumerables;
using UnityEngine;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace States
        {
            public struct MovingPlatformState
            {
                #region Private Variables
                private MovingPlatformRaycastPhysicsComponent platform;

                private UnityEngine.Vector2 previousPosition;
                private CollidedSurfaces stuckToWall;
                #endregion

                #region Properties
                public MovingPlatformRaycastPhysicsComponent Platform
                {
                    get
                    {
                        return this.platform;
                    }

                    set
                    {
                        this.platform = value;
                    }
                }

                public UnityEngine.Vector2 PreviousPosition
                {
                    get
                    {
                        return this.previousPosition;
                    }

                    set
                    {
                        this.previousPosition = value;
                    }
                }

                public CollidedSurfaces StuckToWall
                {
                    get
                    {
                        return this.stuckToWall;
                    }

                    set
                    {
                        this.stuckToWall = value;
                    }
                }

                public bool IsOnPlatform
                {
                    get
                    {
                        return this.platform != null;
                    }
                }
                #endregion
            }
        }
    }
}
