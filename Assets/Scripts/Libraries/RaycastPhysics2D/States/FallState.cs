﻿namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace States
        {
            public struct FallState
            {
                #region Private Variables
                /// <summary>
                /// Returns the height the motor has fallen. Includes fallen fast distance.
                /// </summary>
                private float heightFallen;

                /// <summary>
                /// Returns the height the motor has fallen fast.
                /// </summary>
                private float heightFastFallen;
                #endregion

                #region Properties
                public float HeightFallen
                {
                    get
                    {
                        return this.heightFallen;
                    }

                    set
                    {
                        this.heightFallen = value;
                    }
                }

                public float HeightFastFallen
                {
                    get
                    {
                        return this.heightFastFallen;
                    }

                    set
                    {
                        this.heightFastFallen = value;
                    }
                }
                #endregion
            }
        }
    }
}
