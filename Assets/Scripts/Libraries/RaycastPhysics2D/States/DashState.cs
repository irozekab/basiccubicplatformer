﻿using System;
using UnityEngine;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace States
        {
            [Serializable]
            public struct DashState
            {
                #region Private Variables
                [SerializeField]
                private bool isPressed;
                private bool isForceDirection;
                private UnityEngine.Vector2 direction;// = UnityEngine.Vector2.zero;
                private float distanceCalculated;
                private float distanceDashed;
                private bool isForce;

                private int cooldownFramesRemaining;
                private int cooldownFramesRemainingResetValue;
                [SerializeField]
                private int dashingFramesRemaining;
                private int dashingFramesRemainingResetValue;
                private int noGravityFramesRemaining;
                private int noGravityFramesRemainingResetValue;
                #endregion

                #region Properties
                public bool IsPressed
                {
                    get
                    {
                        return this.isPressed;
                    }

                    set
                    {
                        this.isPressed = value;
                    }
                }

                public bool IsForceDirection
                {
                    get
                    {
                        return this.isForceDirection;
                    }

                    set
                    {
                        this.isForceDirection = value;
                    }
                }

                public UnityEngine.Vector2 Direction
                {
                    get
                    {
                        return this.direction;
                    }

                    set
                    {
                        this.direction = value;
                    }
                }

                public float DistanceCalculated
                {
                    get
                    {
                        return this.distanceCalculated;
                    }

                    set
                    {
                        this.distanceCalculated = value;
                    }
                }

                public float DistanceDashed
                {
                    get
                    {
                        return this.distanceDashed;
                    }

                    set
                    {
                        this.distanceDashed = value;
                    }
                }

                public bool IsForce
                {
                    get
                    {
                        return this.isForce;
                    }

                    set
                    {
                        this.isForce = value;
                    }
                }

                public int CooldownFramesRemaining
                {
                    get
                    {
                        return this.cooldownFramesRemaining;
                    }

                    set
                    {
                        this.cooldownFramesRemaining = value;
                    }
                }

                public int CooldownFramesRemainingResetValue
                {
                    get
                    {
                        return this.cooldownFramesRemainingResetValue;
                    }

                    set
                    {
                        this.cooldownFramesRemainingResetValue = value;
                    }
                }

                public int DashingFramesRemaining
                {
                    get
                    {
                        return this.dashingFramesRemaining;
                    }

                    set
                    {
                        this.dashingFramesRemaining = value;
                    }
                }

                public int DashingFramesRemainingResetValue
                {
                    get
                    {
                        return this.dashingFramesRemainingResetValue;
                    }

                    set
                    {
                        this.dashingFramesRemainingResetValue = value;
                    }
                }

                public int NoGravityFramesRemaining
                {
                    get
                    {
                        return this.noGravityFramesRemaining;
                    }

                    set
                    {
                        this.noGravityFramesRemaining = value;
                    }
                }

                public int NoGravityFramesRemainingResetValue
                {
                    get
                    {
                        return this.noGravityFramesRemainingResetValue;
                    }

                    set
                    {
                        this.noGravityFramesRemainingResetValue = value;
                    }
                }
                #endregion
            }
        }
    }
}
