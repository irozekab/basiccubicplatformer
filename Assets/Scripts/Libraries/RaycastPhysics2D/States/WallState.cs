﻿namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace States
        {
            public struct WallState
            {
                #region Private Variables
                private bool canWallStick;// = true;

                private int wallStickFramesRemaining;
                private int wallStickFramesRemainingResetValue;
                private int wallInteractionCooldownFramesRemaining;
                #endregion

                #region Properties
                public bool CanWallStick
                {
                    get
                    {
                        return this.canWallStick;
                    }

                    set
                    {
                        this.canWallStick = value;
                    }
                }

                public int WallStickFramesRemaining
                {
                    get
                    {
                        return this.wallStickFramesRemaining;
                    }

                    set
                    {
                        this.wallStickFramesRemaining = value;
                    }
                }

                public int WallStickFramesRemainingResetValue
                {
                    get
                    {
                        return this.wallStickFramesRemainingResetValue;
                    }

                    set
                    {
                        this.wallStickFramesRemainingResetValue = value;
                    }
                }

                public int WallInteractionCooldownFramesRemaining
                {
                    get
                    {
                        return this.wallInteractionCooldownFramesRemaining;
                    }

                    set
                    {
                        this.wallInteractionCooldownFramesRemaining = value;
                    }
                }
                #endregion
            }
        }
    }
}
