﻿using System;
using Libraries.CustomEditor;
using Libraries.RaycastPhysics2D.Enumerables;
using UnityEngine;
using Libraries.RaycastPhysics2D.Interfaces;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace States
        {
            [Serializable]
            public struct SurroundingState
            {
                #region Private Variables
                /// <summary>
                /// The normal of the slope the motor is on. Facing into.
                /// This value doesn't have meaning unless isOnSlope is true.
                /// </summary>
                [SerializeField]
                private UnityEngine.Vector2 slopeNormal;

                /// <summary>
                /// The direction up/down the slope.
                /// </summary>
                private UnityEngine.Vector2 slopeDirection;

                private float maxDotAllowedForSlopes;

                [EnumFlag]
                [SerializeField]
                private CollidedSurfaces collidedSurfaces;

                [SerializeField]
                private RaycastHit2D hitUp;
                [SerializeField]
                private RaycastHit2D hitRight;
                [SerializeField]
                private RaycastHit2D hitDown;
                [SerializeField]
                private RaycastHit2D hitLeft;

                [SerializeField]
                private HostController host;
                private UnityEngine.Vector2 hostPreviousPosition;
                #endregion

                #region Properties
                public CollidedSurfaces CollidedSurfaces
                {
                    get
                    {
                        return this.collidedSurfaces;
                    }

                    set
                    {
                        this.collidedSurfaces = value;
                    }
                }

                public RaycastHit2D HitUp
                {
                    get
                    {
                        return this.hitUp;
                    }

                    set
                    {
                        this.hitUp = value;
                    }
                }

                public RaycastHit2D HitRight
                {
                    get
                    {
                        return this.hitRight;
                    }

                    set
                    {
                        this.hitRight = value;
                    }
                }

                public RaycastHit2D HitDown
                {
                    get
                    {
                        return this.hitDown;
                    }

                    set
                    {
                        this.hitDown = value;
                    }
                }

                public RaycastHit2D HitLeft
                {
                    get
                    {
                        return this.hitLeft;
                    }

                    set
                    {
                        this.hitLeft = value;
                    }
                }

                public bool IsOnSlope
                {
                    get
                    {
                        return IsCollidedWithSurface(CollidedSurfaces.SlopeLeft) || IsCollidedWithSurface(CollidedSurfaces.SlopeRight);
                    }
                }


                ///<summary>
                /// Is the motor on a slope that is too steep?
                ///</summary>
                public bool IsSlopeTooSteep
                {
                    get
                    {
                        return IsOnSlope && UnityEngine.Vector3.Dot(UnityEngine.Vector3.up, this.slopeNormal) < this.maxDotAllowedForSlopes;
                    }
                }

                public UnityEngine.Vector2 SlopeNormal
                {
                    get
                    {
                        return IsOnSlope ? this.slopeNormal : UnityEngine.Vector2.zero;
                    }

                    set
                    {
                        if (this.slopeNormal != value)
                        {
                            this.slopeNormal = value;
                            this.slopeDirection = new UnityEngine.Vector2(Mathf.Sign(this.slopeNormal.x) * this.slopeNormal.y, -Mathf.Abs(this.slopeNormal.x));
                        }
                    }
                }

                public UnityEngine.Vector2 SlopeDirection
                {
                    get
                    {
                        if (!IsOnSlope)
                        {
                            return UnityEngine.Vector2.zero;
                        }

                        return this.slopeDirection;
                    }
                }

                public float MaxDotAllowedForSlopes
                {
                    get
                    {
                        return this.maxDotAllowedForSlopes;
                    }

                    set
                    {
                        this.maxDotAllowedForSlopes = value;
                    }
                }

                ///<summary>
                /// Is motor on the ground?
                ///</summary>
                public bool IsOnGround
                {
                    get
                    {
                        return IsCollidedWithSurface(CollidedSurfaces.Ground) || IsOnSlope;
                    }
                }

                ///<summary>
                /// Is motor in mid air?
                ///</summary>
                public bool IsInAir
                {
                    get
                    {
                        return !IsCollidedWithSurface(CollidedSurfaces.Ground) &&
                               !IsCollidedWithSurface(CollidedSurfaces.WallLeft) &&
                               !IsCollidedWithSurface(CollidedSurfaces.WallRight) &&
                               !IsOnSlope;
                    }
                }

                public HostController Host
                {
                    get
                    {
                        return this.host;
                    }

                    set
                    {
                        this.host = value;
                    }
                }

                public UnityEngine.Vector2 HostPreviousPosition
                {
                    get
                    {
                        return this.hostPreviousPosition;
                    }

                    set
                    {
                        this.hostPreviousPosition = value;
                    }
                }

                public bool HasHost
                {
                    get
                    {
                        return this.host != null;
                    }
                }
                #endregion

                #region Public Functions
                public bool IsCollidedWithSurface(CollidedSurfaces collidedSurfaces)
                {
                    return (this.collidedSurfaces & collidedSurfaces) != CollidedSurfaces.None;
                }
                #endregion
            }
        }
    }
}
