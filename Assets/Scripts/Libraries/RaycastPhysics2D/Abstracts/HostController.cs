﻿using System;
using System.Collections.Generic;
using Libraries.RaycastPhysics2D.Components;
using UnityEngine;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Interfaces
        {
            public abstract class HostController : MonoBehaviour
            {
                #region Private Variables
                #endregion

                #region Actions
                public event Action<CharacterRaycastPhysicsComponent> OnAttach;
                public event Action<CharacterRaycastPhysicsComponent> OnDetach;
                #endregion

                #region Properties
                public virtual UnityEngine.Vector2 Position
                {
                    get
                    {
                        return this.transform.position;
                    }

                    set
                    {
                        this.transform.position = value;
                    }
                }
                #endregion

                #region Public Functions
                public void Attach(CharacterRaycastPhysicsComponent parasite)
                {
                    if (OnAttach != null)
                    {
                        OnAttach(parasite);
                    }
                }

                public void Detach(CharacterRaycastPhysicsComponent parasite)
                {
                    if (OnDetach != null)
                    {
                        OnDetach(parasite);
                    }
                }
                #endregion
            }
        }
    }
}