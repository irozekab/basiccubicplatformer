﻿using System;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Enumerables
        {
            [Serializable]
            public enum JumpType
            {
                None,
                OffGround,
                OffRightWall,
                OffLeftWall,
            };
        }
    }
}
