﻿using System;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Enumerables
        {
            /// <summary>
            /// The states the motor can be in.
            /// </summary>
            [Serializable]
            public enum CharacterState
            {
                Dashing,
                Falling,
                Frozen,
                Jumping,
                Slipping,
                Standing,
                Walking,
                WallSticking,
                WallSliding,
            };
        }
    }
}
