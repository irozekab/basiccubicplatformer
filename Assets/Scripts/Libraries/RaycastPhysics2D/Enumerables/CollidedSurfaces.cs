﻿using System;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Enumerables
        {
            /// <summary>
            /// The surfaces the motor may be colliding against.
            /// </summary>
            [Flags]
            public enum CollidedSurfaces
            {
                None = 0x0,
                Ground = 0x1,
                WallLeft = 0x2,
                WallRight = 0x4,
                Ceiling = 0x8,
                SlopeLeft = 0x10,
                SlopeRight = 0x20,
            }
        }
    }
}
