﻿using System;
using System.Collections.Generic;
using Libraries.RaycastPhysics2D.Interfaces;
using UnityEngine;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Components
        {
            public class MovingPlatformRaycastPhysicsComponent : HostController
            {
                #region Constants
                private const float DEFAULT_EASING_VALUE = 1F;
                #endregion  

                #region Actions 
                public Action<CharacterRaycastPhysicsComponent> OnCharacterAttachToMovingPlatform;
                #endregion

                #region Private Variables
                [SerializeField]
                private float speed;

                [SerializeField]
                private float waitTimeAtWaypoint;

                [SerializeField]
                [Range(1, 3)]
                private float easingValue = DEFAULT_EASING_VALUE;

                [SerializeField]
                private bool isCyclic;

                private int lastWaypointIndex;
                private float percentageMoved;
                private float nextMoveTime;

                [SerializeField]
                private List<UnityEngine.Vector2> localWaypoints;
                
                private List<UnityEngine.Vector2> globalWaypoints;

                private UnityEngine.Vector2 previousPosition;

#if UNITY_EDITOR
                [SerializeField]
                private bool isDrawGizmo = true;
#endif
                #endregion

                #region Properties
                public override UnityEngine.Vector2 Position
                {
                    get
                    {
                        return this.transform.position;
                    }

                    set
                    {
                        this.previousPosition = this.transform.position;
                        this.transform.position = value;
                    }
                }

                public UnityEngine.Vector2 PreviousPosition
                {
                    get
                    {
                        return this.previousPosition;
                    }
                }
                #endregion

                private void Start()
                {
                    ConvertLocalToGlobalWaypoints();
                }

                #region Lifecycle Functions
                private void FixedUpdate()
                {
                    MovePosition();
                }

#if UNITY_EDITOR
                private void OnDrawGizmos()
                {
                    if (this.isDrawGizmo)
                    {
                        if (!Application.isPlaying)
                        {
                            ConvertLocalToGlobalWaypoints();
                        }

                        Gizmos.Util.DrawConnectPoints(this.globalWaypoints, Color.red);
                    }
                }
#endif
                #endregion

                #region Private Functions
                private void MovePosition()
                {
                    if (this.globalWaypoints.Count > 1)
                    {
                        this.transform.Translate(CalculatePlatformMovement());
                    }
                }

                private void ConvertLocalToGlobalWaypoints()
                {
                    this.globalWaypoints = new List<UnityEngine.Vector2>();

                    foreach (UnityEngine.Vector2 localWaypoint in this.localWaypoints)
                    {
                        this.globalWaypoints.Add(localWaypoint + (UnityEngine.Vector2)this.transform.position);
                    }
                }

                //https://youtu.be/3D0PeJh6GY8
                private UnityEngine.Vector2 CalculatePlatformMovement()
                {
                    // Do not move if it's not time to move yet.
                    if (Time.time < this.nextMoveTime)
                    {
                        return UnityEngine.Vector2.zero;
                    }

                    this.lastWaypointIndex %= this.globalWaypoints.Count;
                    int nextWaypointIndex = (this.lastWaypointIndex + 1) % this.globalWaypoints.Count;
                    float distanceBetweenWaypoints = UnityEngine.Vector2.Distance(this.globalWaypoints[this.lastWaypointIndex], this.globalWaypoints[nextWaypointIndex]);
                    this.percentageMoved += (Time.deltaTime * this.speed) / distanceBetweenWaypoints;

                    // This will clamp the percentageMoved to [0, 1], because that's required for the function to work, and ease the value based on the the easingValue.
                    float easedPercentageMoved = Math.Util.Ease01(this.percentageMoved, this.easingValue);

                    // Use the eased value instead of the actual.
                    UnityEngine.Vector2 newPosition = UnityEngine.Vector2.Lerp(this.globalWaypoints[this.lastWaypointIndex], this.globalWaypoints[nextWaypointIndex], easedPercentageMoved);

                    // If we reach the next waypoint.
                    if (this.percentageMoved >= 1)
                    {
                        this.percentageMoved = 0;
                        this.lastWaypointIndex++;

                        if (!this.isCyclic)
                        {
                            // If we reach the last waypoint, we want to restart from index 0, but we will reverse the globalwaypoints so that we move the platform backwards.
                            if (this.lastWaypointIndex >= this.globalWaypoints.Count - 1)
                            {
                                this.lastWaypointIndex = 0;
                                this.globalWaypoints.Reverse();
                            }
                        }

                        // Set the next move time.
                        this.nextMoveTime = Time.time + this.waitTimeAtWaypoint;
                    }

                    return newPosition - (UnityEngine.Vector2)this.transform.position;
                }
                #endregion
            }
        }
    }
}
