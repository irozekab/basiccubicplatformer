﻿using System;
using System.Collections;
using System.Collections.Generic;
using Libraries.Gameplay.Abstracts;
using UnityEngine;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Components
        {
            [RequireComponent(typeof(CharacterMoveComponent))]
            public class CharacterInputComponent : GameplayEventEnabler
            {
                #region Inspector Variables
                [Header("References")]
                [SerializeField]
                private CharacterMoveComponent characterMoveComponent;
                #endregion

                #region MonoBehaviour Functions
                protected override void Awake()
                {
                    base.Awake();

                    if (this.characterMoveComponent == null)
                    {
                        this.characterMoveComponent = GetComponent<CharacterMoveComponent>();
                    }
                }

                protected virtual void Update()
                {
                    UnityEngine.Vector3 input = new UnityEngine.Vector3(Input.GetAxisRaw(Gameplay.Constants.Input.HORIZONTAL), Input.GetAxisRaw(Gameplay.Constants.Input.VERTICAL), 0f);
                    this.characterMoveComponent.Move(input);

                    if (UnityEngine.Input.GetButtonDown(Gameplay.Constants.Input.JUMP))
                    {
                        this.characterMoveComponent.Jump();
                    }

                    if (UnityEngine.Input.GetButtonUp(Gameplay.Constants.Input.JUMP))
                    {
                        this.characterMoveComponent.EndJump();
                    }

                    if (UnityEngine.Input.GetButtonDown(Gameplay.Constants.Input.DASH))
                    {
                        this.characterMoveComponent.Dash();
                    }

                    //this.moveComponent.IsJumpingHeld = UnityEngine.Input.GetButton(Libraries.Constants.Input.JUMP);
                }
                #endregion
            }
        }
    }
}
