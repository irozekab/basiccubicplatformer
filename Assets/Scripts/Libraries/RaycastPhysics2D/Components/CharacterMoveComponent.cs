﻿using UnityEngine;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Components
        {
            [RequireComponent(typeof(CharacterRaycastPhysicsComponent))]
            public class CharacterMoveComponent : MonoBehaviour
            {
                #region Inspector Variables
                [Header("References")]
                [SerializeField]
                private CharacterRaycastPhysicsComponent characterRaycastPhysicsComponent;
                #endregion

                #region Private Variables
                //private bool isRestored = true;
                //private bool enableOneWayPlatforms;
                //private bool isOneWayPlatformsAreWalls;
                //private UnityEngine.Vector2 input;
                #endregion

                #region Properties
                #endregion

                #region MonoBehaviour Functions
                private void Awake()
                {
                    if (this.characterRaycastPhysicsComponent == null)
                    {
                        this.characterRaycastPhysicsComponent = GetComponent<CharacterRaycastPhysicsComponent>();
                    }
                }

                //private void Update()
                //{
                /*
                this.input = new UnityEngine.Vector2(UnityEngine.Input.GetAxis(Libraries.Constants.Input.HORIZONTAL), UnityEngine.Input.GetAxis((Libraries.Constants.Input.VERTICAL)));

                // use last state to restore some ladder specific values
                if (this.characterRaycastPhysicsComponent.CurrentMotorState != CharacterRaycastPhysicsComponent.MotorState.FreedomState)
                {
                    // try to restore, sometimes states are a bit messy because change too much in one frame
                    FreedomStateRestore(this.characterRaycastPhysicsComponent);
                }

                // Jump?
                // If you want to jump in ladders, leave it here, otherwise move it down
                if (UnityEngine.Input.GetButtonDown(Libraries.Constants.Input.JUMP))
                {
                    this.characterRaycastPhysicsComponent.Jump();
                    this.characterRaycastPhysicsComponent.DisableRestrictedArea();
                }

                this.characterRaycastPhysicsComponent.IsJumpingHeld = UnityEngine.Input.GetButton(Libraries.Constants.Input.JUMP);

                // XY freedom movement
                // done
                if (this.characterRaycastPhysicsComponent.CurrentMotorState == CharacterRaycastPhysicsComponent.MotorState.FreedomState)
                {
                    this.characterRaycastPhysicsComponent.NormalizedXMovement = this.input.x;
                    this.characterRaycastPhysicsComponent.NormalizedYMovement = this.input.y;

                    return; // do nothing more
                }

                // X axis movement
                if (UnityEngine.Input.GetAxis(Libraries.Constants.Input.HORIZONTAL) != 0)
                {
                    this.characterRaycastPhysicsComponent.NormalizedXMovement = this.input.x;
                }
                else
                {
                    this.characterRaycastPhysicsComponent.NormalizedXMovement = 0;
                }

                if (UnityEngine.Input.GetAxis(Libraries.Constants.Input.VERTICAL) != 0)
                {
                    bool up_pressed = UnityEngine.Input.GetAxis(Libraries.Constants.Input.VERTICAL) > 0;
                    if (this.characterRaycastPhysicsComponent.IsOnLadder)
                    {
                        if ((up_pressed && this.characterRaycastPhysicsComponent.CurrentLadderZone == CharacterRaycastPhysicsComponent.LadderZone.Top) || (!up_pressed && this.characterRaycastPhysicsComponent.CurrentLadderZone == CharacterRaycastPhysicsComponent.LadderZone.Bottom))
                        {
                            // do nothing!
                        }
                        // if player hit up, while on the top do not enter in freeMode or a nasty short jump occurs
                        else
                        {
                            // example ladder behaviour

                            this.characterRaycastPhysicsComponent.FreedomStateEnter(); // enter freedomState to disable gravity
                            this.characterRaycastPhysicsComponent.EnableRestrictedArea();  // movements is retricted to a specific sprite bounds

                            // now disable OWP completely in a "trasactional way"
                            FreedomStateSave(this.characterRaycastPhysicsComponent);
                            this.characterRaycastPhysicsComponent.enableOneWayPlatforms = false;
                            this.characterRaycastPhysicsComponent.isOneWayPlatformsAreWalls = false;

                            // start XY movement
                            this.characterRaycastPhysicsComponent.NormalizedXMovement = this.input.x;
                            this.characterRaycastPhysicsComponent.NormalizedYMovement = this.input.y;
                        }
                    }
                }

                if (UnityEngine.Input.GetButtonDown(Libraries.Constants.Input.DASH))
                {
                    this.characterRaycastPhysicsComponent.Dash();
                }
                */
                //}
                #endregion

                #region Public Functions
                public void Move(UnityEngine.Vector3 input)
                {
                    //if (this.characterRaycastPhysicsComponent.CurrentState == CharacterState.Free)
                    //{
                    //    Debug.Log("in");
                    //    this.characterRaycastPhysicsComponent.NormalizedXMovement = input.x;
                    //    this.characterRaycastPhysicsComponent.NormalizedYMovement = input.y;
                    //}
                    //else
                    //{
                    //    Debug.Log("out");
                    //}

                    this.characterRaycastPhysicsComponent.NormalizedX = input.x;
                }

                public void Jump()
                {
                    this.characterRaycastPhysicsComponent.Jump();
                }

                public void EndJump()
                {
                    this.characterRaycastPhysicsComponent.EndJump();
                }

                public void Dash()
                {
                    this.characterRaycastPhysicsComponent.Dash();
                }
                #endregion

                #region Private Functions
                // before enter en freedom state for ladders
                //    void FreedomStateSave(CharacterRaycastPhysicsComponent characterRaycastPhysicsComponent)
                //    {
                //        // do not enter twice
                //        if (!this.isRestored)
                //        {
                //            return;
                //        }

                //        this.isRestored = false;
                //        this.enableOneWayPlatforms = this.characterRaycastPhysicsComponent.enableOneWayPlatforms;
                //        this.isOneWayPlatformsAreWalls = this.characterRaycastPhysicsComponent.isOneWayPlatformsAreWalls;
                //    }
                //    // after leave freedom state for ladders
                //    void FreedomStateRestore(CharacterRaycastPhysicsComponent characterRaycastPhysicsComponent)
                //    {
                //        // do not enter twice
                //        if (this.isRestored)
                //        {
                //            return;
                //        }

                //        this.isRestored = true;
                //        this.characterRaycastPhysicsComponent.enableOneWayPlatforms = this.enableOneWayPlatforms;
                //        this.characterRaycastPhysicsComponent.isOneWayPlatformsAreWalls = this.isOneWayPlatformsAreWalls;
                //    }

                //}
                ////[SerializeField]
                ////private bool isFlipXScaleOnLeft = false;

                ////[SerializeField]
                ////private bool isSetAnimatorVerticalVelocity = false;

                //protected override void Start()
                //{
                //}

                //private void Update()
                //{
                //    if (this.canMove)
                //    {
                //        UnityEngine.Vector2 input = new UnityEngine.Vector2(UnityEngine.Input.GetAxis("Horizontal"), UnityEngine.Input.GetAxis("Vertical"));
                //        this.characterRaycastPhysicsComponent.Input = input;

                //        if (this.canJump && UnityEngine.Input.GetButtonDown("Jump"))
                //        {
                //            this.characterRaycastPhysicsComponent.Jump();
                //        }
                //        else if (this.canJump && UnityEngine.Input.GetButtonUp("Jump"))
                //        {
                //            this.characterRaycastPhysicsComponent.StopJump();
                //        }
                //    }
                //    //    if (this.isSetAnimatorVerticalVelocity)
                //    //    {
                //    //        this.animator.SetFloat("verticalVelocity", this.rigidbody2D.velocity.y);
                //    //    }
                //}

                //protected override void Initialize()
                //{
                //}

                ////private void SetFaceDirection(float horizontalInput)
                ////{
                ////    if (horizontalInput == 0)
                ////    {
                ////        return;
                ////    }

                ////    this.faceDirection = horizontalInput > 0 ? RelativeDirection.Right : RelativeDirection.Left;

                ////    if (this.isFlipXScaleOnLeft)
                ////    {
                ////        this.spriteRenderer.flipX = this.faceDirection == RelativeDirection.Left;
                ////    }
                //}
                #endregion
            }
        }
    }
}