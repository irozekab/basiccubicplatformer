using System;
using Libraries.Math;
using Libraries.RaycastPhysics2D.Enumerables;
using Libraries.RaycastPhysics2D.States;
using Libraries.Services;
using Libraries.RaycastPhysics2D.Common;
using UnityEngine;
using Libraries.CustomEditor;
using UnityEditor;
using Libraries.RaycastPhysics2D.Interfaces;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Components
        {
            [RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D))]
            public class CharacterRaycastPhysicsComponent : MonoBehaviour
            {
                #region Constants
#if UNITY_EDITOR
                private const float GIZMO_LABEL_OFFSET = 0.2f;
#endif

                private const float FLIP_X_DIRECTION_MULTIPLIER = -1f;
                private const float ALMOST_ZERO = 0.0001f;
                private const float THRESHOLD_DISTANCE_TO_END_ITERATION = 0.001f;
                private const float THRESHOLD_DISTANCE_TO_END_ITERATION_SQUARED = THRESHOLD_DISTANCE_TO_END_ITERATION * THRESHOLD_DISTANCE_TO_END_ITERATION;
                private const float DEFAULT_ENVIRONMENT_CHECK_DISTANCE = 0.14f;
                private const float DEFAULT_MIN_DISTANCE_FROM_ENVIRONMENT = 0.07f;
                private const float CHECK_FOR_OVERLAPPING_OFFSET = 0.02f;
                private const float DEFAULT_TIME_SCALE = 1f;
                private const int DEFAULT_ITERATIONS_PER_FIXED_UPDATE = 2;

                private const float DEFAULT_MAX_ANGLE_ALLOWED_FOR_SLOPE = 50f;
                private const float DEFAULT_STICK_TO_GROUND_ON_SLOPE_DISTANCE = 0.4f;

                private const float DEFAULT_MAX_AIR_SPEED = 5f;
                private const float DEFAULT_TIME_TO_REACH_AIR_SPEED = 0.2f;
                private const float DEFAULT_AIR_STOP_DISTANCE = 2f;
                private const float DEFAULT_MAX_FALL_SPEED = 16f;
                private const float DEFAULT_GRAVITY_MULTIPLIER = 4f;
                private const float DEFAULT_MAX_FAST_FALL_SPEED = 32f;
                private const float DEFAULT_FAST_FALL_GRAVITY_MULTIPLIER = 8f;

                private const float DEFAULT_MAX_GROUND_SPEED = 8f;
                private const float DEFAULT_TIME_TO_REACH_GROUND_SPEED = 0.05f;
                private const float DEFAULT_GROUND_STOP_DISTANCE = 0.333f;

                private const float DEFAULT_CHANGE_SPEED_ON_SLOPE_MULTIPLIER = 0.75f;

                private const float DEFAULT_MIN_JUMP_HEIGHT = 1f;
                private const float DEFAULT_MAX_JUMP_HEIGHT = 1.5f;
                private const int DEFAULT_MAX_AIR_JUMP_COUNT = 1;
                private const float DEFAULT_JUMP_ACTIVATED_GRACE_TIME = 0.2f;
                private const float DEFAULT_JUMP_ALLOWED_AFTER_FALLING_GRACE_TIME = 0.2f;

                private const float DEFAULT_NORMALIZES_X_THRESHOLD_FOR_WALL_INTERACTION = 0.5f;
                private const float DEFAULT_WALL_INTERACTION_COOLDOWN = 0.1f;
                private const float DEFAULT_WALL_JUMP_MULTIPLIER = 1f;
                private const float DEFAULT_WALL_JUMP_ANGLE = 80f;
                private const float DEFAULT_WALL_JUMP_AWAY_ANGLE = 30f;
                private const float DEFAULT_NO_MOVEMENT_AFTER_JUMP_GRACE_TIME = 0.2f;
                private const float DEFAULT_WALL_SLIDE_SPEED = 5f;
                private const float DEFAULT_TIME_TO_REACH_WALL_SLIDE_SPEED = 3f;
                private const float DEFAULT_WALL_STICK_DURATION = 0.5f;

                private const float DEFAULT_DASH_DISTANCE = 3f;
                private const float DEFAULT_DASH_DURATION = 0.3f;
                private const float DEFAULT_DASH_COOLDOWN = 0.8f;

                private const int DEFAULT_HITS_AND_OVERLAPPED_COLLIDERS_ARRAY_SIZE = 4;
                private const int HITS_AND_OVERLAPPED_COLLIDERS_INCREASE_ARRAY_SIZE_MULTIPLIER = 2;
                private const float CHECK_TOUCHING_TRIM = 0.01f;

                private const int DEFAULT_RAYCAST_COUNT_PER_SIDE_FOR_OVERLAP_HANDLING = 1;
                #endregion

                #region Actions
                /// <summary>
                /// Delegate that notifies when the motor dashes.
                /// </summary>
                public Action OnDash;

                /// <summary>
                /// Delegate that notifies when the motor's dash ends.
                /// </summary>
                public Action OnDashEnd;

                /// <summary>
                /// Delegate that notifies when the motor jumps (ALL JUMPS!).
                /// </summary>
                public Action OnJump;

                /// <summary>
                /// Delegate that notifies, before change state, last jump has ended, reached the maximum allowed.
                /// Do not has parity with OnJump, this is called when the last Jump ends not when Jump again.
                /// </summary>
                public Action OnJumpEnd;

                /// <summary>
                /// Delegate that notifies when the motor air jumps (called before OnJump).
                /// </summary>
                public Action OnAirJump;

                /// <summary>
                /// Delegate that notifies when the motor walls jumps (called before onJump). The vector passed is the normal of the wall.
                /// </summary>
                public Action<UnityEngine.Vector2> OnWallJump;

                /// <summary>
                /// Delegate that notifies when the motor has landed. HeightFallen can be queried for distance fallen.
                /// </summary>
                public Action OnLanded;

                /// <summary>
                /// Delegate that notifies when the motor start slipping.
                /// </summary>
                public Action OnSlipping;

                /// <summary>
                /// Delegate that notifies, before change state, that the motor end sliping.
                /// </summary>
                public Action OnSlippingEnd;
                #endregion

                #region Private Variables
#if UNITY_EDITOR
                [SerializeField]
                private bool isDrawGizmo = true;

                private int fixedUpdateSincePlay = 0;
#endif
 
                #region General                
                private new Rigidbody2D rigidbody2D;
                private new Collider2D collider2D;

                /// <summary>
                /// Previous position use to track jump and fall distance.
                /// </summary>
                private UnityEngine.Vector3 previousPosition;

                /// <summary>
                /// The static environment check mask. This should only be environment that doesn't move.
                /// </summary>
                [SerializeField]
                private LayerMask staticEnvironmentLayerMask;

                /// <summary>
                /// Time scale for the motor. Independent of the global time scale. Can slow down or speed up motor. 
                /// Do not set this directly. Refer to TimeScale.
                /// </summary>
                [SerializeField]
                private float timeScale = DEFAULT_TIME_SCALE;

                private float deltaTime;

                /// <summary>
                /// The number of iterations the motor is allowed to make during the fixed update. Lower number will be more performant
                /// at a cost of losing some movement when collisions occur.
                /// </summary>
                [SerializeField]
                private int iterationsPerFixedUpdate = DEFAULT_ITERATIONS_PER_FIXED_UPDATE;

                /// <summary>
                /// How far out the motor will check for the environment mask. 
                /// This value can be tweaked if jump checks are not firing when wanted.
                /// </summary>
                [SerializeField]
                private float environmentCheckDistance = DEFAULT_ENVIRONMENT_CHECK_DISTANCE;
                private float environmentCheckDistanceCorner;

                /// <summary>
                /// The distance the motor will separate itself from environment. This is useful to prevent the motor from catching on edges.
                /// </summary>
                [SerializeField]
                private float minDistanceFromEnvironment = DEFAULT_MIN_DISTANCE_FROM_ENVIRONMENT;
                private float minDistanceFromEnvironmentCorner;

                /// <summary>
                /// Should the motor snap to the mindistance?
                /// </summary>
                [SerializeField]
                private bool isSnapToMinDistanceFromEnvironment = true;

                /// <summary>
                /// The current state of the motor.
                /// </summary>
                [SerializeField]
                private CharacterState currentState;

                private bool isCurrentStateInitialized = false;

                [SerializeField]
                private SurroundingState surroundingState;

                private LayerMask collisionMask;
                private bool isFacingLeft;

                /// <summary>
                /// Maintain the previous state.
                /// </summary>
                private CharacterState previousState;

                [SerializeField]
                private float normalizedX;
                private float normalizedY;

                /// <summary>
                /// This is the unconverted motor velocity. This ignores slopes. It is converted into the appropriate vector before moving.
                /// MoveMotor() will move the motor based on this value.
                /// </summary>
                [SerializeField]
                private UnityEngine.Vector2 velocity;
                #endregion

                #region Air
                private FallState fallState = new FallState();

                /// <summary>
                /// The maximum speed that the motor will fall. Only effects vertical speed when falling.
                /// </summary>
                [SerializeField]
                private float maxFallSpeed = DEFAULT_MAX_FALL_SPEED;

                /// <summary>
                /// Gravity multiplier to the Physics2D.gravity setting. Works like RigidBody2D's gravityMultiplier. Also refer to GravityMultiplier.
                /// </summary>
                [SerializeField]
                private float gravityMultiplier = DEFAULT_GRAVITY_MULTIPLIER;

                /// <summary>
                /// If true, then the player can change x direction while jumping. If false, then
                /// the x velocity when leaving the ground will be isHeld while in the air
                /// </summary>
                [SerializeField]
                private bool enableCanMoveInAir = true;

                /// <summary>
                /// The maximum horizontal speed of the motor in the air.
                /// </summary>
                [SerializeField]
                private float maxAirSpeed = DEFAULT_MAX_AIR_SPEED;

                /// <summary>
                /// The time it takes to move from zero horizontal speed to the maximum speed. 
                /// This value is used to calculate the acceleration.
                /// </summary>
                [SerializeField]
                private float timeToReachAirSpeed = DEFAULT_TIME_TO_REACH_AIR_SPEED;

                /// <summary>
                /// The distance the motor will 'slide' to a stop while in the air. Only effects horizontal
                /// movement.
                /// </summary>
                [SerializeField]
                private float airStopDistance = DEFAULT_AIR_STOP_DISTANCE;
                #endregion

                #region Ground
                /// <summary>
                /// The maximum speed the motor will move on the ground, only effects horizontal speed.
                /// </summary>
                [SerializeField]
                private float maxGroundSpeed = DEFAULT_MAX_GROUND_SPEED;

                /// <summary>
                /// How much time does it take for the motor to get from zero to max ground speed. 
                /// This value is used to calculate the ground acceleration.
                /// </summary>
                [SerializeField]
                private float timeToReachGroundSpeed = DEFAULT_TIME_TO_REACH_GROUND_SPEED;

                /// <summary>
                /// The distance the motor will slide to a stop from full speed while on the ground.
                /// </summary>
                [SerializeField]
                private float groundStopDistance = DEFAULT_GROUND_STOP_DISTANCE;
                #endregion

                #region Slope
                /// <summary>
                /// 2 vectors with a angle of 45dgree right down and left down respectively.
                /// Use for slope checking.
                /// </summary>
                private UnityEngine.Vector2 rightSlopeCheckDirection;
                private UnityEngine.Vector2 leftSlopeCheckDirection;

                /// <summary>
                /// Should the motor check for any slopes? Set this to false if there are no slopes, the motor will be more efficient.
                /// </summary>
                [SerializeField]
                private bool enableSlope = true;

                /// <summary>
                /// The angle of slope the motor is allowed to walk on. It could be a good idea to keep this slightly above the minimum.
                /// </summary>
                [SerializeField]
                private float maxAngleAllowedForSlope = DEFAULT_MAX_ANGLE_ALLOWED_FOR_SLOPE;

                /// <summary>
                /// Should the speed of the motor change depending of the angle of the slope. 
                /// This only impacts walking on slopes, not while sliding.
                /// </summary>
                [SerializeField]
                private bool enableChangeSpeedOnSlope = true;

                /// <summary>
                /// If the motor changes speed on slopes then this acts as a multiplier. Lower values will mean bigger slow downs. A value
                /// of 1 means that it's only based off of the angle of the slope.
                /// </summary>
                [SerializeField]
                [Range(0f, 1f)]
                private float changeSpeedOnSlopeMultiplier = DEFAULT_CHANGE_SPEED_ON_SLOPE_MULTIPLIER;

                /// <summary>
                /// Should the motor stick to the ground when walking down onto a slope or up over a slope? Otherwise the motor may fall onto
                /// the slope or have a slight hop when moving up over a slope.
                /// </summary>
                //[SerializeField]
                //private bool enableStickToGroundOnSlope = true;

                /// <summary>
                /// If enableStickToGroundOnSlope is true then the motor will search down for the ground to place itself on. This is how far it is willing
                /// to check. This needs to be high enough to account for the distance placed by the motor speed but should be smaller than
                /// the difference between environment heights. Play around until a nice value is found.
                /// </summary>
                [SerializeField]
                private float stickToGroundOnSlopeDistance = DEFAULT_STICK_TO_GROUND_ON_SLOPE_DISTANCE;
                #endregion

                #region Jump
                [SerializeField]
                private JumpState jumpState = new JumpState();

                /// <summary>
                /// The height the motor will jump when a jump command is issued.
                /// </summary>
                [SerializeField]
                private float minJumpHeight = DEFAULT_MIN_JUMP_HEIGHT;

                /// <summary>
                /// The height the motor will jump if jump is 'isHeld' down.
                /// </summary>
                [SerializeField]
                private float maxJumpHeight = DEFAULT_MAX_JUMP_HEIGHT;

                /// <summary>
                /// Number of air jumps allowed.
                /// </summary>
                [SerializeField]
                private int maxAirJumpCount = DEFAULT_MAX_AIR_JUMP_COUNT;

                /// <summary>
                /// The grace period once the motor is told to jump where it will jump.
                /// How long the motor should remember that Jump() was called and activate a jump if it becomes valid in that time. 
                /// This means that a player could press jump before they actually hit the ground and the motor will allow it to occur.
                /// </summary>
                [SerializeField]
                private float jumpActivatedGraceTime = DEFAULT_JUMP_ACTIVATED_GRACE_TIME;

                /// <summary>
                /// The amount of time once the motor has left an environment that a jump will be allowed.
                /// </summary>
                [SerializeField]
                private float currentJumpTypeAllowedAfterFallingGraceTime = DEFAULT_JUMP_ALLOWED_AFTER_FALLING_GRACE_TIME;

                /// <summary>
                /// If true, motor can try to jump up a slope that is too steep.
                /// </summary>
                [SerializeField]
                private bool enableJumpUpSlopeTooSteep = true;
                #endregion

                #region Wall
                private WallState wallState = new WallState();

                /// <summary>
                /// The threshold that normalizedX will have to be higher than to consider wall sticks, wall slides and wall jumps.
                /// </summary>
                [SerializeField]
                [Range(0f, 1f)]
                private float normalizedXThresholdForWallInteraction = DEFAULT_NORMALIZES_X_THRESHOLD_FOR_WALL_INTERACTION;

                /// <summary>
                /// Cooldown for allowing slides and sticks. This may be necessary if the motor can slide down a vertical
                /// moving platform. If they don't exist then this can be 0.
                /// </summary>
                //[SerializeField]
                //private float wallInteractionCooldown = DEFAULT_WALL_INTERACTION_COOLDOWN;

                /// <summary>
                /// After a wall jump, this is how longer horizontal input is ignored.
                /// </summary>
                [SerializeField]
                private float noMovementAfterJumpGraceTime = DEFAULT_NO_MOVEMENT_AFTER_JUMP_GRACE_TIME;

                /// <summary>
                /// If wall jumps are allowed.
                /// </summary>
                [SerializeField]
                private bool enableWallJump = true;

                /// <summary>
                /// The jump speed multiplier when wall jumping. This is useful to isForce bigger jumps off of the wall.
                /// </summary>
                [SerializeField]
                private float wallJumpMultiplier = DEFAULT_WALL_JUMP_MULTIPLIER;

                /// <summary>
                /// The angle (degrees) in which the motor will jump away from the wall. 
                /// 0 is horizontal and 90 is straight up.
                /// </summary>
                [SerializeField]
                [Range(0f, 90f)]
                private float wallJumpAngle = DEFAULT_WALL_JUMP_ANGLE;

                /// <summary>
                /// The angle (degrees) in which the motor will jump away from the wall when normalized x is away from the wall. 
                ///0 is horizontal and 90 is straight up.
                /// </summary>
                [SerializeField]
                [Range(0f, 90f)]
                private float wallJumpAwayAngle = DEFAULT_WALL_JUMP_AWAY_ANGLE;

                private UnityEngine.Vector2 wallJumpVector;
                private UnityEngine.Vector2 wallJumpAwayVector;

                /// <summary>
                /// If wall slides are allowed. A wall slide is when a motor slides down a wall. This will only take in effect
                /// once the stick is over.
                /// </summary>
                [SerializeField]
                private bool enableWallSlide = true;

                /// <summary>
                /// The speed that the motor will slide down the wall.
                /// </summary>
                [SerializeField]
                private float wallSlideSpeed = DEFAULT_WALL_SLIDE_SPEED;

                /// <summary>
                /// The time, in seconds, to get to wall slide speed.
                /// </summary>
                [SerializeField]
                private float timeToReachWallSlideSpeed = DEFAULT_TIME_TO_REACH_WALL_SLIDE_SPEED;

                /// <summary>
                /// If wall sticking is allowed. A wall sticking is when a motor will 'grab' a wall.
                /// </summary>
                [SerializeField]
                private bool enableWallStick = true;

                /// <summary>
                /// The duration of the wall sticks in seconds. Set to a very large number to effectively allow permanent sticks.
                /// </summary>
                [SerializeField]
                private float wallStickDuration = DEFAULT_WALL_STICK_DURATION;
                #endregion

                #region Dash
                [SerializeField]
                private DashState dashState = new DashState();

                /// The function is cached to avoid unnecessary memory allocation.
                //private Easing.EasingFunc dashFunction;
                private Easing.EasingFunc dashDerivativeFunction;

                /// <summary>
                /// Is dash allowed?
                /// </summary>
                [SerializeField]
                private bool enableDash = true;

                /// <summary>
                /// How far the motor will dash.
                /// </summary>
                [SerializeField]
                private float dashDistance = DEFAULT_DASH_DISTANCE;

                /// <summary>
                /// How long the dash lasts in seconds.
                /// </summary>
                [SerializeField]
                private float dashDuration = DEFAULT_DASH_DURATION;

                /// <summary>
                /// When the motor will be allowed to dash again after dashState. The cooldown begins at the end of a dash.
                /// </summary>
                [SerializeField]
                private float dashCooldownTime = DEFAULT_DASH_COOLDOWN;

                /// <summary>
                /// The easing function used during the dash. Pick 'Linear' for just a set speed.
                /// </summary>
                [SerializeField]
                private Easing.Functions dashEasingFunction = Easing.Functions.OutQuad;

                /// <summary>
                /// Delay (in seconds) before gravity is turned back on after a dash.
                /// </summary>
                [SerializeField]
                private float endDashNoGravityTime = 0.1f;
                #endregion

                #region One Way Platform
                /// <summary>
                /// Should the motor check for one way platforms? Set this to false if there aren't any, the motor will be more efficient.
                /// This will only have an effect if the motor's collider can't collide with its own layer. If it can then setting this to
                /// false won't help, one way platforms or not.
                ///
                /// To acheive one way platforms with the motor. Have a environment piece with a collider and a PlatformEffector2D component attached. 
                /// Be sure to check Use One Way and to check Used By Effector on the accompanied 2D collider. 
                /// The direction of the one way platform is the local up Vector of the platform (same as how the Unity Physics engine considers it). 
                /// The motor will collide with the platform if it's velocity points away from the 'up' of the platform.
                /// </summary>
                [SerializeField]
                private bool enableOneWayPlatform = false;

                /// <summary>
                /// if enableOneWayPlatforms is disabled how the motor should treat OWP?
                /// when both are disabled, OWP are ignored from collisions.
                /// </summary>
                [SerializeField]
                private bool isOneWayPlatformConsiderWall = true;

                /// <summary>
                /// Both used for GetFirstHit().
                /// Need an array if we enableOneWayPlatform. Because there can be instances where they line up in a column and we will overlap a few etc... 
                /// Quite a few nasty edge cases.
                /// We preassigned the memory because this will mean lesser overhead.
                /// </summary>
                private RaycastHit2D[] hits = new RaycastHit2D[DEFAULT_HITS_AND_OVERLAPPED_COLLIDERS_ARRAY_SIZE];
                private Collider2D[] overlappingColliders = new Collider2D[DEFAULT_HITS_AND_OVERLAPPED_COLLIDERS_ARRAY_SIZE];
                #endregion

                #region Host
                //private MovingPlatformState movingPlatformState = new MovingPlatformState();

                /// <summary>
                /// The layer that contains hosts. If there are no host then make sure this has no layers (value of 0).
                /// Optimizations are made in the motor if it isn't expecting any hosts.
                /// </summary>
                [SerializeField]
                private LayerMask hostLayerMask;
                #endregion

                #region Handle Overlapped
                /// <summary>
                /// Most of these are calculation that will be used alot to handle overlapping, caching them for efficiency.
                /// </summary>
                private UnityEngine.Vector2 centerToTopRightCornerVector;
                private UnityEngine.Vector2 centerToBottomRightCornerVector;
                private UnityEngine.Vector2 centerToBottomLeftCornerVector;
                private UnityEngine.Vector2 centerToTopLeftCornerVector;
                private float centerToMinDistanceFromEnvironmentCornerMagnitude;

                /// <summary>
                /// When checking for moving platforms that may have moved into the motor the corners are automatically casted on. This
                /// variable impacts how many more casts are made to each side. If the smallest size environment is the same size or bigger
                /// than the motor (width and height) then this can be 0. If it's at least half size then this can be 1. Increasing this
                /// number allows separation from smaller platform pieces but at a performance cost.
                /// </summary>
                [SerializeField]
                private int raycastCountPerSideForOverlapHandling = DEFAULT_RAYCAST_COUNT_PER_SIDE_FOR_OVERLAP_HANDLING;
                #endregion
                #endregion

                #region Properties
                /// <summary>
                /// Timescale for the motor. Independent of the global time scale. Can slow down or speed up motor.
                /// </summary>
                private float TimeScale
                {
                    get
                    {
                        return this.timeScale;
                    }

                    set
                    {
                        if (value != this.timeScale)
                        {
                            if (value > 0f)
                            {
                                /// Update all frame count to compensate for the new time scale.
                                float multiplier = this.timeScale / value;
                                this.dashState.CooldownFramesRemaining = Mathf.RoundToInt(this.dashState.CooldownFramesRemaining * multiplier);
                                this.dashState.NoGravityFramesRemaining = Mathf.RoundToInt(this.dashState.NoGravityFramesRemaining * multiplier);
                                this.dashState.DashingFramesRemaining = Mathf.RoundToInt(this.dashState.DashingFramesRemaining * multiplier);
                                this.jumpState.NoMovementAfterJumpGraceFramesRemaining = Mathf.RoundToInt(this.jumpState.NoMovementAfterJumpGraceFramesRemaining * multiplier);
                                this.jumpState.CurrentJumpTypeAllowedGraceFramesRemaining = Mathf.RoundToInt(this.jumpState.CurrentJumpTypeAllowedGraceFramesRemaining * multiplier);
                                this.jumpState.JumpActivatedGraceFramesRemaining = Mathf.RoundToInt(this.jumpState.JumpActivatedGraceFramesRemaining * multiplier);
                                this.jumpState.MaxJumpHeightFramesRemaining = Mathf.RoundToInt(this.jumpState.MaxJumpHeightFramesRemaining * multiplier);
                                this.wallState.WallStickFramesRemaining = Mathf.RoundToInt(this.wallState.WallStickFramesRemaining * multiplier);
                                this.wallState.WallInteractionCooldownFramesRemaining = Mathf.RoundToInt(this.wallState.WallInteractionCooldownFramesRemaining * multiplier);
                            }

                            this.timeScale = Mathf.Max(value, 0f);

                            /// Update reset values
                            this.dashState.CooldownFramesRemainingResetValue = ConvertTimeToFrame(this.dashCooldownTime);
                            this.dashState.DashingFramesRemainingResetValue = ConvertTimeToFrame(this.dashDuration);
                            this.dashState.NoGravityFramesRemainingResetValue = ConvertTimeToFrame(this.endDashNoGravityTime);

                            this.jumpState.CurrentJumpTypeAllowedGraceFramesRemainingResetValue = ConvertTimeToFrame(this.currentJumpTypeAllowedAfterFallingGraceTime);
                            this.jumpState.JumpActivatedGraceFramesRemainingResetValue = ConvertTimeToFrame(this.jumpActivatedGraceTime);
                            this.jumpState.NoMovementAfterJumpGraceFramesRemainingResetValue = ConvertTimeToFrame(this.noMovementAfterJumpGraceTime);

                            this.wallState.WallStickFramesRemainingResetValue = ConvertTimeToFrame(this.wallStickDuration);
                        }
                    }
                }

                /// <summary>
                /// Timescale adjusted delta time for current fixed update.
                /// </summary>
                private float TimeScaleAdjustedDeltaTime
                {
                    get
                    {
                        return this.deltaTime * this.timeScale;
                    }
                }

                /// <summary>
                /// The surfaces the motor is currently colliding against.
                /// </summary>
                public CollidedSurfaces CollidingAgainst
                {
                    get
                    {
                        return this.surroundingState.CollidedSurfaces;
                    }
                }

                /// <summary>
                /// Set the x movement direction. This is multiplied by the max speed. -1 is full left, 1 is full right. 
                /// Higher numbers will result in faster speed.
                /// </summary>
                public float NormalizedX
                {
                    get
                    {
                        return this.normalizedX;
                    }

                    set
                    {
                        this.normalizedX = value;

                        if (this.normalizedX != 0f)
                        {
                            this.isFacingLeft = this.normalizedX < 0f;
                        }
                    }
                }

                /// <summary>
                /// Set the y movement direction. This is multiplied by the max speed. -1 is full down, 1 is full up. 
                /// Higher numbers will result in faster speed.
                /// Not used.
                /// </summary>
                public float NormalizedY
                {
                    get
                    {
                        return this.normalizedY;
                    }

                    set
                    {
                        this.normalizedY = value;
                    }
                }

                /// <summary>
                /// The velocity of the motor. 
                /// This should be queried instead of the rigidbody's velocity. Setting this during a dash doesn't have any meaning.
                /// </summary>
                public UnityEngine.Vector2 Velocity
                {
                    get
                    {
                        return IsDashing ? this.dashState.Direction * GetDashSpeed() : this.velocity;
                    }

                    set
                    {
                        this.velocity = value;
                    }
                }

                /// <summary>
                /// Current character state of the motor. 
                /// This will be information such as if the object is in the air or on the ground. Can be use to set the appropriate animations.
                /// </summary>
                public CharacterState CurrentState
                {
                    get
                    {
                        return this.currentState;
                    }

                    set
                    {
                        if (value != this.currentState)
                        {
                            /// Call the end delegate for the previous state since we are moving to the new state.
                            switch (this.currentState)
                            {
                                case CharacterState.Dashing:
                                    if (this.OnDashEnd != null)
                                    {
                                        this.OnDashEnd();
                                    }
                                    break;

                                case CharacterState.Falling:
                                    if (this.OnLanded != null)
                                    {
                                        this.OnLanded();
                                    }
                                    break;

                                case CharacterState.Jumping:
                                    if (this.OnJumpEnd != null)
                                    {
                                        this.OnJumpEnd();
                                    }
                                    break;

                                case CharacterState.Slipping:
                                    if (this.OnSlippingEnd != null)
                                    {
                                        this.OnSlippingEnd();
                                    }
                                    break;
                            }

                            this.previousState = this.currentState;
                            this.currentState = value;
                            this.isCurrentStateInitialized = false;

                            switch (this.currentState)
                            {
                                case CharacterState.Dashing:
                                    if (this.OnDash != null)
                                    {
                                        this.OnDash();
                                    }
                                    break;

                                case CharacterState.Jumping:
                                    if (this.OnJump != null)
                                    {
                                        this.OnJump();
                                    }
                                    break;

                                case CharacterState.Slipping:
                                    if (this.OnSlipping != null)
                                    {
                                        this.OnSlipping();
                                    }
                                    break;
                            }
                        }
                    }
                }

                #region CurrentState Flags
                ///<summary>
                /// Is the motor dashing?
                ///</summary>
                public bool IsDashing
                {
                    get
                    {
                        return this.currentState == CharacterState.Dashing;
                    }
                }

                ///<summary>
                /// Is the motor falling? This do not include fast falling.
                ///</summary>
                public bool IsFalling
                {
                    get
                    {
                        return this.currentState == CharacterState.Falling;
                    }
                }

                /// <summary>
                /// Is the motor frozen?
                /// </summary>
                public bool IsFrozen
                {
                    get
                    {
                        return this.currentState == CharacterState.Frozen;
                    }
                }

                ///<summary>
                /// Is the motor Jumping? This includes walljumps and airjumps.
                ///</summary>
                public bool IsJumping
                {
                    get
                    {
                        return this.currentState == CharacterState.Jumping;
                    }
                }
                
                ///<summary>
                /// Is the motor slipping?
                ///</summary>
                public bool IsSlipping
                {
                    get
                    {
                        return this.currentState == CharacterState.Slipping;
                    }
                }

                ///<summary>
                /// Motor is Standing?
                ///</summary>
                public bool IsStanding
                {
                    get
                    {
                        return this.currentState == CharacterState.Standing;
                    }
                }

                ///<summary>
                /// Motor is Walking?
                ///</summary>
                public bool IsWalking
                {
                    get
                    {
                        return this.currentState == CharacterState.Walking;
                    }
                }

                ///<summary>
                /// Is the motor stick to a wall?
                /// Use PressingIntoLeftWall, PressingIntoRightWall to know what wall.
                ///</summary>
                public bool IsWallSticking
                {
                    get
                    {
                        return this.currentState == CharacterState.WallSticking;
                    }
                }

                ///<summary>
                /// Is the motor sliding down a wall?
                /// Use PressingIntoLeftWall, PressingIntoRightWall to know what wall.
                ///</summary>
                public bool IsWallSliding
                {
                    get
                    {
                        return this.currentState == CharacterState.WallSliding;
                    }
                }
                #endregion

                #region Dash
                /// <summary>
                /// If the motor is currently able to dash.
                /// </summary>
                public bool CanDash
                {
                    get
                    {
                        return this.enableDash && !IsDashing && this.dashState.CooldownFramesRemaining < 0f;
                    }
                }

                /// <summary>
                /// Returns the direction of the current dash. If not dashState then returns UnityEngine.Vector2.zero.
                /// </summary>
                public UnityEngine.Vector2 DashDirection
                {
                    get
                    {
                        return IsDashing ? this.dashState.Direction : UnityEngine.Vector2.zero;
                    }
                }

                /// <summary>
                /// Returns the amount of distance dashed. If not dashState then returns 0.
                /// </summary>
                public float DistanceDashed
                {
                    get
                    {
                        return IsDashing ? this.dashState.DistanceDashed : 0f;
                    }
                }

                /// <summary>
                /// This is the distance calculated for dashed. Not be confused with DistanceDashed. 
                /// This doesn't care if the motor has hit a wall.
                /// </summary>
                public float DashDistanceCalculated
                {
                    get
                    {
                        return IsDashing ? this.dashState.DistanceCalculated : 0f;
                    }
                }
                #endregion

                /// <summary>
                /// Returns the amount the motor has jumped. This ceases to keep calculating after the motor starts to come down.
                /// </summary>
                public float HeightJumped
                {
                    get
                    {
                        return this.jumpState.HeightJumped;
                    }
                }

                /// <summary>
                /// Returns the height the motor has fallen. Includes fallen fast distance.
                /// </summary>
                public float HeightFallen
                {
                    get
                    {
                        return this.fallState.HeightFallen;
                    }
                }

                /// <summary>
                /// Motor is against a left wall.
                /// </summary>
                public bool IsPressAgainstLeftWall
                {

                    get
                    {
                        // TODO: not sure what's this.
                        //    if (this.movingPlatformState.IsOnPlatform &&
                        //        this.movingPlatformState.StuckToWall == CollidedSurfaces.LeftWall &&
                        //        this.normalizedX < -this.normalizedXThresholdForWallInteraction)
                        //    {
                        //        return true;
                        //}

                        return this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.WallLeft) && (this.normalizedX < -this.normalizedXThresholdForWallInteraction);
                    }
                }

                /// <summary>
                /// Motor is against a right wall.
                /// </summary>
                public bool IsPressAgainstRightWall
                {
                    get
                    {
                        // TODO: not sure what's this.
                        //    if (this.movingPlatformState.IsOnPlatform &&
                        //        this.movingPlatformState.StuckToWall == CollidedSurfaces.RightWall &&
                        //        this.normalizedX > this.normalizedXThresholdForWallInteraction)
                        //    {
                        //        return true;
                        //    }

                        return this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.WallRight) && (this.normalizedX > this.normalizedXThresholdForWallInteraction);
                    }
                }

                /// <summary>
                /// Gravity multiplier to the Physics2D.gravity setting. Works like RigidBody2D's gravityMultiplier.
                /// </summary>
                public float GravityMultiplier
                {
                    get
                    {
                        return this.gravityMultiplier;
                    }
                }

                /*

                /// <summary>
                /// Returns the moving platform that the motor is coupled with. If null then no moving platform.
                /// </summary>
                public MovingPlatformRaycastPhysicsComponent ConnectedPlatform
                {
                    get { return this.movingPlatformState.Platform; }
                }

                ///<summary>
                /// is the motor Jumping? include walljumps and airjumps.
                ///</summary>
                public bool IsUserHandled
                {
                    get
                    {
                        return this.currentState == CharacterState.Free;
                    }
                }
                */
                #endregion

                #region Lifecycle Functions
                private void Awake()
                {
                    this.collider2D = GetComponent<Collider2D>();
                    this.rigidbody2D = GetComponent<Rigidbody2D>();
                }

                private void Start()
                {
                    /// Set
                    this.environmentCheckDistanceCorner = Mathf.Sqrt(2f * this.environmentCheckDistance * this.environmentCheckDistance);
                    this.minDistanceFromEnvironmentCorner = Mathf.Sqrt(2f * this.minDistanceFromEnvironment * this.minDistanceFromEnvironment);

                    float centerToBoundsCornerMagnitude = (this.collider2D.bounds.max - this.collider2D.bounds.center).magnitude;
                    this.centerToMinDistanceFromEnvironmentCornerMagnitude = centerToBoundsCornerMagnitude + this.minDistanceFromEnvironmentCorner;
                    this.centerToTopRightCornerVector = (this.collider2D.bounds.max - this.collider2D.bounds.center) / centerToBoundsCornerMagnitude;
                    this.centerToBottomRightCornerVector = (new UnityEngine.Vector3(this.collider2D.bounds.max.x, this.collider2D.bounds.min.y) - this.collider2D.bounds.center) / centerToBoundsCornerMagnitude;
                    this.centerToBottomLeftCornerVector = (this.collider2D.bounds.min - this.collider2D.bounds.center) / centerToBoundsCornerMagnitude;
                    this.centerToTopLeftCornerVector = (new UnityEngine.Vector3(this.collider2D.bounds.min.x, this.collider2D.bounds.max.y) - this.collider2D.bounds.center) / centerToBoundsCornerMagnitude;

                    /// Start motor on falling state.
                    this.previousState = CharacterState.Falling;
                    this.currentState = CharacterState.Falling;

                    this.previousPosition = this.collider2D.bounds.center;
                    this.collisionMask = this.staticEnvironmentLayerMask | this.hostLayerMask;

                    /// Set dash variables.
                    //this.dashFunction = Easing.GetEasingFunction(this.dashEasingFunction);
                    this.dashDerivativeFunction = Easing.GetEasingFunctionDerivative(this.dashEasingFunction);
                    this.dashState.CooldownFramesRemainingResetValue = ConvertTimeToFrame(this.dashCooldownTime);
                    this.dashState.DashingFramesRemainingResetValue = ConvertTimeToFrame(this.dashDuration);
                    this.dashState.NoGravityFramesRemainingResetValue = ConvertTimeToFrame(this.endDashNoGravityTime);

                    /// Set jump variables
                    this.jumpState.CurrentJumpTypeAllowedGraceFramesRemainingResetValue = ConvertTimeToFrame(this.currentJumpTypeAllowedAfterFallingGraceTime);
                    this.jumpState.JumpActivatedGraceFramesRemainingResetValue = ConvertTimeToFrame(this.jumpActivatedGraceTime);
                    this.jumpState.NoMovementAfterJumpGraceFramesRemainingResetValue = ConvertTimeToFrame(this.noMovementAfterJumpGraceTime);

                    // Set wall jump varitables
                    this.wallJumpVector = Quaternion.AngleAxis(this.wallJumpAngle, UnityEngine.Vector3.forward) * UnityEngine.Vector3.right;
                    this.wallJumpAwayVector = Quaternion.AngleAxis(this.wallJumpAwayAngle, UnityEngine.Vector3.forward) * UnityEngine.Vector3.right;
                    this.wallState.WallStickFramesRemainingResetValue = ConvertTimeToFrame(this.wallStickDuration);

                    /// Set slope variables.
                    this.rightSlopeCheckDirection = new UnityEngine.Vector2(1f, -1f).normalized;
                    this.leftSlopeCheckDirection = new UnityEngine.Vector2(-1f, -1f).normalized;

                    UnityEngine.Vector3 temp = Quaternion.AngleAxis(this.maxAngleAllowedForSlope, UnityEngine.Vector3.back) * UnityEngine.Vector3.up;
                    this.surroundingState.MaxDotAllowedForSlopes = UnityEngine.Vector3.Dot(UnityEngine.Vector3.up, temp);
                }

                private void FixedUpdate()
                {
#if UNITY_EDITOR
                    this.fixedUpdateSincePlay++;
#endif
 
                    if (IsFrozen || this.timeScale == 0f)
                    {
                        return;
                    }

                    UpdateFrames();

                    /// Updating the collisionMask here allows user to change collision mask at runtime for game mechanic reasons or debugging.
                    /// TODO: Shift this into a function that can update collisionMask and other mechanics stuff in the future.
                    this.collisionMask = this.staticEnvironmentLayerMask | this.hostLayerMask;

                    this.deltaTime = Time.fixedDeltaTime;

                    for (int i = 0; this.deltaTime > 0f && i < this.iterationsPerFixedUpdate; i++)
                    {
                        /// Phase One
                        /// Initial update of the surrounding states. 
                        UpdateSurroundingState();
                        HandleIsSnapToMinDistanceFromEnvironment();

                        /// Phase Two
                        /// Handle Host.
                        /// This will move the motor's transform.position.
                        HandleHost();

                        /// Phase Three
                        /// Check and handle overlapping if there's any.
                        if (IsOverlapping())
                        {
                            HandleOverlapping();
                            UpdateSurroundingState();
                        }

                        /// Phase Four
                        /// These are flags that require updating based on the surrounding states.
                        /// Currently, I am trying to avoid change state here. Will make debugging easier.
                        UpdateFlagsAndCounters();

                        /// Phase Five
                        /// Depending on the current state, handle the input and surrounding state accordingly. These will be done.
                        /// Handle user inputs.
                        /// Handle residuals velocity and frames remaining.
                        /// Update velocity based on the input and surrounding state.
                        /// Change the current state based on the input and surrounding state.
                        HandleCurrentState();

                        /// Phase Six (Move)
                        /// Move the motor to the new location based on the offset, velocity and states updated in earlier phases.
                        MoveMotor();
                    }
                }

#if UNITY_EDITOR
                private void OnDrawGizmos()
                {
                    if (this.isDrawGizmo)
                    {
                        Bounds box = GetComponent<Collider2D>().bounds;

                        {
                            UnityEngine.Vector2 min;
                            UnityEngine.Vector2 max;

                            /// environmentCheckDistance;
                            {
                                UnityEngine.Gizmos.color = Color.white;

                                /// Up
                                min = box.min;
                                max = box.max;
                                max.y += this.environmentCheckDistance;
                                min.y = box.max.y;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                                /// Right
                                min = box.min;
                                max = box.max;
                                min.x = box.max.x;
                                max.x += this.environmentCheckDistance;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                                /// Ground
                                min = box.min;
                                max = box.max;
                                min.y -= this.environmentCheckDistance;
                                max.y = box.min.y;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                                /// Left
                                min = box.min;
                                max = box.max;
                                min.x -= this.environmentCheckDistance;
                                max.x = box.min.x;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));
                            }

                            /// minDistanceFromEnvironment
                            {
                                UnityEngine.Gizmos.color = Color.blue;

                                /// Up
                                min = box.min;
                                max = box.max;
                                max.y += this.minDistanceFromEnvironment;
                                min.y = box.max.y;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                                /// Right
                                min = box.min;
                                max = box.max;
                                min.x = box.max.x;
                                max.x += this.minDistanceFromEnvironment;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                                /// Ground 
                                min = box.min;
                                max = box.max;
                                min.y -= this.minDistanceFromEnvironment;
                                max.y = box.min.y;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                                /// Left 
                                min = box.min;
                                max = box.max;
                                min.x -= this.minDistanceFromEnvironment;
                                max.x = box.min.x;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));
                            }

                            /// Check for overlapping
                            {
                                UnityEngine.Gizmos.color = Color.cyan;
                                min = box.min;
                                max = box.max;
                                min.x -= (this.minDistanceFromEnvironment - CHECK_FOR_OVERLAPPING_OFFSET);
                                min.y -= (this.minDistanceFromEnvironment - CHECK_FOR_OVERLAPPING_OFFSET);
                                max.x += (this.minDistanceFromEnvironment - CHECK_FOR_OVERLAPPING_OFFSET);
                                max.y += (this.minDistanceFromEnvironment - CHECK_FOR_OVERLAPPING_OFFSET);
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));
                            }

                            /// stickToGroundOnSlopeDistance
                            {
                                UnityEngine.Gizmos.color = Color.red;
                                min = box.min;
                                max = box.max;
                                min.y -= this.stickToGroundOnSlopeDistance;
                                max.y = box.min.y;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));
                            }

                            Handles.Label(new UnityEngine.Vector2(box.max.x + GIZMO_LABEL_OFFSET, box.max.y), "fixedUpdateSincePlay: " + this.fixedUpdateSincePlay);
                            Handles.Label(new UnityEngine.Vector2(box.max.x + GIZMO_LABEL_OFFSET, box.max.y - GIZMO_LABEL_OFFSET * 1), "WHITE: environmentCheckDistance");
                            Handles.Label(new UnityEngine.Vector2(box.max.x + GIZMO_LABEL_OFFSET, box.max.y - GIZMO_LABEL_OFFSET * 2), "BLUE: minDistanceFromEnvironment");
                            Handles.Label(new UnityEngine.Vector2(box.max.x + GIZMO_LABEL_OFFSET, box.max.y - GIZMO_LABEL_OFFSET * 3), "RED: stickToGroundOnSlopeDistance");
                        }

                        {
                            /// Show the distance that it will take for the motor to stop on the ground.
                            //UnityEngine.Vector2 from = new UnityEngine.Vector2(box.max.x, box.min.y);
                            //UnityEngine.Gizmos.color = Color.blue;
                            //UnityEngine.Gizmos.DrawLine(from, from + UnityEngine.Vector2.right * this.groundStopDistance);

                            /// Show the distance that it will take for the motor to stop in the air.
                            //from = box.max;
                            //UnityEngine.Gizmos.color = Color.blue;
                            //UnityEngine.Gizmos.DrawLine(from, from + UnityEngine.Vector2.right * this.airStopDistance);
                        }

                        {
                            /// Show the slope check direction and line.
                            UnityEngine.Gizmos.color = Color.cyan;
                            UnityEngine.Vector2 colliderBottomRightPosition = new UnityEngine.Vector2(box.max.x, box.min.y);
                            UnityEngine.Vector2 colliderBottomLeftPosition = box.min;
                            UnityEngine.Gizmos.DrawRay(colliderBottomRightPosition, this.rightSlopeCheckDirection * this.environmentCheckDistanceCorner);
                            UnityEngine.Gizmos.DrawRay(colliderBottomLeftPosition, this.leftSlopeCheckDirection * this.environmentCheckDistanceCorner);
                        }
                    }
                }
#endif

                private void OnEnable()
                {
                    this.velocity = this.rigidbody2D.velocity;
                    this.rigidbody2D.isKinematic = true;
                }

                private void OnDisable()
                {
                    this.rigidbody2D.velocity = this.velocity;
                }
                #endregion

                #region Public Functions
                #region Freeze
                /// <summary>
                /// Set CurrentState to Frozen will put the motor in a 'frozen' state. All information will be saved and set once unfrozen
                /// (the motor also reduces gravity to 0f).
                ///
                /// Note: This isn't a way to turn off the motor. To turn off the motor, simply set the script to disabled.
                /// Note: No delegate (onXXX) will be called.
                /// </summary>
                public void Freeze()
                {
                    CurrentState = CharacterState.Frozen;
                }
                
                public void Thaw()
                {
                    CurrentState = this.previousState;
                }
                #endregion

                #region Jump
                /// <summary>
                /// Call this to have the GameObject try to jump, once called it will be handled in the FixedUpdate tick. 
                /// The y axis is considered jump.
                /// </summary>
                public void Jump()
                {
                    this.jumpState.IsPressed = true;
                    this.jumpState.JumpActivatedGraceFramesRemaining = this.jumpState.JumpActivatedGraceFramesRemainingResetValue;
                    this.jumpState.Height = this.minJumpHeight;

                    /// Assume jumping isHeld in case there are multiple fixed ticks before the next update tick.
                    /// This is useful as jumpingHeld may not be set to true with a GetButton() call.
                    this.jumpState.IsHeld = true;
                }

                /// <summary>
                /// Jump that allows a custom height.
                /// </summary>
                /// <param name="customHeight">The height the motor should jump to. The extraJumpHeight is still applicable.</param>
                public void Jump(float customHeight)
                {
                    Jump();
                    this.jumpState.Height = customHeight;
                }

                /// <summary>
                /// This will force a jump to occur even if the motor doesn't think a jump is valid.
                /// This function will not work if the motor is dashState.
                /// </summary>
                /// TODO: Not supported for now.
                //public void ForceJump()
                //{
                //    Jump();
                //    this.jumpState.IsForce = true;
                //}

                /// <summary>
                /// Force a jump with a custom height.
                /// </summary>
                /// <param name="customHeight">
                /// The height the motor should jump to. The extraJumpHeight is still applicable.
                /// </param>
                /// TODO: Not supported for now.
                //public void ForceJump(float customHeight)
                //{
                //    ForceJump();
                //    this.jumpState.Height = customHeight;
                //}

                /// <summary>
                /// Call to end a jump. Causes the motor to stop calculated isHeld speed for a jump.
                /// </summary>
                public void EndJump()
                {
                    this.jumpState.IsPressed = false;
                    this.jumpState.IsHeld = false;
                    this.jumpState.JumpActivatedGraceFramesRemaining = -1;
                }

                /// <summary>
                /// Resets air jumps by setting the counter to 0.
                /// </summary>
                public void ResetAirJump()
                {
                    this.jumpState.AirJumpCount = 0;
                }
                #endregion

                #region Wall
                /// <summary>
                /// Resets wall stick.
                /// </summary>
                //public void ResetWallStick()
                //{
                //    this.wallState.CanWallStick = true;
                //}
                #endregion

                #region Dash
                /// <summary>
                /// Call this to have the motor try to dash, once called it will be handled in the FixedUpdate tick.
                /// This causes the object to dash along their facing (if left or right for side scrollers).
                /// </summary>
                public void Dash()
                {
                    /// It's better putting this here. Putting this in the Update will mean checking every frame.
                    if (CanDash)
                    {
                        this.dashState.IsPressed = true;
                        this.dashState.IsForceDirection = false;
                    }
                }

                /// <summary>
                /// Forces the motor to dash regardless if the motor thinks it is valid or not.
                /// </summary>
                // TODO: Not supported for now.
                //public void ForceDash()
                //{
                //    Dash();
                //    this.dashState.IsForce = true;
                //}

                /// <summary>
                /// Send a direction vector to dash allow dashState in a specific direction.
                /// </summary>
                /// <param name="dir">The normalized direction of the dash.</param>
                public void Dash(UnityEngine.Vector2 direction)
                {
                    this.dashState.IsPressed = true;
                    this.dashState.IsForceDirection = true;
                    this.dashState.Direction = direction;
                }

                /// <summary>
                /// Forces a dash along a specified direction.
                /// </summary>
                /// <param name="dir">The normalized direction of the dash.</param>
                // TODO: Not supported for now.
                //public void ForceDash(UnityEngine.Vector2 direction)
                //{
                //    Dash(direction);
                //    this.dashState.IsForce = true;
                //}

                /// <summary>
                /// Call to end dash immediately.
                /// </summary>
                public void EndDash()
                {
                    if (IsDashing)
                    {
                        this.dashState.DashingFramesRemaining = -1;

                        // I am trying to leave all the state changes to the HandleCurrentState().
                        //this.dashState.CooldownFramesRemaining = ConvertTimeToFrame(this.dashCooldownTime);
                        //this.dashState.IsPressed = false;
                        //this.dashState.NoGravityFramesRemaining = ConvertTimeToFrame(this.endDashNoGravityTime);

                        //this.velocity = this.dashState.Direction * GetDashSpeed();

                        //CurrentState = this.surroundingState.IsOnGround ? CharacterState.Standing : CharacterState.Falling;
                    }
                }

                /// <summary>
                /// Reset the cooldown for dash.
                /// </summary>
                public void ResetDashCooldown()
                {
                    this.dashState.CooldownFramesRemaining = -1;
                }
                #endregion

                #region Queries
                ///<summary>
                /// Given a game object return if this motor consider the object attachable.
                ///</summary>
                public virtual bool IsHost(UnityEngine.GameObject gameObject)
                {
                    return ((0x1 << gameObject.layer) & this.hostLayerMask) != 0;
                }
                #endregion
                #endregion

                #region Main Loop Functions 
                private void UpdateFrames()
                {
                    /// All timers in the motor are countdowns and are considered valid so long as the timer is >= 0f.
                    this.dashState.CooldownFramesRemaining--;
                    this.dashState.NoGravityFramesRemaining--;
                    this.dashState.DashingFramesRemaining--;
                    this.wallState.WallStickFramesRemaining--;
                    this.wallState.WallInteractionCooldownFramesRemaining--;
                    this.jumpState.NoMovementAfterJumpGraceFramesRemaining--;
                    this.jumpState.JumpActivatedGraceFramesRemaining--;
                    this.jumpState.CurrentJumpTypeAllowedGraceFramesRemaining--;
                    this.jumpState.MaxJumpHeightFramesRemaining--;
                }

                private void UpdateSurroundingState()
                {
                    this.surroundingState.CollidedSurfaces = CollidedSurfaces.None;

                    /// Up 
                    {
                        this.surroundingState.HitUp = GetHit(this.collider2D.bounds.center, UnityEngine.Vector3.up, this.environmentCheckDistance);

                        if (this.surroundingState.HitUp.collider != null)
                        {
                            this.surroundingState.CollidedSurfaces |= CollidedSurfaces.Ceiling;
                        }
                    }

                    /// Right
                    {
                        this.surroundingState.HitRight = GetHit(this.collider2D.bounds.center, UnityEngine.Vector3.right, this.environmentCheckDistance);

                        if (this.surroundingState.HitRight.collider != null)
                        {
                            this.surroundingState.CollidedSurfaces |= CollidedSurfaces.WallRight;
                        }
                    }

                    /// Down
                    {
                        //TODO: revisit later.
                        float distance = this.environmentCheckDistance;
                        //float distance = (this.enableSlopes && this.enableStickToGroundOnSlope && (IsOnGround || IsSlipping) && (collidedSurfaces == CollidedSurfaces.None)) 
                        //    ? this.stickToGroundOnSlopeDistance
                        //    : this.environmentCheckDistance;

                        this.surroundingState.HitDown = GetHit(this.collider2D.bounds.center, UnityEngine.Vector3.down, distance);

                        if (this.surroundingState.HitDown.collider != null)
                        {
                            this.surroundingState.CollidedSurfaces |= CollidedSurfaces.Ground;
                        }
                    }

                    /// Left
                    {
                        this.surroundingState.HitLeft = GetHit(this.collider2D.bounds.center, UnityEngine.Vector3.left, this.environmentCheckDistance);

                        if (this.surroundingState.HitLeft.collider != null)
                        {
                            this.surroundingState.CollidedSurfaces |= CollidedSurfaces.WallLeft;
                        }
                    }

                    /// Slopes check
                    if (this.enableSlope)
                    {
                        /// Only check for slopes if we are at least colliding on the ground, left or right wall.
                        if ((this.surroundingState.CollidedSurfaces & (CollidedSurfaces.Ground | CollidedSurfaces.WallRight | CollidedSurfaces.WallLeft)) != CollidedSurfaces.None)
                        {
                            /// Reset all variables we are going to use to default.
                            this.surroundingState.SlopeNormal = UnityEngine.Vector2.up;
                            UnityEngine.Vector2 rightNormal = UnityEngine.Vector2.zero;
                            UnityEngine.Vector2 leftNormal = UnityEngine.Vector2.zero;

                            /// Check for right slope.
                            UnityEngine.Vector2 colliderBottomRightPosition = new UnityEngine.Vector2(this.collider2D.bounds.max.x, this.collider2D.bounds.min.y);
                            RaycastHit2D hit = GetHit(colliderBottomRightPosition, this.rightSlopeCheckDirection, this.environmentCheckDistanceCorner, false);
                            if (hit.collider != null && hit.normal.x < -ALMOST_ZERO && Mathf.Abs(hit.normal.y) > ALMOST_ZERO)
                            {
                                this.surroundingState.CollidedSurfaces |= CollidedSurfaces.SlopeRight;
                                rightNormal = hit.normal;
                            }

                            /// Check for left slope.
                            UnityEngine.Vector2 colliderBottomLeftPosition = this.collider2D.bounds.min;
                            hit = GetHit(colliderBottomLeftPosition, this.leftSlopeCheckDirection, this.environmentCheckDistanceCorner, false);
                            if (hit.collider != null && hit.normal.x > ALMOST_ZERO && Mathf.Abs(hit.normal.y) > ALMOST_ZERO)
                            {
                                this.surroundingState.CollidedSurfaces |= CollidedSurfaces.SlopeLeft;
                                leftNormal = hit.normal;
                            }

                            /// Motor is standing between 2 slope. If the motor can stand on both slopes then we will consider the gentler slope.
                            if (this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.SlopeLeft) && this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.SlopeRight))
                            {
                                float leftDot = leftNormal.y;
                                float rightDot = rightNormal.y;

                                if (leftDot < this.surroundingState.MaxDotAllowedForSlopes && rightDot < this.surroundingState.MaxDotAllowedForSlopes)
                                {
                                    /// Motor will slip down both slopes.
                                    /// Just let it stand on ground.

                                    this.surroundingState.CollidedSurfaces &= ~(CollidedSurfaces.SlopeLeft | CollidedSurfaces.SlopeRight);
                                    this.surroundingState.CollidedSurfaces |= CollidedSurfaces.Ground;
                                }
                                else if (leftDot >= this.surroundingState.MaxDotAllowedForSlopes && rightDot >= this.surroundingState.MaxDotAllowedForSlopes)
                                {
                                    /// Motor can walk on both slopes.
                                    /// Set to the one it is facing.

                                    if (this.isFacingLeft)
                                    {
                                        this.surroundingState.SlopeNormal = leftNormal;
                                        this.surroundingState.CollidedSurfaces &= ~CollidedSurfaces.SlopeRight;
                                    }
                                    else
                                    {
                                        this.surroundingState.SlopeNormal = rightNormal;
                                        this.surroundingState.CollidedSurfaces &= ~CollidedSurfaces.SlopeLeft;
                                    }
                                }
                                else if (rightDot >= this.surroundingState.MaxDotAllowedForSlopes)
                                {
                                    this.surroundingState.SlopeNormal = rightNormal;
                                    this.surroundingState.CollidedSurfaces &= ~CollidedSurfaces.SlopeLeft;
                                }
                                else
                                {
                                    this.surroundingState.SlopeNormal = leftNormal;
                                    this.surroundingState.CollidedSurfaces &= ~CollidedSurfaces.SlopeRight;
                                }
                            }
                            else if (this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.SlopeRight))
                            {
                                this.surroundingState.SlopeNormal = rightNormal;
                            }
                            else if (this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.SlopeLeft))
                            {
                                this.surroundingState.SlopeNormal = leftNormal;
                            }
                        }
                    }

                    /// Host check
                    if (this.hostLayerMask != 0)
                    {
                        HostController newHost = null;

                        if (this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.Ground) && IsHost(this.surroundingState.HitDown.collider.gameObject))
                        {
                            newHost = this.surroundingState.HitDown.collider.GetComponent<HostController>();
                        }
                        else if (IsPressAgainstLeftWall && IsHost(this.surroundingState.HitLeft.collider.gameObject))
                        {
                            newHost = this.surroundingState.HitLeft.collider.GetComponent<HostController>();
                        }
                        else if (IsPressAgainstRightWall && IsHost(this.surroundingState.HitRight.collider.gameObject))
                        {
                            newHost = this.surroundingState.HitRight.collider.GetComponent<HostController>();
                        }

                        if (newHost != null && newHost != this.surroundingState.Host)
                        {
                            this.surroundingState.Host = newHost;
                            this.surroundingState.HostPreviousPosition = newHost.Position;
                            this.surroundingState.Host.Attach(this);
                        }
                        else if (newHost == null && this.surroundingState.Host != null)
                        {
                            this.surroundingState.Host.Detach(this);
                            this.surroundingState.Host = null;
                        }
                    }
                }

                private void HandleIsSnapToMinDistanceFromEnvironment()
                {
                    //if (this.isSnapToMinDistanceFromEnvironment && !this.surroundingState.HasHost)
                    if (this.isSnapToMinDistanceFromEnvironment)
                    {
                        /// Down
                        if (this.surroundingState.HitDown.collider != null)
                        {
                            this.transform.position += (this.minDistanceFromEnvironment - (this.collider2D.bounds.center.y - this.surroundingState.HitDown.centroid.y)) * UnityEngine.Vector3.up;
                        }
                        /// Right
                        else if (this.surroundingState.HitRight.collider != null)
                        {
                            this.transform.position += (this.minDistanceFromEnvironment - (this.surroundingState.HitRight.centroid.x - this.collider2D.bounds.center.x)) * UnityEngine.Vector3.left;
                        }
                        /// Left
                        else if (this.surroundingState.HitLeft.collider != null)
                        {
                            this.transform.position += (this.minDistanceFromEnvironment - (this.collider2D.bounds.center.x - this.surroundingState.HitLeft.centroid.x)) * UnityEngine.Vector3.right;
                        }
                        /// Up 
                        else if (this.surroundingState.HitUp.collider != null)
                        {
                            this.transform.position += (this.minDistanceFromEnvironment - (this.surroundingState.HitUp.centroid.y - this.collider2D.bounds.center.y)) * UnityEngine.Vector3.down;
                        }
                    }
                }

                private void HandleHost()
                {
                    if (this.surroundingState.HasHost)
                    {
                        //TODO: Figure this out.
                        //if (this.surroundingState.Host.Velocity.y < -this.maxFallSpeed)
                        //{
                        //    this.surroundingState.Host = null;
                        //    this.velocity.y = -this.maxFallSpeed;
                        //}
                        //else
                        //{
                            this.transform.position += (UnityEngine.Vector3)(this.surroundingState.Host.Position - this.surroundingState.HostPreviousPosition);
                            this.surroundingState.HostPreviousPosition = this.surroundingState.Host.Position;
                        //}
                    }
                }

                private bool IsOverlapping()
                {
                    return Physics2D.OverlapArea(this.collider2D.bounds.min, this.collider2D.bounds.max, this.collisionMask) != null;
                }

                private void UpdateFlagsAndCounters()
                {
                    if (!this.surroundingState.IsInAir)
                    {
                        this.jumpState.AirJumpCount = 0;
                        this.fallState.HeightFallen = 0f;
                    }

                    if (this.surroundingState.IsOnGround)
                    {
                        this.jumpState.CurrentJumpType = JumpType.OffGround;
                    }
                    else if (this.enableWallJump)
                    {
                        if (IsPressAgainstLeftWall)
                        {
                            this.jumpState.CurrentJumpType = JumpType.OffLeftWall;
                        }
                        else if (IsPressAgainstRightWall)
                        {
                            this.jumpState.CurrentJumpType = JumpType.OffRightWall;
                        }
                    }

                    if (this.jumpState.JumpActivatedGraceFramesRemaining < 0f)
                    {
                        this.jumpState.IsPressed = false;
                    }
                }

                private void HandleCurrentState()
                {
                    switch (CurrentState)
                    {
                        case CharacterState.Dashing:
                            HandleDashing();
                            break;

                        case CharacterState.Falling:
                            HandleFalling();
                            break;

                        case CharacterState.Frozen:
                            this.isCurrentStateInitialized = true;
                            break;

                        case CharacterState.Jumping:
                            HandleJumping();
                            break;

                        case CharacterState.Slipping:
                            HandleSlipping();
                            break;

                        case CharacterState.Standing:
                            HandleStanding();
                            break;

                        case CharacterState.Walking:
                            HandleWalking();
                            break;

                        case CharacterState.WallSticking:
                            HandleWallSticking();
                            break;

                        case CharacterState.WallSliding:
                            HandleWallSliding();
                            break;
                    }

                    if (!this.isCurrentStateInitialized)
                    {
                        HandleCurrentState();
                    }
                }

                private void HandleDashing()
                {
                    /// Initialize
                    if (!this.isCurrentStateInitialized)
                    {
                        if (!this.dashState.IsForceDirection)
                        {
                            this.dashState.Direction = this.isFacingLeft ? UnityEngine.Vector2.left : UnityEngine.Vector2.right;
                        }

                        this.dashState.DistanceDashed = 0f;
                        this.dashState.DistanceCalculated = 0f;
                        this.dashState.IsForce = false;
                        this.dashState.IsPressed = false;

                        /// This will begin the dash this frame.
                        this.dashState.DashingFramesRemaining = this.dashState.DashingFramesRemainingResetValue;
                    }

                    /// Change CurrentState
                    if (this.dashState.DashingFramesRemaining < 0f)
                    {
                        this.dashState.CooldownFramesRemaining = this.dashState.CooldownFramesRemainingResetValue;
                        this.dashState.NoGravityFramesRemaining = this.dashState.NoGravityFramesRemainingResetValue;

                        this.velocity = this.dashState.Direction * GetDashSpeed();

                        CurrentState = this.surroundingState.IsOnGround ? CharacterState.Standing : CharacterState.Falling;
                    }

                    /// Handle
                    this.isCurrentStateInitialized = true;

                    this.velocity = this.dashState.Direction * GetDashSpeed();

                    /// Dash only moves along a line, doesn't ever need to adjust. We don't need multiple iterations for that.
                    this.dashState.DistanceDashed += (this.collider2D.bounds.center - this.previousPosition).magnitude;
                }

                private void HandleFalling()
                {
                    /// Change CurrentState
                    if (this.dashState.IsPressed)
                    {
                        CurrentState = CharacterState.Dashing;
                        return;
                    }
                    else if (this.surroundingState.IsOnGround)
                    {
                        CurrentState = CharacterState.Walking;
                        return;
                    }
                    else if (this.jumpState.IsPressed && (this.jumpState.AirJumpCount < this.maxAirJumpCount || this.jumpState.CurrentJumpTypeAllowedGraceFramesRemaining > 0f))
                    {
                        CurrentState = CharacterState.Jumping;
                        return;
                    }
                    else if (this.wallState.WallInteractionCooldownFramesRemaining < 0f && (IsPressAgainstLeftWall || IsPressAgainstRightWall))
                    {
                        if (this.enableWallStick)
                        {
                            CurrentState = CharacterState.WallSticking;
                            return;
                        }
                        else if (this.enableWallSlide)
                        {
                            CurrentState = CharacterState.WallSliding;
                            return;
                        }
                    }

                    /// Handle
                    this.isCurrentStateInitialized = true;

                    if (this.velocity.y > -this.maxFallSpeed)
                    {
                        this.velocity.y = Accelerate(this.velocity.y, this.gravityMultiplier * Physics2D.gravity.y, -this.maxFallSpeed);
                    }
                    else if (this.velocity.y < -this.maxFallSpeed)
                    {
                        this.velocity.y = Decelerate(this.velocity.y, Mathf.Abs(this.gravityMultiplier * Physics2D.gravity.y), -this.maxFallSpeed);
                    }

                    HandleMovingInAir();

                    this.fallState.HeightFallen += Mathf.Abs(this.collider2D.bounds.center.y - this.previousPosition.y);
                }

                private void HandleJumping()
                {
                    /// Initialize
                    if (!this.isCurrentStateInitialized)
                    {
                        if ((this.jumpState.CurrentJumpType == JumpType.OffGround && this.jumpState.CurrentJumpTypeAllowedGraceFramesRemaining >= 0f) || this.surroundingState.IsOnGround)
                        {
                            /// Normal jump
                            if (this.surroundingState.IsSlopeTooSteep && !this.enableJumpUpSlopeTooSteep)
                            {
                                this.velocity = this.surroundingState.SlopeNormal * GetSpeedForJumpHeight(this.jumpState.Height);
                            }
                            else
                            {
                                this.velocity.y = GetSpeedForJumpHeight(this.jumpState.Height);
                            }
                        }
                        else if (this.enableWallJump &&
                                ((this.jumpState.CurrentJumpType == JumpType.OffLeftWall && this.jumpState.CurrentJumpTypeAllowedGraceFramesRemaining >= 0f) || IsPressAgainstLeftWall))
                        {
                            if (this.normalizedX > 0f)
                            {
                                /// Jump away the wall
                                this.velocity = this.wallJumpAwayVector * GetSpeedForJumpHeight(this.jumpState.Height) * this.wallJumpMultiplier;
                            }
                            else
                            {
                                /// Regular wall jump
                                this.velocity = this.wallJumpVector * GetSpeedForJumpHeight(this.jumpState.Height) * this.wallJumpMultiplier;
                            }

                            this.jumpState.NoMovementAfterJumpGraceFramesRemaining = this.jumpState.NoMovementAfterJumpGraceFramesRemainingResetValue;

                            if (this.OnWallJump != null)
                            {
                                this.OnWallJump(this.surroundingState.HitRight.normal);
                            }
                        }
                        else if (this.enableWallJump &&
                                ((this.jumpState.CurrentJumpType == JumpType.OffRightWall && this.jumpState.CurrentJumpTypeAllowedGraceFramesRemaining >= 0f) || IsPressAgainstRightWall))
                        {
                            if (this.normalizedX < 0f)
                            {
                                /// Jump away the wall
                                this.velocity = this.wallJumpAwayVector * GetSpeedForJumpHeight(this.jumpState.Height) * this.wallJumpMultiplier;
                            }
                            else
                            {
                                /// Regular wall jump
                                this.velocity = this.wallJumpVector * GetSpeedForJumpHeight(this.jumpState.Height) * this.wallJumpMultiplier;
                            }

                            this.velocity.x *= -1f;

                            this.jumpState.NoMovementAfterJumpGraceFramesRemaining = this.jumpState.NoMovementAfterJumpGraceFramesRemainingResetValue;

                            if (this.OnWallJump != null)
                            {
                                this.OnWallJump(this.surroundingState.HitLeft.normal);
                            }
                        }
                        else if (this.jumpState.AirJumpCount < this.maxAirJumpCount)
                        {
                            /// Air jump
                            this.velocity.y = GetSpeedForJumpHeight(this.jumpState.Height);
                            this.jumpState.AirJumpCount++;

                            if (this.OnAirJump != null)
                            {
                                this.OnAirJump();
                            }
                        }

                        // TODO: Not supported for now.
                        //this.jumpState.IsForce = false;
                        this.jumpState.MaxJumpHeightFramesRemaining = ConvertTimeToFrame(this.maxJumpHeight / GetSpeedForJumpHeight(this.jumpState.Height));
                        this.jumpState.HeightJumped = 0f;
                        this.jumpState.IsPressed = false;
                        // TODO: revisit one day.
                        //this.movingPlatformState.Platform = null;
                        this.jumpState.CurrentJumpType = JumpType.None;
                        this.jumpState.JumpActivatedGraceFramesRemaining = -1;
                    }

                    /// Change CurrentState
                    if (this.dashState.IsPressed)
                    {
                        CurrentState = CharacterState.Dashing;
                        return;
                    }
                    else if (this.velocity.y > 0f && this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.Ceiling))
                    {
                        ///  So motor don't go through the ceiling.
                        this.velocity.y = 0f;
                        CurrentState = CharacterState.Falling;
                        return;
                    }
                    else if (!this.jumpState.IsHeld || this.jumpState.MaxJumpHeightFramesRemaining < 0f)
                    {
                        CurrentState = CharacterState.Falling;
                        return;
                    }

                    /// Handle
                    this.isCurrentStateInitialized = true;

                    HandleMovingInAir();

                    this.jumpState.HeightJumped = Mathf.Abs(this.collider2D.bounds.center.y - this.previousPosition.y);
                }

                private void HandleSlipping()
                {
                    /// Change CurrentState
                    if (this.dashState.IsPressed)
                    {
                        CurrentState = CharacterState.Dashing;
                        return;
                    }
                    else if (this.surroundingState.IsInAir)
                    {
                        CurrentState = CharacterState.Falling;
                        return;
                    }
                    else if (this.jumpState.IsPressed)
                    {
                        CurrentState = CharacterState.Jumping;
                        return;
                    }
                    else if (!this.surroundingState.IsSlopeTooSteep)
                    {
                        CurrentState = CharacterState.Walking;
                        return;
                    }

                    /// Handle
                    this.isCurrentStateInitialized = true;

                    UnityEngine.Vector2 downSlopeDirection = this.surroundingState.SlopeDirection;
                    float increaseBy = -downSlopeDirection.y * this.gravityMultiplier * Mathf.Abs(Physics2D.gravity.y) * TimeScaleAdjustedDeltaTime;
                    this.velocity += downSlopeDirection * increaseBy;

                    if (UnityEngine.Vector2.Dot(GetForwardDirection(this.velocity.x), downSlopeDirection) > ALMOST_ZERO)
                    {
                        this.velocity = UnityEngine.Vector2.ClampMagnitude(this.velocity, -downSlopeDirection.y * this.maxFallSpeed);
                    }
                }

                private void HandleStanding()
                {
                    /// Change CurrentState
                    if (this.dashState.IsPressed)
                    {
                        CurrentState = CharacterState.Dashing;
                        return;
                    }
                    else if (!this.surroundingState.IsOnGround)
                    {
                        CurrentState = CharacterState.Falling;
                        return;
                    }
                    else if (this.normalizedX != 0f)
                    {
                        CurrentState = CharacterState.Walking;
                        return;
                    }
                    else if (this.jumpState.IsPressed)
                    {
                        CurrentState = CharacterState.Jumping;
                        return;
                    }
                    else if (this.surroundingState.IsSlopeTooSteep)
                    {
                        CurrentState = CharacterState.Slipping;
                        return;
                    }

                    /// Handle
                    this.isCurrentStateInitialized = true;

                    this.velocity = UnityEngine.Vector2.zero;
                }

                private void HandleWalking()
                {
                    /// Change CurrentState
                    if (this.dashState.IsPressed)
                    {
                        CurrentState = CharacterState.Dashing;
                        return;
                    }
                    else if (this.surroundingState.IsInAir)
                    {
                        CurrentState = CharacterState.Falling;
                        return;
                    }
                    else if (this.jumpState.IsPressed)
                    {
                        CurrentState = CharacterState.Jumping;
                        return;
                    }
                    else if (this.surroundingState.IsSlopeTooSteep)
                    {
                        CurrentState = CharacterState.Slipping;
                        return;
                    }

                    /// Handle
                    this.isCurrentStateInitialized = true;

                    if (this.jumpState.NoMovementAfterJumpGraceFramesRemaining >= 0f)
                    {
                        /// Apply movement only if we're not ignoring it.
                        return;
                    }
                    else if (Mathf.Abs(this.normalizedX) > 0f)
                    {
                        UnityEngine.Vector2 direction = GetForwardDirection(this.normalizedX);

                        float speed = GetWalkingSpeed();
                        float maxSpeed = GetMaxWalkingSpeed();

                        if (this.timeToReachGroundSpeed > 0f)
                        {
                            /// If we're moving faster than our normalizedX * maxGroundSpeed
                            /// or, if we are trying to move in the direction opposite of where we are facing.
                            /// Then decelerate rather than accelerate.
                            if ((speed > 0f && this.normalizedX > 0f && speed > this.normalizedX * maxSpeed) ||
                                (speed < 0f && this.normalizedX < 0f && speed < this.normalizedX * maxSpeed) ||
                                (speed < 0f && this.normalizedX > 0f) ||
                                (speed > 0f && this.normalizedX < 0f))
                            {
                                float deceleration = (maxSpeed * maxSpeed) / (2 * this.groundStopDistance);

                                if (this.surroundingState.IsOnSlope && this.enableChangeSpeedOnSlope)
                                {
                                    float factor = (this.changeSpeedOnSlopeMultiplier * (1f - this.surroundingState.SlopeNormal.y));

                                    if (direction.y > 0f)
                                    {
                                        deceleration /= factor;
                                    }
                                    else
                                    {
                                        deceleration *= factor;
                                    }
                                }

                                speed = Decelerate(speed, deceleration, this.normalizedX * maxSpeed);
                            }
                            else
                            {
                                float acceleration = this.normalizedX * (maxSpeed / this.timeToReachGroundSpeed);

                                if (this.surroundingState.IsOnSlope && this.enableChangeSpeedOnSlope)
                                {
                                    float factor = (this.changeSpeedOnSlopeMultiplier * (1f - this.surroundingState.SlopeNormal.y));

                                    if (direction.y < 0f)
                                    {
                                        acceleration /= factor;
                                    }
                                    else
                                    {
                                        acceleration *= factor;
                                    }
                                }

                                speed = Accelerate(speed, acceleration, this.normalizedX * maxSpeed);
                            }

                            this.velocity = GetForwardDirection(this.normalizedX) * Mathf.Abs(speed);
                            //this.velocity = GetForwardDirection(speed) * Mathf.Abs(speed);
                        }
                        else
                        {
                            /// GetForwardDirection(this.normalizedX) gives us the direction to move towards. This includes ascending and descending y for slope.
                            /// Mathf.Abs(this.normalizedX * maxSpeed) because we are moving up to max ground speed depending on how hard the button is pressed.
                            this.velocity = GetForwardDirection(this.normalizedX) * Mathf.Abs(this.normalizedX * maxSpeed);
                            //this.velocity = GetForwardDirection(speed) * Mathf.Abs(this.normalizedX * maxSpeed);
                        }
                    }
                    else if (this.velocity.x != 0f)
                    {
                        float speed = GetWalkingSpeed();
                        //float maxSpeed = GetMaxWalkingSpeed();

                        if (this.groundStopDistance > 0f)
                        {
                            float deceleration = (this.maxGroundSpeed * this.maxGroundSpeed) / (2f * this.groundStopDistance);

                            if (this.surroundingState.IsOnSlope && this.enableChangeSpeedOnSlope)
                            {
                                float factor = (this.changeSpeedOnSlopeMultiplier * this.surroundingState.SlopeNormal.y) / 2f;

                                if (GetForwardDirection(this.velocity.x).y > 0f)
                                {
                                    deceleration /= factor;
                                }
                                else
                                {
                                    deceleration *= factor;
                                }
                            }

                            speed = Decelerate(speed, deceleration, 0f);
                        }
                        else
                        {
                            speed = 0f;
                        }

                        this.velocity = GetForwardDirection(speed) * Mathf.Abs(speed);
                    }
                    else
                    {
                        /// I don't like to place a change state logic here, but it allow us to skip 2 checks.
                        CurrentState = CharacterState.Standing;
                        return;
                    }

                    /// I put this here for a safe guard so that velocity is killed before the move step. Not sure if it's necessary.
                    /// These mean we can't progress forward. Either a wall or a slope
                    if (this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.WallLeft) && this.velocity.x < 0f && this.surroundingState.HitLeft.normal == UnityEngine.Vector2.right ||
                        (this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.WallRight) && this.velocity.x > 0f && this.surroundingState.HitRight.normal == UnityEngine.Vector2.left))
                    {
                        this.velocity.x = 0f;
                    }

                    // Might be the solution behind #82
                    //var minimumSpeedToMoveUpSlipperySlope = 7.5f;

                    //if (this.surroundingState.IsOnGround &&

                    //     //this.disallowedSlopeNormal != UnityEngine.Vector2.zero &&
                    //     //(this.disallowedSlopeNormal.x < 0 && this.velocity.x > 0 || this.disallowedSlopeNormal.x > 0 && this.velocity.x < 0) &&
                    //     this.velocity.sqrMagnitude < minimumSpeedToMoveUpSlipperySlope * minimumSpeedToMoveUpSlipperySlope)
                    //{
                    //    this.velocity = UnityEngine.Vector2.zero;
                    //}
                }

                private void HandleWallSticking()
                {
                    /// Initialize
                    if (!this.isCurrentStateInitialized)
                    {
                        this.wallState.WallStickFramesRemaining = this.wallState.WallStickFramesRemainingResetValue;
                        this.velocity = UnityEngine.Vector2.zero;
                    }

                    /// Change CurrentState
                    if (this.dashState.IsPressed)
                    {
                        CurrentState = CharacterState.Dashing;
                        return;
                    }
                    else if (!IsPressAgainstLeftWall && !IsPressAgainstRightWall)
                    {
                        CurrentState = CharacterState.Falling;
                        return;
                    }
                    else if (this.enableWallJump && this.jumpState.IsPressed)
                    {
                        CurrentState = CharacterState.Jumping;
                        return;
                    }
                    else if (this.wallState.WallStickFramesRemaining < 0f)
                    {
                        CurrentState = this.enableWallSlide ? CharacterState.WallSliding : CharacterState.Falling;
                    }
                    else if (this.surroundingState.IsOnGround)
                    {
                        CurrentState = CharacterState.Standing;
                    }

                    /// Handle
                    this.isCurrentStateInitialized = true;
                }

                private void HandleWallSliding()
                {
                    /// Change CurrentState
                    if (this.dashState.IsPressed)
                    {
                        CurrentState = CharacterState.Dashing;
                        return;
                    }
                    else if (!IsPressAgainstLeftWall && !IsPressAgainstRightWall)
                    {
                        CurrentState = CharacterState.Falling;
                        return;
                    }
                    else if (this.enableWallJump && this.jumpState.IsPressed)
                    {
                        CurrentState = CharacterState.Jumping;
                        return;
                    }

                    /// Handle
                    this.isCurrentStateInitialized = true;

                    if (this.timeToReachWallSlideSpeed > 0f)
                    {
                        if (this.velocity.y > -this.wallSlideSpeed)
                        {
                            this.velocity = UnityEngine.Vector2.up * Accelerate(this.velocity.y, -this.wallSlideSpeed / this.timeToReachWallSlideSpeed, -this.wallSlideSpeed);
                        }
                        else
                        {
                            this.velocity = UnityEngine.Vector2.up * Decelerate(this.velocity.y, Mathf.Abs(this.wallSlideSpeed / this.timeToReachWallSlideSpeed), -this.wallSlideSpeed);
                        }
                    }
                    else
                    {
                        this.velocity = UnityEngine.Vector2.down * this.wallSlideSpeed;
                    }
                }

                #region Move Functions
                private void MoveMotor()
                {
                    UnityEngine.Vector3 currentPosition = this.collider2D.bounds.center;
                    UnityEngine.Vector3 targetPosition = this.collider2D.bounds.center + (UnityEngine.Vector3)this.velocity * TimeScaleAdjustedDeltaTime;

                    MovePosition(targetPosition);

                    if (IsDashing || 
                        ((targetPosition - this.collider2D.bounds.center).sqrMagnitude < (THRESHOLD_DISTANCE_TO_END_ITERATION_SQUARED)))
                    {
                        this.deltaTime = 0f;
                    }
                    else
                    {
                        this.deltaTime = Mathf.Lerp(this.deltaTime, 0, (this.collider2D.bounds.center - currentPosition).magnitude / (targetPosition - currentPosition).magnitude);
                    }
                }

                private void MovePosition(UnityEngine.Vector3 newPosition)
                {
                    this.previousPosition = this.collider2D.bounds.center;

                    if (newPosition == this.collider2D.bounds.center)
                    {
                        return;
                    }

                    UnityEngine.Vector3 differenceToNewPosition = newPosition - this.collider2D.bounds.center;
                    float distanceToNewPosition = differenceToNewPosition.magnitude;
                    RaycastHit2D hit = GetHit(this.collider2D.bounds.center, differenceToNewPosition / distanceToNewPosition, distanceToNewPosition);

                    if (hit.collider != null)
                    {
                        this.transform.position = (UnityEngine.Vector3)hit.centroid + (UnityEngine.Vector3)hit.normal * this.minDistanceFromEnvironment;
                    }
                    else
                    {
                        this.transform.position = newPosition;
                    }
                }
                #endregion
                #endregion

                #region Helper Functions
                private void HandleMovingInAir()
                {
                    if (this.jumpState.NoMovementAfterJumpGraceFramesRemaining >= 0f)
                    {
                        return;
                    }

                    if (Mathf.Abs(this.normalizedX) > 0f)
                    {
                        if (this.enableCanMoveInAir)
                        {
                            if (this.timeToReachAirSpeed > 0f)
                            {
                                if ((this.velocity.x > 0f && this.normalizedX > 0f && (this.velocity.x > (this.normalizedX * this.maxAirSpeed))) ||
                                    (this.velocity.x < 0f && this.normalizedX < 0f && (this.velocity.x < (this.normalizedX * this.maxAirSpeed))))
                                {
                                    this.velocity.x = Decelerate(this.velocity.x,
                                        (this.maxAirSpeed * this.maxAirSpeed) / (2f * this.airStopDistance),
                                        this.normalizedX * this.maxAirSpeed);
                                }
                                else
                                {
                                    this.velocity.x = Accelerate(
                                        this.velocity.x,
                                        this.normalizedX * (this.maxAirSpeed / this.timeToReachAirSpeed),
                                        this.normalizedX * this.maxAirSpeed);
                                }
                            }
                            else
                            {
                                this.velocity.x = this.normalizedX * this.maxAirSpeed;
                            }
                        }
                    }
                    else if (this.velocity.x != 0f)
                    {
                        {
                            if (this.airStopDistance > 0f)
                            {
                                this.velocity.x = Decelerate(this.velocity.x, (this.maxAirSpeed * this.maxAirSpeed) / (2f * this.airStopDistance), 0f);
                            }
                            else
                            {
                                this.velocity.x = 0f;
                            }
                        }
                    }

                    /// I put this here for a safe guard so that velocity is killed before the move step. Not sure if it's necessary.
                    /// These mean we can't progress forward. Either a wall or a slope
                    if (this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.WallLeft) && this.velocity.x < 0f && this.surroundingState.HitLeft.normal == UnityEngine.Vector2.right || 
                        (this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.WallRight) && this.velocity.x > 0f && this.surroundingState.HitRight.normal == UnityEngine.Vector2.left))
                    {
                        this.velocity.x = 0f;
                    }
                }

                private RaycastHit2D GetHit(UnityEngine.Vector2 origin, UnityEngine.Vector3 direction, float distance, bool isUseBoxCast = true, bool checkWereTouching = false)
                {
                    if (!this.enableOneWayPlatform && this.isOneWayPlatformConsiderWall)
                    {
                        return isUseBoxCast ? Physics2D.BoxCast(origin, this.collider2D.bounds.size, 0f, direction, distance, this.collisionMask)
                                            : Physics2D.Raycast(origin, direction, distance, this.collisionMask);
                    }

                    bool isOverlappingCollidersUpdated = false;
                    int overlappingCollidersCount = 0;
                    RaycastHit2D hit = new RaycastHit2D();
                    float hitDistance = float.MaxValue;

                    int hitCount = isUseBoxCast
                        ? RaycastPhysics2D.Common.Util.BoxCastNonAllocAutoResize(origin, this.collider2D.bounds.size, 0f, direction, this.hits, distance, this.collisionMask)
                        : RaycastPhysics2D.Common.Util.RaycastNonAllocHitsAutoResize(origin, direction, this.hits, distance, this.collisionMask);

                    /// For all hit owp colliders, skip if they are overlapped with player.
                    for (int i = 0; i < hitCount; i++)
                    {
                        if (this.hits[i].collider.usedByEffector &&
                            this.hits[i].collider.GetComponent<PlatformEffector2D>().useOneWay)
                        {
                            if (!this.isOneWayPlatformConsiderWall)
                            {
                                continue;
                            }

                            if (!isOverlappingCollidersUpdated)
                            {
                                overlappingCollidersCount = RaycastPhysics2D.Common.Util.OverlapAreaNonAllocAutoResize(
                                    this.collider2D.bounds.min + new UnityEngine.Vector3(CHECK_TOUCHING_TRIM, CHECK_TOUCHING_TRIM),
                                    this.collider2D.bounds.max + new UnityEngine.Vector3(-CHECK_TOUCHING_TRIM, -CHECK_TOUCHING_TRIM),
                                    this.overlappingColliders,
                                    this.collisionMask);
                                isOverlappingCollidersUpdated = true;
                            }

                            bool isOverlapped = Libraries.Collections.Arrays.Util.Exists(this.overlappingColliders, e => e == this.hits[i].collider, 0, overlappingCollidersCount);

                            if (isOverlapped)
                            {
                                // TODO not sure if we need this.
                                //if (checkWereTouching && ((1 << RaycastPhysicsMotorComponent.hits[i].collider.gameObject.layer) & this.movingPlatformLayerMask) != 0)
                                //{
                                //    // If it's a moving platform then we need to know if we were touching.
                                //    RaycastPhysicsMovingPlatformMotorComponent mpMotor = RaycastPhysicsMotorComponent.hits[i].collider.GetComponent<RaycastPhysicsMovingPlatformMotorComponent>();
                                //    UnityEngine.Vector3 curPos = mpMotor.transform.position;
                                //    mpMotor.transform.position = mpMotor.PreviousPosition;
                                //    bool wasTouching = false;

                                //    numOfNoDistanceHits = GetOverlappingColliders();
                                //    haveGotOverlapping = false;

                                //    mpMotor.transform.position = curPos;

                                //    for (int j = 0; j < numOfNoDistanceHits; j++)
                                //    {
                                //        if (RaycastPhysicsMotorComponent.overlappingColliders[j] == RaycastPhysicsMotorComponent.hits[i].collider)
                                //        {
                                //            wasTouching = true;
                                //            break;
                                //        }
                                //    }

                                //    if (wasTouching)
                                //    {
                                //        continue;
                                //    }

                                //}
                                //else
                                //{
                                continue;
                                //}
                            }

                            /// Implemented to block/ignore based on platform effector's rotational offset.
                            /// This only works on one side of the platform. The side that is a wall.
                            UnityEngine.Vector2 platformEffector2DRotationOffset = Vector2.Util.DegreeToVector2(this.hits[i].collider.GetComponent<PlatformEffector2D>().rotationalOffset + 90);
                            UnityEngine.Vector2 platformEffectorCollidingVector = this.hits[i].collider.transform.TransformDirection(platformEffector2DRotationOffset);
                            float dotProduct = 0f;

                            if (this.velocity != UnityEngine.Vector2.zero)
                            {
                                dotProduct = UnityEngine.Vector2.Dot(platformEffectorCollidingVector, this.velocity);
                            }
                            // TODO not sure if we need this.
                            //else if (((1 << this.hits[i].collider.gameObject.layer) & this.movingPlatformLayerMask) != 0)
                            //{
                            //    // If we aren't moving then it's interesting if it's a moving platform.
                            //    RaycastPhysicsMovingPlatformMotorComponent mpMotor = RaycastPhysicsMotorComponent.hits[i].collider.GetComponent<RaycastPhysicsMovingPlatformMotorComponent>();

                            //    if (this.movingPlatformState.platform != mpMotor)
                            //    {
                            //        // This might break for more complicated one way platform moving platforms but it'll have to do.
                            //        if (mpMotor.Velocity != UnityEngine.Vector2.zero)
                            //        {
                            //            dot = UnityEngine.Vector3.Dot(oneWayPlatformForward, -Velocity);
                            //        }
                            //        else
                            //        {
                            //            UnityEngine.Vector2 toNewPlatformPos = mpMotor.Position - mpMotor.PreviousPosition;
                            //            dot = UnityEngine.Vector3.Dot(oneWayPlatformForward, -toNewPlatformPos);
                            //        }
                            //    }
                            //}

                            if (dotProduct > ALMOST_ZERO)
                            {
                                continue;
                            }
                        }

                        UnityEngine.Vector2 currentHitDistance = this.collider2D.bounds.center - (UnityEngine.Vector3)this.hits[i].centroid;

                        if (currentHitDistance.sqrMagnitude < hitDistance)
                        {
                            hitDistance = currentHitDistance.sqrMagnitude;
                            hit = this.hits[i];
                        }
                    }

                    return hit;
                }

                private float Accelerate(float speed, float acceleration, float limit)
                {
                    /// Acceleration can be negative or positive. Negative means accelerating to the left.
                    speed += acceleration * TimeScaleAdjustedDeltaTime;

                    if (acceleration > 0f)
                    {
                        if (speed > limit)
                        {
                            speed = limit;
                        }
                    }
                    else
                    {
                        if (speed < limit)
                        {
                            speed = limit;
                        }
                    }

                    return speed;
                }

                private float Decelerate(float speed, float deceleration, float limit)
                {
                    /// Deceleration is always positive.
                    if (speed < 0f)
                    {
                        speed += deceleration * TimeScaleAdjustedDeltaTime;

                        if (speed > limit)
                        {
                            speed = limit;
                        }
                    }
                    else if (speed > 0f)
                    {
                        speed -= deceleration * TimeScaleAdjustedDeltaTime;

                        if (speed < limit)
                        {
                            speed = limit;
                        }
                    }

                    return speed;
                }

                /// <summary>
                /// This direction includes slope ascending and descending, not just the x sign.
                /// </summary>
                /// <param name="speed"></param>
                /// <returns></returns>
                private UnityEngine.Vector2 GetForwardDirection(float speed)
                {
                    UnityEngine.Vector2 direction = UnityEngine.Vector3.zero;

                    float multiplier = Mathf.Sign(speed);

                    if (speed == 0f)
                    {
                        multiplier = Mathf.Sign(this.normalizedX);
                        speed = this.normalizedX;
                    }

                    if (!this.surroundingState.IsOnSlope &&
                        ((!this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.SlopeLeft) && !this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.SlopeRight)) ||
                        (this.isFacingLeft && this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.SlopeRight)) ||
                        (!this.isFacingLeft && this.surroundingState.IsCollidedWithSurface(CollidedSurfaces.SlopeLeft)) ||
                        (UnityEngine.Vector2.Dot(UnityEngine.Vector2.up, this.surroundingState.SlopeNormal) < this.surroundingState.MaxDotAllowedForSlopes)))
                    {
                        return Mathf.Sign(speed) * UnityEngine.Vector2.right;
                    }

                    direction.x = multiplier * this.surroundingState.SlopeNormal.y;

                    if (this.surroundingState.SlopeNormal.x * speed > 0f)
                    {
                        direction.y = -Mathf.Abs(this.surroundingState.SlopeNormal.x);
                    }
                    else
                    {
                        direction.y = Mathf.Abs(this.surroundingState.SlopeNormal.x);
                    }

                    return direction;
                }

                private float GetWalkingSpeed()
                {
                    return this.surroundingState.IsOnSlope
                        ? this.velocity.magnitude * Mathf.Sign(this.velocity.x)
                        : this.velocity.x;
                }

                private float GetMaxWalkingSpeed()
                {
                    if (this.surroundingState.IsOnSlope)
                    {
                        UnityEngine.Vector2 direction = GetForwardDirection(this.velocity.x);

                        if (this.surroundingState.IsSlopeTooSteep && UnityEngine.Vector2.Dot(direction, this.surroundingState.SlopeDirection) > ALMOST_ZERO)
                        {
                            return this.enableChangeSpeedOnSlope
                                ? -this.surroundingState.SlopeDirection.y * this.maxFallSpeed
                                :  this.maxFallSpeed;
                        }
                        else if (this.enableChangeSpeedOnSlope)
                        {
                            return (direction.y > 0f) 
                                ? this.maxGroundSpeed * this.surroundingState.SlopeNormal.y * this.changeSpeedOnSlopeMultiplier
                                : this.maxGroundSpeed * (2f - (this.surroundingState.SlopeNormal.y * this.changeSpeedOnSlopeMultiplier));
                        }
                    }

                    return this.maxGroundSpeed;
                }

                private int ConvertTimeToFrame(float time)
                {
                    return Mathf.RoundToInt(FrameService.ConvertTimeToFrame(time) / (this.timeScale != 0f ? this.timeScale : DEFAULT_TIME_SCALE));
                }

                private float GetSpeedForJumpHeight(float height)
                {
                    return Mathf.Sqrt(-2 * height * this.gravityMultiplier * Physics2D.gravity.y);
                }
                
                private float GetDashSpeed()
                {
                    float normalizedTime = ((float)this.dashState.DashingFramesRemainingResetValue - this.dashState.DashingFramesRemaining) / this.dashState.DashingFramesRemainingResetValue;
                    float speed = this.dashDerivativeFunction(0, this.dashDistance, normalizedTime) / this.dashDuration;

                    /// Some of the easing functions may result in infinity, we'll uh, lower our expectations and make it maxfloat.
                    /// This will almost certainly be clamped.
                    if (float.IsNegativeInfinity(speed))
                    {
                        speed = float.MinValue;
                    }
                    else if (float.IsPositiveInfinity(speed))
                    {
                        speed = float.MaxValue;
                    }

                    return speed;
                }

                #region Handle Overlapped
                protected virtual void HandleOverlapping()
                {
                    /// 4 Corners check are mandatory.
                    RaycastAndSeparate(this.centerToTopRightCornerVector, this.centerToMinDistanceFromEnvironmentCornerMagnitude);
                    RaycastAndSeparate(this.centerToBottomRightCornerVector, this.centerToMinDistanceFromEnvironmentCornerMagnitude);
                    RaycastAndSeparate(this.centerToBottomLeftCornerVector, this.centerToMinDistanceFromEnvironmentCornerMagnitude);
                    RaycastAndSeparate(this.centerToTopLeftCornerVector, this.centerToMinDistanceFromEnvironmentCornerMagnitude);

                    /// Top/Bottom
                    for (int i = 0; i < this.raycastCountPerSideForOverlapHandling; i++)
                    {
                        UnityEngine.Vector2 direction = new UnityEngine.Vector2(
                            Mathf.Lerp(this.collider2D.bounds.min.x, this.collider2D.bounds.max.x, (float)(i + 1) / (this.raycastCountPerSideForOverlapHandling + 1)),
                            this.collider2D.bounds.max.y) - (UnityEngine.Vector2)this.collider2D.bounds.center;

                        float distance = direction.magnitude;

                        RaycastAndSeparate(direction / distance, distance + this.minDistanceFromEnvironment);
                        RaycastAndSeparate(-direction / distance, distance + this.minDistanceFromEnvironment);
                    }

                    /// Right/Left
                    for (int i = 0; i < this.raycastCountPerSideForOverlapHandling; i++)
                    {
                        UnityEngine.Vector2 direction = new UnityEngine.Vector2(
                            this.collider2D.bounds.max.x,
                            Mathf.Lerp(this.collider2D.bounds.min.y, this.collider2D.bounds.max.y,
                            (float)(i + 1) / (this.raycastCountPerSideForOverlapHandling + 1))) - (UnityEngine.Vector2)this.collider2D.bounds.center;

                        float distance = direction.magnitude;

                        RaycastAndSeparate(direction / distance, distance + this.minDistanceFromEnvironment);
                        RaycastAndSeparate(-direction / distance, distance + this.minDistanceFromEnvironment);
                    }
                }

                protected virtual void RaycastAndSeparate(UnityEngine.Vector2 direction, float distance)
                {
                    RaycastHit2D hit = GetHit(this.collider2D.bounds.center, direction, distance, false, true);

                    if (hit.collider != null)
                    {
                        UnityEngine.Vector2 pointOnCollision = GetPointOnBounds(this.collider2D.bounds, -hit.normal);
                        UnityEngine.Vector3 toPointOnCollision = pointOnCollision - hit.point;
                        UnityEngine.Vector3 pointToSeparateFrom = (UnityEngine.Vector3)hit.point + UnityEngine.Vector3.Project(toPointOnCollision, -hit.normal);

                        this.transform.position += ((UnityEngine.Vector3)hit.point - pointToSeparateFrom) + (UnityEngine.Vector3)hit.normal * this.minDistanceFromEnvironment * 2;
                    }
                }

                protected static UnityEngine.Vector2 GetPointOnBounds(Bounds bounds, UnityEngine.Vector3 toPoint)
                {
                    /// From http://stackoverflow.com/questions/4061576/finding-points-on-a-rectangle-at-a-given-angle
                    float angle = UnityEngine.Vector3.Angle(UnityEngine.Vector3.right, toPoint);

                    if (toPoint.y < 0)
                    {
                        angle = 360f - angle;
                    }

                    float multiplier = 1f;

                    if ((angle >= 0f && angle < 45f) || angle > 315f || (angle >= 135f && angle < 225f))
                    {
                        if (angle >= 135f && angle < 225f)
                        {
                            multiplier = -1f;
                        }

                        return new UnityEngine.Vector2(
                            multiplier * bounds.size.x / 2 + bounds.center.x,
                            bounds.center.y + multiplier * ((bounds.size.x / 2) * Mathf.Tan(angle * Mathf.Deg2Rad)));
                    }

                    if (angle >= 225f)
                    {
                        multiplier = -1f;
                    }

                    return new UnityEngine.Vector2(
                        bounds.center.x + multiplier * bounds.size.y / (2 * Mathf.Tan(angle * Mathf.Deg2Rad)),
                        multiplier * bounds.size.y / 2 + bounds.center.y);
                }
                #endregion
                #endregion
            }
        }
    }
}