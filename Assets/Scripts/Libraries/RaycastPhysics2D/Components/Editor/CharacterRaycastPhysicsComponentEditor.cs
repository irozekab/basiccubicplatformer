﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;
using Libraries.Services;

namespace Libraries
{
    namespace RaycastPhysics2D
    {
        namespace Components
        {
            [UnityEditor.CustomEditor(typeof(CharacterRaycastPhysicsComponent))]
            [CanEditMultipleObjects]
            public class RaycastPhysicsCharacterComponentEditor : UnityEditor.Editor
            {
                private const float MINIMUM_DISTANCE_CHECK = 0.01f;

                private class Property
                {
                    public string name;
                    public string text;

                    public Property(string name, string text)
                    {
                        this.name = name;
                        this.text = text;
                    }
                }

                private Dictionary<string, SerializedProperty> properties = new Dictionary<string, SerializedProperty>();
                private List<Property> timingProperties = new List<Property>();

                private static bool isShowGeneral;
                private static bool isShowAir;
                private static bool isShowGround;
                private static bool isShowSlope;
                private static bool isShowJump;
                private static bool isShowWall;
                private static bool isShowDash;
                private static bool isShowOneWayPlatform;
                private static bool isShowHost;

                private static bool isShowInformation;

                #region Properties
                #region General
                private readonly Property STATIC_ENVIRONMENT_LAYER_MASK = new Property("staticEnvironmentLayerMask", "Static Environment Layer Mask");
                private readonly Property TIME_SCALE = new Property("timeScale", "Time Scale");
                private readonly Property ITERATIONS_PER_FIXED_UPDATE = new Property("iterationsPerFixedUpdate", "Iterations Per Fixed Update");
                private readonly Property ENVIRONMENT_CHECK_DISTANCE = new Property("environmentCheckDistance", "Environment Check Distance");
                private readonly Property MIN_DISTANCE_FROM_ENVIRONMENT = new Property("minDistanceFromEnvironment", "Min Distance From Environment");
                private readonly Property IS_SNAP_TO_MIN_DISTANCE_FROM_ENVIRONMENT = new Property("isSnapToMinDistanceFromEnvironment", "Is Snap To Min Distance From Environment");
                // TODO: this should just be in information
                private readonly Property CURRENT_STATE = new Property("currentState", "Current State");
                #endregion

                #region Air
                private readonly Property MAX_FALL_SPEED = new Property("maxFallSpeed", "Max Fall Speed");
                private readonly Property GRAVITY_MUTLIPLIER = new Property("gravityMultiplier", "Gravity Multiplier");
                private readonly Property ENABLE_CAN_MOVE_IN_AIR = new Property("enableCanMoveInAir", "Can Move In Air");
                private readonly Property MAX_AIR_SPEED = new Property("maxAirSpeed", "Max Air Speed");
                private readonly Property TIME_TO_REACH_AIR_SPEED = new Property("timeToReachAirSpeed", "Time To Reach Air Speed");
                private readonly Property AIR_STOP_DISTANCE = new Property("airStopDistance", "Air Stop Distance");
                #endregion

                #region Ground
                private readonly Property MAX_GROUND_SPEED = new Property("maxGroundSpeed", "Max Ground Speed");
                private readonly Property TIME_TO_REACH_GROUND_SPEED = new Property("timeToReachGroundSpeed", "Time to Reach Ground Speed");
                private readonly Property GROUND_STOP_DISTANCE = new Property("groundStopDistance", "Ground Stop Distance");
                #endregion

                #region Slope
                private readonly Property ENABLE_SLOPE = new Property("enableSlope", "Enable Slope");
                private readonly Property MAX_ANGLE_ALLOWED_FOR_SLOPE = new Property("maxAngleAllowedForSlope", "Max Angle (Degrees) Allowed For Slope");
                private readonly Property ENABLE_CHANGE_SPEED_ON_SLOPE = new Property("enableChangeSpeedOnSlope", "Enable Change Speed On Slope");
                private readonly Property CHANGE_SPEED_ON_SLOPE_MULTIPLIER = new Property("changeSpeedOnSlopeMultiplier", "Change Speed On Slope Multiplier");
                //private readonly Property ENABLE_STICK_TO_GROUND_ON_SLOPE = new Property("enableStickToGroundOnSlope", "Enable Stick to Ground On Slope");
                //private readonly Property STICK_TO_GROUND_ON_SLOPE_DISTANCE = new Property("stickToGroundOnSlopeDistance", "Stick To Ground On Slope Distance");
                #endregion

                #region Jump
                private readonly Property MIN_JUMP_HEIGHT = new Property("minJumpHeight", "Min Jump Height");
                private readonly Property MAX_JUMP_HEIGHT = new Property("maxJumpHeight", "Max Jump Height");
                private readonly Property MAX_AIR_JUMP_COUNT = new Property("maxAirJumpCount", "Max Air Jump Count");
                private readonly Property JUMP_ACTIVATED_GRACE_TIME = new Property("jumpActivatedGraceTime", "Jump Activated Grace Time");
                private readonly Property CURRENT_JUMP_TYPE_ALLOWED_AFTER_FALLING_GRACE_TIME = new Property("currentJumpTypeAllowedAfterFallingGraceTime", "Current Jump Type Allowed After Falling Grace Time");
                private readonly Property ENABLE_JUMP_UP_SLOPE_TOO_STEEP = new Property("enableJumpUpSlopeTooSteep", "Enable Jump Up Slope Too Steep");
                #endregion

                #region Wall
                private readonly Property NORMALIZED_X_THRESHOLD_FOR_WALL_INTERACTION = new Property("normalizedXThresholdForWallInteraction", "Normalized X Threshold For Wall Interaction");

                private readonly Property ENABLE_WALL_JUMP = new Property("enableWallJump", "Enable Wall Jump");
                private readonly Property WALL_JUMP_MULTIPLIER = new Property("wallJumpMultiplier", "Wall Jump Multiplier");
                private readonly Property WALL_JUMP_ANGLE = new Property("wallJumpAngle", "Wall Jump Angle (Degrees)");
                private readonly Property WALL_JUMP_AWAY_ANGLE = new Property("wallJumpAwayAngle", "Wall Jump Away Angle (Degrees)");

                private readonly Property ENABLE_WALL_SLIDE = new Property("enableWallSlide", "Enable Wall Slide");
                private readonly Property WALL_SLIDE_SPEED = new Property("wallSlideSpeed", "Wall Slide Speed");
                private readonly Property TIME_TO_REACH_WALL_SLIDE_SPEED = new Property("timeToReachWallSlideSpeed", "Time To Reach Wall Slide Speed");

                private readonly Property ENABLE_WALL_STICK = new Property("enableWallStick", "Enable Wall Stick");
                private readonly Property WALL_STICK_DURATION = new Property("wallStickDuration", "Wall Stick Duration");
                #endregion

                #region Dash
                private readonly Property ENABLE_DASH = new Property("enableDash", "Enable Dash");
                private readonly Property DASH_DISTANCE = new Property("dashDistance", "Dash Distance");
                private readonly Property DASH_DURATION = new Property("dashDuration", "Dash Duration");
                private readonly Property DASH_COOLDOWN_TIME = new Property("dashCooldownTime", "Dash Cooldown Time");
                private readonly Property DASH_EASING_FUNCTION = new Property("dashEasingFunction", "Dash Easing Function");
                private readonly Property END_DASH_NO_GRAVITY_TIME = new Property("endDashNoGravityTime", "End Dash No Gravity Time");
                #endregion

                #region One Way Platform
                private readonly Property ENABLE_ONE_WAY_PLATFORM = new Property("enableOneWayPlatform", "Enable One Way Platform");
                private readonly Property IS_ONE_WAY_PLATFORM_CONSIDER_WALL = new Property("isOneWayPlatformConsiderWall", "Is One Way Platform Consider Wall");
                #endregion

                #region Host
                private readonly Property HOST_LAYER_MASK = new Property("hostLayerMask", "Host Layer Mask");
                #endregion
                #endregion

                private void OnEnable()
                {
                    this.properties.Clear();
                    SerializedProperty property = serializedObject.GetIterator();

                    while (property.NextVisible(true))
                    {
                        this.properties[property.name] = property.Copy();
                    }
                }

                public override void OnInspectorGUI()
                {
                    serializedObject.Update();
                    this.timingProperties.Clear();
                    EditorGUILayout.Separator();

                    RaycastPhysicsCharacterComponentEditor.isShowGeneral = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowGeneral, "General");
                    if (RaycastPhysicsCharacterComponentEditor.isShowGeneral)
                    {
                        DisplayRegularField(this.STATIC_ENVIRONMENT_LAYER_MASK);
                        DisplayRegularField(this.TIME_SCALE);
                        DisplayRegularField(this.ITERATIONS_PER_FIXED_UPDATE);
                        DisplayRegularField(this.ENVIRONMENT_CHECK_DISTANCE);
                        DisplayRegularField(this.MIN_DISTANCE_FROM_ENVIRONMENT);
                        DisplayRegularField(this.IS_SNAP_TO_MIN_DISTANCE_FROM_ENVIRONMENT);
                        DisplayRegularField(this.CURRENT_STATE);

                        //        EditorGUILayout.Separator();

                        //        DisplayRegularField(this.MOVING_PLATFORM_LAYER_MASK);

                        //        if (this.properties[this.MOVING_PLATFORM_LAYER_MASK.name].hasMultipleDifferentValues ||
                        //            this.properties[this.MOVING_PLATFORM_LAYER_MASK.name].intValue != 0)
                        //        {
                        //            DisplayRegularField(this.RAYCASTS_PER_SIDE);
                        //        }

                        EditorGUILayout.Separator();
                    }

                    RaycastPhysicsCharacterComponentEditor.isShowAir = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowAir, "Air");
                    if (RaycastPhysicsCharacterComponentEditor.isShowAir)
                    {
                        DisplayRateField(this.MAX_FALL_SPEED);
                        DisplayRegularField(this.GRAVITY_MUTLIPLIER);
                        DisplayRegularField(this.ENABLE_CAN_MOVE_IN_AIR);
                        DisplayRateField(this.MAX_AIR_SPEED);

                        if (this.properties[this.ENABLE_CAN_MOVE_IN_AIR.name].hasMultipleDifferentValues || this.properties[this.ENABLE_CAN_MOVE_IN_AIR.name].boolValue)
                        {
                            DisplayAccelerationField(this.TIME_TO_REACH_AIR_SPEED);
                            DisplayRegularField(this.AIR_STOP_DISTANCE);
                        }

                        EditorGUILayout.Separator();
                    }

                    RaycastPhysicsCharacterComponentEditor.isShowGround = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowGround, "Ground");
                    if (RaycastPhysicsCharacterComponentEditor.isShowGround)
                    {
                        DisplayRateField(this.MAX_GROUND_SPEED);
                        DisplayAccelerationField(this.TIME_TO_REACH_GROUND_SPEED);
                        DisplayRegularField(this.GROUND_STOP_DISTANCE);

                        EditorGUILayout.Separator();
                    }

                    RaycastPhysicsCharacterComponentEditor.isShowSlope = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowSlope, "Slope");
                    if (RaycastPhysicsCharacterComponentEditor.isShowSlope)
                    {
                        DisplayRegularField(this.ENABLE_SLOPE);

                        if (this.properties[this.ENABLE_SLOPE.name].hasMultipleDifferentValues || this.properties[this.ENABLE_SLOPE.name].boolValue)
                        {
                            DisplayRegularField(this.MAX_ANGLE_ALLOWED_FOR_SLOPE);
                            DisplayRegularField(this.ENABLE_CHANGE_SPEED_ON_SLOPE);

                            if (this.properties[this.ENABLE_CHANGE_SPEED_ON_SLOPE.name].hasMultipleDifferentValues ||
                                this.properties[this.ENABLE_CHANGE_SPEED_ON_SLOPE.name].boolValue)
                            {
                                DisplayRegularField(this.CHANGE_SPEED_ON_SLOPE_MULTIPLIER);
                            }

                            //            DisplayRegularField(this.ENABLE_STICK_TO_GROUND_ON_SLOPE);

                            //            if (this.properties[this.ENABLE_STICK_TO_GROUND_ON_SLOPE.name].hasMultipleDifferentValues || this.properties[this.ENABLE_STICK_TO_GROUND_ON_SLOPE.name].boolValue)
                            //            {
                            //                DisplayRegularField(this.STICK_TO_GROUND_ON_SLOPE_DISTANCE);
                            //            }
                        }

                        EditorGUILayout.Separator();
                    }

                    RaycastPhysicsCharacterComponentEditor.isShowJump = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowJump, "Jump");
                    if (RaycastPhysicsCharacterComponentEditor.isShowJump)
                    {
                        DisplayRegularField(this.MIN_JUMP_HEIGHT);
                        DisplayRegularField(this.MAX_JUMP_HEIGHT);
                        DisplayRegularField(this.MAX_AIR_JUMP_COUNT);
                        DisplayTimingField(this.JUMP_ACTIVATED_GRACE_TIME);
                        DisplayTimingField(this.CURRENT_JUMP_TYPE_ALLOWED_AFTER_FALLING_GRACE_TIME);
                        DisplayRegularField(this.ENABLE_JUMP_UP_SLOPE_TOO_STEEP);

                        EditorGUILayout.Separator();
                    }

                    RaycastPhysicsCharacterComponentEditor.isShowWall = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowWall, "Wall");
                    if (RaycastPhysicsCharacterComponentEditor.isShowWall)
                    {
                        DisplayRegularField(this.NORMALIZED_X_THRESHOLD_FOR_WALL_INTERACTION);

                        EditorGUILayout.Separator();

                        DisplayRegularField(this.ENABLE_WALL_JUMP);

                        if (this.properties[this.ENABLE_WALL_JUMP.name].hasMultipleDifferentValues || this.properties[this.ENABLE_WALL_JUMP.name].boolValue)
                        {
                            DisplayRegularField(this.WALL_JUMP_MULTIPLIER);
                            DisplayRegularField(this.WALL_JUMP_ANGLE);
                            DisplayRegularField(this.WALL_JUMP_AWAY_ANGLE);
                        }

                        EditorGUILayout.Separator();

                        DisplayRegularField(this.ENABLE_WALL_SLIDE);

                        if (this.properties[this.ENABLE_WALL_SLIDE.name].hasMultipleDifferentValues || this.properties[this.ENABLE_WALL_SLIDE.name].boolValue)
                        {
                            DisplayRateField(this.WALL_SLIDE_SPEED);
                            DisplayAccelerationField(this.TIME_TO_REACH_WALL_SLIDE_SPEED);
                        }

                        EditorGUILayout.Separator();

                        DisplayRegularField(this.ENABLE_WALL_STICK);

                        if (this.properties[this.ENABLE_WALL_STICK.name].hasMultipleDifferentValues || this.properties[this.ENABLE_WALL_STICK.name].boolValue)
                        {
                            DisplayTimingField(this.WALL_STICK_DURATION);
                        }

                        EditorGUILayout.Separator();
                    }

                    RaycastPhysicsCharacterComponentEditor.isShowDash = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowDash, "Dash");
                    if (RaycastPhysicsCharacterComponentEditor.isShowDash)
                    {
                        DisplayRegularField(this.ENABLE_DASH);

                        if (this.properties[this.ENABLE_DASH.name].hasMultipleDifferentValues || this.properties[this.ENABLE_DASH.name].boolValue)
                        {
                            DisplayRegularField(this.DASH_DISTANCE);

                            DisplayTimingField(this.DASH_DURATION);
                            DisplayTimingField(this.DASH_COOLDOWN_TIME);

                            DisplayRegularField(this.DASH_EASING_FUNCTION);
                            DisplayTimingField(this.END_DASH_NO_GRAVITY_TIME);
                        }

                        EditorGUILayout.Separator();
                    }

                    RaycastPhysicsCharacterComponentEditor.isShowOneWayPlatform = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowOneWayPlatform, "One Way Platform");
                    if (RaycastPhysicsCharacterComponentEditor.isShowOneWayPlatform)
                    {
                        DisplayRegularField(this.ENABLE_ONE_WAY_PLATFORM);

                        if (this.properties[this.ENABLE_ONE_WAY_PLATFORM.name].hasMultipleDifferentValues || this.properties[this.ENABLE_ONE_WAY_PLATFORM.name].boolValue)
                        {
                            DisplayRegularField(this.IS_ONE_WAY_PLATFORM_CONSIDER_WALL);
                        }

                        EditorGUILayout.Separator();
                    }

                    RaycastPhysicsCharacterComponentEditor.isShowHost = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowHost, "Host");
                    if (RaycastPhysicsCharacterComponentEditor.isShowHost)
                    {
                        DisplayRegularField(this.HOST_LAYER_MASK);

                        EditorGUILayout.Separator();
                    }

                    if (!serializedObject.isEditingMultipleObjects)
                    {
                        RaycastPhysicsCharacterComponentEditor.isShowInformation = EditorGUILayout.Foldout(RaycastPhysicsCharacterComponentEditor.isShowInformation, "Information");

                        if (RaycastPhysicsCharacterComponentEditor.isShowInformation)
                        {
                            DisplayRegularField(this.CURRENT_STATE);

                            EditorGUILayout.HelpBox(
                                GetInformation(),
                                MessageType.Info,
                                true);
                        }
                    }

                    CheckValues();
                    CheckAndDisplayInfo();

                    serializedObject.ApplyModifiedProperties();
                }

                private void CheckValues()
                {
                    if (!this.properties[this.ENVIRONMENT_CHECK_DISTANCE.name].hasMultipleDifferentValues &&
                        this.properties[this.ENVIRONMENT_CHECK_DISTANCE.name].floatValue <= MINIMUM_DISTANCE_CHECK * 2)
                    {
                        this.properties[this.ENVIRONMENT_CHECK_DISTANCE.name].floatValue = MINIMUM_DISTANCE_CHECK * 2;
                    }

                    if (!this.properties[this.MIN_DISTANCE_FROM_ENVIRONMENT.name].hasMultipleDifferentValues &&
                        this.properties[this.MIN_DISTANCE_FROM_ENVIRONMENT.name].floatValue <= MINIMUM_DISTANCE_CHECK)
                    {
                        this.properties[this.MIN_DISTANCE_FROM_ENVIRONMENT.name].floatValue = MINIMUM_DISTANCE_CHECK;
                    }

                    if (!this.properties[this.ITERATIONS_PER_FIXED_UPDATE.name].hasMultipleDifferentValues &&
                        this.properties[this.ITERATIONS_PER_FIXED_UPDATE.name].intValue < 0)
                    {
                        this.properties[this.ITERATIONS_PER_FIXED_UPDATE.name].intValue = 1;
                    }
                }

                private void CheckAndDisplayInfo()
                {
                    if (!Physics2D.queriesStartInColliders &&
                        (this.properties[this.HOST_LAYER_MASK.name].hasMultipleDifferentValues ||
                        this.properties[this.HOST_LAYER_MASK.name].intValue != 0))
                    {
                        EditorGUILayout.HelpBox(
                            "'Raycasts Start in Colliders' in the Physics 2D settings needs to be checked on for hosts.",
                            MessageType.Error,
                            true);
                    }

                    if (!this.properties[this.STATIC_ENVIRONMENT_LAYER_MASK.name].hasMultipleDifferentValues &&
                        this.properties[this.STATIC_ENVIRONMENT_LAYER_MASK.name].intValue == 0)
                    {
                        EditorGUILayout.HelpBox(
                            "<b>General > Static Environment Layer Mask</b> required.",
                            MessageType.Error,
                            true);

                        EditorGUILayout.HelpBox(
                            "Static Environment Layer Mask refers to elements like \"Ground\" and \"Wall\" that remain static throughout the game.",
                            MessageType.Info,
                            true);
                    }

                    if (!this.properties[this.STATIC_ENVIRONMENT_LAYER_MASK.name].hasMultipleDifferentValues &&
                        (this.properties[this.STATIC_ENVIRONMENT_LAYER_MASK.name].intValue & (1 << ((CharacterRaycastPhysicsComponent)target).gameObject.layer)) != 0)
                    {
                        EditorGUILayout.HelpBox(
                            "The Static Environment Layer Mask can not include the layer the motor is on!",
                            MessageType.Error,
                            true);
                    }

                    for (int i = 0; i < this.timingProperties.Count; i++)
                    {
                        CheckAndDisplayTimingWarnings(this.timingProperties[i]);
                    }
                }

                private void CheckAndDisplayTimingWarnings(Property property)
                {
                    if (!this.properties[property.name].hasMultipleDifferentValues &&
                        !Mathf.Approximately(this.properties[property.name].floatValue / Time.fixedDeltaTime,
                            Mathf.Round(this.properties[property.name].floatValue / Time.fixedDeltaTime)))
                    {
                        string msg = string.Format(
                            "'{0}' is not a multiple of the fixed time step ({1}). This results in an extra frame effectively making '{0}' {2} instead of {3}",
                            property.text,
                            Time.fixedDeltaTime,
                            FrameService.ConvertTimeToFrame(this.properties[property.name].floatValue) * Time.fixedDeltaTime,
                            this.properties[property.name].floatValue);

                        EditorGUILayout.HelpBox(
                            msg,
                            MessageType.Warning,
                            true);
                    }
                }

                private string GetInformation()
                {
                    StringBuilder sb = new StringBuilder();

                    sb.AppendFormat("Approx jump distance: {0}", GetJumpDistance());

                    if (this.properties[this.TIME_TO_REACH_GROUND_SPEED.name].floatValue != 0)
                    {
                        sb.AppendFormat(
                            "\nGround acceleration: {0}",
                            this.properties[this.MAX_GROUND_SPEED.name].floatValue / this.properties[this.TIME_TO_REACH_GROUND_SPEED.name].floatValue);
                    }

                    if (this.properties[this.GROUND_STOP_DISTANCE.name].floatValue != 0)
                    {
                        sb.AppendFormat(
                            "\nTime to stop on ground: {0}",
                            GetTimeToDistance(
                                this.properties[this.GROUND_STOP_DISTANCE.name].floatValue,
                                this.properties[this.MAX_GROUND_SPEED.name].floatValue));
                    }

                    if (this.properties[this.TIME_TO_REACH_AIR_SPEED.name].floatValue != 0 && this.properties[this.ENABLE_CAN_MOVE_IN_AIR.name].boolValue)
                    {
                        sb.AppendFormat(
                            "\nMax air acceleration: {0}",
                            this.properties[this.MAX_AIR_SPEED.name].floatValue / this.properties[this.TIME_TO_REACH_AIR_SPEED.name].floatValue);
                    }

                    if (this.properties[this.AIR_STOP_DISTANCE.name].floatValue != 0 && this.properties[this.ENABLE_CAN_MOVE_IN_AIR.name].boolValue)
                    {
                        sb.AppendFormat(
                            "\nTime to stop on ground: {0}",
                            GetTimeToDistance(
                                this.properties[this.AIR_STOP_DISTANCE.name].floatValue,
                                this.properties[this.MAX_AIR_SPEED.name].floatValue));
                    }

                    sb.AppendFormat("\nApprox single jump duration (up & down): {0}",
                        Mathf.Sqrt(-8 * (this.properties[this.MIN_JUMP_HEIGHT.name].floatValue + this.properties[this.MAX_JUMP_HEIGHT.name].floatValue) /
                            (this.properties[this.GRAVITY_MUTLIPLIER.name].floatValue * Physics2D.gravity.y)));

                    sb.AppendFormat(
                        "\nWill hit fall speed cap during jump: {0}",
                        (GetTotalJumpSpeed() >= this.properties[this.MAX_FALL_SPEED.name].floatValue));

                    sb.AppendFormat(
                        "\nFrames needed to get reach extra jump height: {0}",
                        (this.properties[this.MAX_JUMP_HEIGHT.name].floatValue / GetBaseJumpSpeed()) / Time.fixedDeltaTime);

                    if (this.properties[this.TIME_TO_REACH_WALL_SLIDE_SPEED.name].floatValue != 0 && this.properties[this.ENABLE_WALL_SLIDE.name].boolValue)
                    {
                        sb.AppendFormat(
                            "\nWall slide acceleration: {0}",
                            this.properties[this.WALL_SLIDE_SPEED.name].floatValue / this.properties[this.TIME_TO_REACH_WALL_SLIDE_SPEED.name].floatValue);
                    }

                    //if (this.properties[this.ENABLE_SLOPE.name].boolValue)
                    //{
                    //    sb.AppendFormat(
                    //        "\nColliding slope angle: {0}",
                    //        (UnityEngine.Vector2.Angle(this.properties[this.MAX_ANGLE_ALLOWED_FOR_SLOPE.name].vector2Value, UnityEngine.Vector2.up)));
                    //}

                    return sb.ToString();
                }

                private float GetJumpDistance()
                {
                    return this.properties[this.MAX_AIR_SPEED.name].floatValue * 2 *
                           Mathf.Sqrt(2 *
                                (this.properties[this.MIN_JUMP_HEIGHT.name].floatValue +
                                this.properties[this.MAX_JUMP_HEIGHT.name].floatValue) /
                                (((CharacterRaycastPhysicsComponent)target).GravityMultiplier *
                                Mathf.Abs(Physics2D.gravity.y)));
                }

                private float GetBaseJumpSpeed()
                {
                    return Mathf.Sqrt(-2 * ((this.properties[this.MIN_JUMP_HEIGHT.name].floatValue) *
                        this.properties[this.GRAVITY_MUTLIPLIER.name].floatValue * Physics2D.gravity.y));
                }

                private float GetTotalJumpSpeed()
                {
                    return Mathf.Sqrt(-2 * ((this.properties[this.MIN_JUMP_HEIGHT.name].floatValue + this.properties[this.MAX_JUMP_HEIGHT.name].floatValue) *
                        this.properties[this.GRAVITY_MUTLIPLIER.name].floatValue * Physics2D.gravity.y));
                }

                private float GetTimeToDistance(float distance, float maxSpeed)
                {
                    float deceleration = (maxSpeed * maxSpeed) / (2 * distance);

                    return Mathf.Sqrt(2 * distance / deceleration);
                }

                #region Display Functions
                private void DisplayRegularField(Property property)
                {
                    EditorGUILayout.PropertyField(this.properties[property.name], new GUIContent(property.text));
                }

                private void DisplayRateField(Property property)
                {
                    string frameRate = "-";

                    if (!this.properties[property.name].hasMultipleDifferentValues)
                    {
                        frameRate = (this.properties[property.name].floatValue * Time.fixedDeltaTime).ToString();
                    }

                    EditorGUILayout.PropertyField(this.properties[property.name],
                        new GUIContent(string.Format("{0} ({1} Distance/Frame)", property.text, frameRate)));
                }

                private void DisplayAccelerationField(Property property)
                {
                    string frameCount = "-";

                    if (!this.properties[property.name].hasMultipleDifferentValues)
                    {
                        frameCount = (this.properties[property.name].floatValue / Time.fixedDeltaTime).ToString();
                    }

                    EditorGUILayout.PropertyField(this.properties[property.name],
                        new GUIContent(string.Format("{0} ({1} Frames)", property.text, frameCount)));
                }

                private void DisplayTimingField(Property property)
                {
                    this.timingProperties.Add(property);

                    string frameCount = "-";

                    if (!this.properties[property.name].hasMultipleDifferentValues)
                    {
                        frameCount = FrameService.ConvertTimeToFrame(this.properties[property.name].floatValue).ToString();
                    }

                    EditorGUILayout.PropertyField(this.properties[property.name],
                        new GUIContent(string.Format("{0} ({1} Frames)", property.text, frameCount)));
                }
                #endregion


            }
        }
    }
}
