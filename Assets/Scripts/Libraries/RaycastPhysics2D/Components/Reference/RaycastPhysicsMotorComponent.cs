using System;
using Kameosa.GameObject.Gameplay2D.Platformer.Patrol;
using Kameosa.Math;
using Kameosa.Services;
using UnityEditor;
using UnityEngine;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Common
                {
                    public class EnumFlagsAttribute : PropertyAttribute
                    {
                        public EnumFlagsAttribute() { }
                    }

                    [CustomPropertyDrawer(typeof(EnumFlagsAttribute))]
                    public class EnumFlagsAttributeDrawer : PropertyDrawer
                    {
                        public override void OnGUI(Rect _position, SerializedProperty _property, GUIContent _label)
                        {
                            _property.intValue = EditorGUI.MaskField(_position, _label, _property.intValue, _property.enumNames);
                        }
                    }

                    [RequireComponent(typeof(BoxCollider2D))]
                    public class RaycastPhysicsMotorComponent : MonoBehaviour
                    {
                        #region Enumerables
                        /// <summary>
                        /// The states the motor can be in.
                        /// </summary>
                        public enum MotorState
                        {
                            OnGround,
                            Jumping,
                            Falling,
                            FallingFast,
                            WallSliding,
                            OnCorner,
                            WallSticking,
                            Dashing,
                            Frozen,
                            Slipping,
                            FreedomState
                        }

                        /// <summary>
                        /// The surfaces the motor may be colliding against.
                        /// </summary>
                        [Flags]
                        public enum CollidedSurface
                        {
                            None = 0x0,
                            Ground = 0x1,
                            LeftWall = 0x2,
                            RightWall = 0x4,
                            Ceiling = 0x8,
                            SlopeLeft = 0x10,
                            SlopeRight = 0x20
                        }

                        /// <summary>
                        /// The surfaces the motor may be colliding against.
                        /// </summary>
                        [Flags]
                        public enum CollidedArea
                        {
                            None = 0x0,
                            Restricted = 0x01,
                            FreedomArea = 0x02,
                            Ladder = 0x04 // maybe 0x06
                        }

                        /// <summary>
                        /// Zone in the ladder, to handle input properly
                        /// </summary>
                        public enum LadderZone
                        {
                            Top,
                            Bottom,
                            Middle
                        }
                        #endregion

                        #region Protected Classes

                        // Contains the various jump variables, this is for organization.
                        protected class JumpState
                        {
                            public enum JumpType
                            {
                                None,
                                Normal,
                                RightWall,
                                LeftWall,
                                Corner
                            }

                            public bool isPressed;
                            public bool isHeld;
                            public int numAirJumps;

                            public int activatedGraceFrames;
                            public int allowExtraFrames;

                            public bool isForce;
                            public float height;

                            public float jumpAllowedAfterFallingGraceFrame;
                            public bool isJumpTypeChanged;

                            protected JumpType lastValidJump;

                            public JumpType LastValidJump
                            {
                                get { return this.lastValidJump; }
                                set
                                {
                                    if (value != JumpType.None)
                                    {
                                        this.isJumpTypeChanged = true;
                                    }
                                    else
                                    {
                                        this.jumpAllowedAfterFallingGraceFrame = -1;
                                    }

                                    this.lastValidJump = value;
                                }
                            }
                        }

                        // Contains the various dash variables.
                        protected class DashState
                        {
                            public bool isPressed;
                            public float cooldownFrames;
                            public int dashingFrames;
                            public bool isDashWithDirection;
                            public UnityEngine.Vector2 dashDir = UnityEngine.Vector2.zero;
                            public float distanceCalculated;
                            public float distanceDashed;
                            public bool isForce;
                            public float gravityEnabledFrames;
                        }

                        // Contains information for wall sticks, slides, and corner grabs.
                        protected class WallState
                        {
                            public float cornerHangFrames;
                            public float stickFrames;

                            public float wallInteractionCooldownFrames;
                            public bool canHangAgain = true;
                        }

                        protected class MovingPlatformState
                        {
                            public RaycastPhysicsMovingPlatformMotorComponent platform;

                            public UnityEngine.Vector2 previousPos;
                            public CollidedSurface stuckToWall;
                            public bool IsOnPlatform { get { return this.platform != null; } }
                        }

                        #endregion

                        #region Constants

                        protected const float NEAR_ZERO = 0.0001f;

                        protected const float DISTANCE_TO_END_ITERATION = 0.001f;
                        protected const float CHECK_TOUCHING_TRIM = 0.01f;

                        protected const int STARTING_ARRAY_SIZE = 4;
                        protected const float INCREASE_ARRAY_SIZE_MULTIPLIER = 2;

                        protected const int DIRECTIONS_CHECKED = 4;
                        protected const int DIRECTION_DOWN = 0;
                        protected const int DIRECTION_UP = 1;
                        protected const int DIRECTION_LEFT = 2;
                        protected const int DIRECTION_RIGHT = 3;

                        #endregion

                        #region Statics

                        // Used for environment checks and one way platforms
                        protected static RaycastHit2D[] hits = new RaycastHit2D[STARTING_ARRAY_SIZE];
                        protected static Collider2D[] overlappingColliders = new Collider2D[STARTING_ARRAY_SIZE];

                        #endregion

                        #region Public Variables
                        /// <summary>
                        /// The static environment check mask. This should only be environment that doesn't move.
                        /// </summary>
                        public LayerMask staticEnvLayerMask;

                        /// <summary>
                        /// How far out the motor will check for the environment mask. This value can be tweaked if jump checks are not firing when
                        /// wanted.
                        /// </summary>
                        public float envCheckDistance = 0.14f;

                        /// <summary>
                        /// The distance the motor will separate itself from environment. This is useful to prevent the motor from catching on edges.
                        /// </summary>
                        public float minDistanceFromEnv = 0.07f;

                        /// <summary>
                        /// The number of iterations the motor is allowed to make during the fixed update. Lower number will be more performant
                        /// at a cost of losing some movement when collisions occur.
                        /// </summary>
                        public int numOfIterations = 2;

                        /// <summary>
                        /// Should the motor check for one way platforms? Set this to false if there aren't any, the motor will be more efficient.
                        /// This will only have an effect if the motor's collider can't collide with its own layer. If it can then setting this to
                        /// false won't help, one way platforms or not.
                        /// </summary>
                        public bool enableOneWayPlatforms = true;

                        /// <summary>
                        /// if enableOneWayPlatforms is disabled how the motor should treat OWP?
                        /// when both are disabled, OWP are ignored from collisions.
                        /// </summary>
                        public bool isOneWayPlatformsAreWalls = true;

                        /// <summary>
                        /// The layer that contains moving platforms. If there are no moving platforms then make sure this has no layers (value of 0).
                        /// Optimizations are made in the motor if it isn't expecting any moving platforms.
                        /// </summary>
                        public LayerMask movingPlatformLayerMask;

                        /// <summary>
                        /// When checking for moving platforms that may have moved into the motor the corners are automatically casted on. This
                        /// variable impacts how many more casts are made to each side. If the smallest size environment is the same size or bigger
                        /// than the motor (width and height) then this can be 0. If it's at least half size then this can be 1. Increasing this
                        /// number allows separation from smaller platform pieces but at a performance cost.
                        /// </summary>
                        public int additionalRaycastsPerSide = 1;

                        /// <summary>
                        /// The maximum speed the motor will move on the ground, only effects horizontal speed.
                        /// </summary>
                        public float groundSpeed = 8f;

                        /// <summary>
                        /// How much time does it take for the motor to get from zero speed to max speed. This value
                        /// is used to calculate the acceleration.
                        /// </summary>
                        public float timeToGroundSpeed = 0.0f;

                        /// <summary>
                        /// The distance the motor will slide to a stop from full speed while on the ground.
                        /// </summary>
                        public float groundStopDistance = 0.333f;

                        /// <summary>
                        /// The maximum horizontal speed of the motor in the air.
                        /// </summary>
                        public float airSpeed = 5f;

                        /// <summary>
                        /// If true, then the player can change x direction while jumping. If false, then
                        /// the x velocity when leaving the ground will be isHeld while in the air
                        /// </summary>
                        public bool canChangeDirectionInAir = true;

                        /// <summary>
                        /// The time it takes to move from zero horizontal speed to the maximum speed. This value is
                        /// used to calculate the acceleration.
                        /// </summary>
                        public float timeToAirSpeed = 0.2f;

                        /// <summary>
                        /// The distance the motor will 'slide' to a stop while in the air. Only effects horizontal
                        /// movement.
                        /// </summary>
                        public float airStopDistance = 2f;

                        /// <summary>
                        /// The maximum speed that the motor will fall. Only effects vertical speed when falling.
                        /// </summary>
                        public float fallSpeed = 16f;

                        /// <summary>
                        /// Gravity multiplier to the Physics2D.gravity setting. Works like RigidBody2D's gravityMultiplier.
                        /// </summary>
                        public float gravityMultiplier = 4;

                        /// <summary>
                        /// The maximum speed that the motor will fall during 'fast fall'.
                        /// </summary>
                        public float fastFallSpeed = 32f;

                        /// <summary>
                        /// If the motor is in 'fast fall' then the gravityMultiplier is multiplied by the value. Higher number means
                        /// faster acceleration while falling.
                        /// </summary>
                        public float fastFallGravityMultiplier = 8f;

                        /// <summary>
                        /// Maximum horizontal/vertical speed of the motor while climb a ladder
                        /// </summary>
                        public float ladderSpeed = 5f;

                        /// <summary>
                        /// The height the motor will jump when a jump command is issued.
                        /// </summary>
                        public float jumpHeight = 1.5f;

                        /// <summary>
                        /// The extra height the motor will jump if jump is 'isHeld' down.
                        /// </summary>
                        public float extraJumpHeight = 1.5f;

                        /// <summary>
                        /// Number of air jumps allowed.
                        /// </summary>
                        public int numOfAirJumps = 1;

                        /// <summary>
                        /// The amount of time once the motor has left an environment that a jump will be allowed.
                        /// </summary>
                        public float jumpWindowWhenFalling = 0.2f;

                        /// <summary>
                        /// The grace period once the motor is told to jump where it will jump.
                        /// </summary>
                        public float jumpWindowWhenActivated = 0.2f;

                        /// <summary>
                        /// Should the motor check for any slopes? Set this to false if there are no slopes, the motor will be more efficient.
                        /// </summary>
                        public bool enableSlopes = true;

                        /// <summary>
                        /// The angle of slope the motor is allowed to walk on. It could be a good idea to keep this slightly above the minimum.
                        /// </summary>
                        public float angleAllowedForMoving = 50;

                        /// <summary>
                        /// The speed necessary to try running up a slope that's too steep. If speed is less than the minimum then the motor's
                        /// velocity is zeroed out and the motor can't try to run up the slope.
                        /// </summary>
                        public float minimumSpeedToMoveUpSlipperySlope = 7.5f;

                        /// <summary>
                        /// Should the speed of the motor change depending of the angle of the slope. This only impacts walking on slopes, not
                        /// while sliding.
                        /// </summary>
                        public bool enableChangeSpeedOnSlopes = true;

                        /// <summary>
                        /// If the motor changes speed on slopes then this acts as a multiplier. Lower values will mean bigger slow downs. A value
                        /// of 1 means that it's only based off of the angle of the slope.
                        /// </summary>

                        [Range(0f, 1f)]
                        public float speedMultiplierOnSlope = 0.75f;

                        /// <summary>
                        /// Should the motor stick to the ground when walking down onto a slope or up over a slope? Otherwise the motor may fall onto
                        /// the slope or have a slight hop when moving up over a slope.
                        /// </summary>
                        public bool enableStickOnGround = true;

                        /// <summary>
                        /// If enableStickOnGround is true then the motor will search down for the ground to place itself on. This is how far it is willing
                        /// to check. This needs to be high enough to account for the distance placed by the motor speed but should be smaller than
                        /// the difference between environment heights. Play around until a nice value is found.
                        /// </summary>
                        public float distanceToCheckToStick = 0.4f;

                        /// <summary>
                        /// If wall jumps are allowed.
                        /// </summary>
                        public bool enableWallJumps = true;

                        /// <summary>
                        /// The jump speed multiplier when wall jumping. This is useful to isForce bigger jumps off of the wall.
                        /// </summary>
                        public float wallJumpMultiplier = 1f;

                        /// <summary>
                        /// The angle (degrees) in which the motor will jump away from the wall. 0 is horizontal and 90 is straight up.
                        /// </summary>
                        [Range(0f, 90f)]
                        public float wallJumpAngle = 80;

                        /// <summary>
                        /// The angle (degrees) in which the motor will jump away from the wall when normalized x is away from the wall. 0 is horizontal and 90 is straight up.
                        /// </summary>
                        [Range(0f, 90f)]
                        public float wallJumpAwayAngle = 30;

                        /// <summary>
                        /// If wall sticking is allowed. A wall sticking is when a motor will 'grab' a wall.
                        /// </summary>
                        public bool enableWallSticks = true;

                        /// <summary>
                        /// The duration of the wall sticks in seconds. Set to a very large number to effectively allow permanent sticks.
                        /// </summary>
                        public float wallSticksDuration = 1f;

                        /// <summary>
                        /// If wall slides are allowed. A wall slide is when a motor slides down a wall. This will only take in effect
                        /// once the stick is over.
                        /// </summary>
                        public bool enableWallSlides = true;

                        /// <summary>
                        /// The speed that the motor will slide down the wall.
                        /// </summary>
                        public float wallSlideSpeed = 5;

                        /// <summary>
                        /// The time, in seconds, to get to wall slide speed.
                        /// </summary>
                        public float timeToWallSlideSpeed = 3;

                        /// <summary>
                        /// Are corner grabs allowed? A corner grab is when the motor sticks to a corner.
                        /// </summary>
                        public bool enableCornerGrabs = true;

                        /// <summary>
                        /// The duration, in seconds, that the motor will stick to a corner.
                        /// </summary>
                        public float cornerGrabDuration = 1f;

                        /// <summary>
                        /// The jump speed multiplier when jumping from a corner grab. Useful to forcing bigger jumps.
                        /// </summary>
                        public float cornerJumpMultiplier = 1f;

                        /// <summary>
                        /// This is the size of the corner check. This can be tweaked with if corner grabs are not working correctly.
                        /// </summary>
                        public float cornerDistanceCheck = 0.2f;

                        /// <summary>
                        /// This is the size of a valid check (normalized to collider height) that will consider wall interactions valid.
                        /// Starts from the top of the collider and moves down.
                        /// </summary>
                        [Range(0.1f, 1f)]
                        public float normalizedValidWallInteraction = 0.2f;

                        /// <summary>
                        /// After a corner or wall jump, this is how longer horizontal input is ignored.
                        /// </summary>
                        public float ignoreMovementAfterJump = 0.2f;

                        /// <summary>
                        /// Cooldown for allowing slides, sticks, and corner grabs. This may be necessary if the motor can slide down a vertical
                        /// moving platform. If they don't exist then this can be 0.
                        /// </summary>
                        public float wallInteractionCooldown = 0.1f;

                        /// <summary>
                        /// The threshold that normalizedXMovement will have to be higher than to consider wall sticks, wall slides, wall jumps,
                        /// and corner grabs.
                        /// </summary>
                        [Range(0f, 1f)]
                        public float wallInteractionThreshold = 0.5f;

                        /// <summary>
                        /// Is dashState allowed?
                        /// </summary>
                        public bool enableDashes = true;

                        /// <summary>
                        /// How far the motor will dash.
                        /// </summary>
                        public float dashDistance = 3;

                        /// <summary>
                        /// How long the dash lasts in seconds.
                        /// </summary>
                        public float dashDuration = 0.2f;

                        /// <summary>
                        /// When the motor will be allowed to dash again after dashState. The cooldown begins at the end of a dash.
                        /// </summary>
                        public float dashCooldown = 0.76f;

                        /// <summary>
                        /// The easing function used during the dash. Pick 'Linear' for just a set speed.
                        /// </summary>
                        public Easing.Functions dashEasingFunction = Easing.Functions.OutQuad;

                        /// <summary>
                        /// Delay (in seconds) before gravity is turned back on after a dash.
                        /// </summary>
                        public float endDashNoGravityDuration = 0.1f;


                        /// <summary>
                        /// Internal gizmos moving platform debug rendering.
                        /// </summary>
                        public bool enableMovingPlatformDebug;

                        /// <summary>
                        /// Internal gizmos for iteration debugging.
                        /// </summary>
                        public bool enableIterationDebug;

                        /// <summary>
                        /// The normal of the slope the motor is on. This value doesn't have meaning unless isOnSlope is true.
                        /// </summary>
                        public UnityEngine.Vector2 slopeNormal;

                        #endregion

                        #region Actions

                        /// <summary>
                        /// Delegate to attach to when the motor dashes.
                        /// </summary>
                        public Action OnDash;

                        /// <summary>
                        /// Delegate to attach to when the motor's dash ends.
                        /// </summary>
                        public Action OnDashEnd;

                        /// <summary>
                        /// Delegate to attach to when the motor jumps (ALL JUMPS!).
                        /// </summary>
                        public Action OnJump;

                        /// <summary>
                        /// Delegate that notifies, before change state, last jump has ended, reached the maximum allowed.
                        /// Do not has parity with onJump, this is called when the last Jump ends not when Jump again.
                        /// </summary>
                        public Action OnJumpEnd;

                        /// <summary>
                        /// Delegate to attach to when the motor air jumps (called before onJump).
                        /// </summary>
                        public Action OnAirJump;

                        /// <summary>
                        /// Delegate to attach to when the motor walls jumps (called before onJump). The vector passed is the normal of the wall.
                        /// </summary>
                        public Action<UnityEngine.Vector2> OnWallJump;

                        /// <summary>
                        /// Delegate to attach to when the motor corner jumps (called before onJump).
                        /// </summary>
                        public Action OnCornerJump;

                        /// <summary>
                        /// Delegate that notifies that the motor has landed. amountFallen can be queried for distance fallen.
                        /// </summary>
                        public Action OnLanded;

                        /// <summary>
                        /// Delegate that notifies that the motor start slipping.
                        /// </summary>
                        public Action OnSlipping;

                        /// <summary>
                        /// Delegate that notifies, before change state, that the motor end sliping.
                        /// </summary>
                        public Action OnSlippingEnd;

                        #endregion

                        #region Protected Variables

                        protected LayerMask collisionMask;
                        protected UnityEngine.Vector2 restrictedAreaTR;
                        protected Bounds restrictedArea;
                        protected UnityEngine.Vector2 restrictedAreaBL;
                        protected UnityEngine.Vector2 wallJumpVector;
                        protected UnityEngine.Vector2 wallJumpAwayVector;
                        protected float ignoreMovementFrames;
                        protected bool isFrozen;
                        protected bool isOriginalKinematic;
                        protected float timeScale = 1;
                        protected UnityEngine.Vector3 previousLoc;
                        protected Collider2D[] collidersUpAgainst = new Collider2D[DIRECTIONS_CHECKED];
                        protected UnityEngine.Vector2[] collidedNormals = new UnityEngine.Vector2[DIRECTIONS_CHECKED];
                        protected MotorState prevState;
                        protected Bounds prevColliderBounds;
                        protected float dotAllowedForSlopes;
                        protected float _cornerDistanceCheck;
                        protected float distanceFromEnvCorner;
                        protected UnityEngine.Vector2 bottomRight;
                        protected UnityEngine.Vector3 toTransform;
                        protected float currentDeltaTime;
                        protected new Rigidbody2D rigidbody2D;
                        protected new Collider2D collider2D;
                        protected float distanceToBoundsCorner;
                        protected float savedTimeScale;
                        protected UnityEngine.Vector2 disallowedSlopeNormal;
                        protected UnityEngine.Vector2 previousMoveDir;
                        protected bool isValidWallInteraction;

                        // This is the unconverted motor velocity. This ignores slopes. It is converted into the appropriate vector before
                        // moving.
                        protected UnityEngine.Vector2 velocity;

                        // The function is cached to avoid unnecessary memory allocation.
                        protected Easing.EasingFunc dashFunction;
                        protected Easing.EasingFunc dashDerivativeFunction;

                        // This is stored to notice if the public field changes during runtime.
                        protected Easing.Functions currentDashEasingFunction;
                        protected float currentWallJumpDegree;
                        protected float currentSlopeDegreeAllowed;

                        // Moving Platform Debug
                        protected UnityEngine.Vector3 point;
                        protected UnityEngine.Vector3 point2;
                        protected Bounds prevPosPlat;
                        protected Bounds startPosMotor;
                        protected Bounds movedPosMotor;

                        // Iteration Debug
                        protected int iterationsUsed;
                        protected Bounds[] iterationBounds;

                        protected Bounds ladderArea;
                        protected Bounds ladderBottomArea;
                        protected Bounds ladderTopArea;

                        protected LadderZone currentLadderZone;
                        protected float normalizedXMovement;
                        protected float normalizedYMovement;
                        protected MotorState currentMotorState;
                        [SerializeField]
                        [EnumFlagsAttribute]
                        protected CollidedSurface collidingAgainst;
                        protected CollidedArea inArea;
                        protected bool isFacingLeft;
                        protected float amountFastFallen;
                        protected float amountFallen;
                        protected float amountJumpedFor;
                        protected bool isFallFast;
                        protected bool isOnSlope;
                        protected bool isIgnoreGravity;

                        protected JumpState jumpState = new JumpState();
                        protected DashState dashState = new DashState();
                        protected WallState wallState = new WallState();
                        protected MovingPlatformState movingPlatformState = new MovingPlatformState();

                        #endregion

                        #region Properties
                        public LadderZone CurrentLadderZone
                        {
                            get
                            {
                                return this.currentLadderZone;
                            }
                        }

                        /// <summary>
                        /// Set the x movement direction. This is multiplied by the max speed. -1 is full left, 1 is full right. Higher numbers will
                        /// result in faster speed.
                        /// </summary>
                        public float NormalizedXMovement
                        {
                            get
                            {
                                return this.normalizedXMovement;
                            }

                            set
                            {
                                this.normalizedXMovement = value;
                            }
                        }

                        /// <summary>
                        /// Set the y movement direction. This is multiplied by the max speed. -1 is full left, 1 is full right. Higher numbers will
                        /// result in faster speed.
                        /// Only used for ladders
                        /// </summary>
                        public float NormalizedYMovement
                        {
                            get
                            {
                                return this.normalizedYMovement;
                            }

                            set
                            {
                                this.normalizedYMovement = value;
                            }
                        }

                        /// <summary>
                        /// Set the time scale for the motor. This is independent of the global time scale. Negative values are not supported.
                        /// </summary>
                        public float TimeScale
                        {
                            get
                            {
                                return this.timeScale;
                            }
                            set
                            {
                                if (value > 0)
                                {
                                    if (this.timeScale != 0)
                                    {
                                        ReadjustTimers(this.timeScale / value);
                                    }
                                    else
                                    {
                                        ReadjustTimers(this.savedTimeScale / value);
                                    }
                                }

                                this.savedTimeScale = this.timeScale;
                                this.timeScale = value;

                                if (this.timeScale < 0)
                                {
                                    this.timeScale = 0;
                                }
                            }
                        }

                        /// <summary>
                        /// The velocity of the motor. This should be queried instead of the rigidbody's velocity. Setting this during a dash doesn't
                        /// have any meaning.
                        /// </summary>
                        public UnityEngine.Vector2 Velocity
                        {
                            get
                            {
                                return IsDashing ? this.dashState.dashDir * GetDashSpeed() : this.velocity;
                            }
                            set
                            {
                                this.velocity = value;
                            }
                        }

                        /// <summary>
                        /// Call this to get state information about the motor. This will be information such as if the object is in the air or on the
                        /// ground. This can be used to set the appropriate animations.
                        /// </summary>
                        public MotorState CurrentMotorState
                        {
                            get
                            {
                                return this.currentMotorState;
                            }
                        }

                        /// <summary>
                        /// The surfaces the motor is currently colliding against.
                        /// </summary>
                        public CollidedSurface CollidingAgainst
                        {
                            get
                            {
                                return this.collidingAgainst;
                            }
                        }

                        /// <summary>
                        /// The Areas the motor is currently in.
                        /// </summary>
                        public CollidedArea InArea
                        {
                            get
                            {
                                return this.inArea;
                            }
                        }

                        /// <summary>
                        /// Since the motor needs to know the facing of the object, this information is made available to anyone else who might need
                        /// it.
                        /// </summary>
                        public bool IsFacingLeft
                        {
                            get
                            {
                                return this.isFacingLeft;
                            }

                            set
                            {
                                this.isFacingLeft = value;
                            }
                        }

                        /// <summary>
                        /// Returns the direction of the current dash. If not dashState then returns UnityEngine.Vector2.zero.
                        /// </summary>
                        public UnityEngine.Vector2 DashDirection
                        {
                            get
                            {
                                return IsDashing ? this.dashState.dashDir : UnityEngine.Vector2.zero;
                            }
                        }

                        /// <summary>
                        /// Returns the amount of distance dashed. If not dashState then returns 0.
                        /// </summary>
                        public float DistanceDashed
                        {
                            get
                            {
                                return IsDashing ? this.dashState.distanceDashed : 0;
                            }
                        }

                        /// <summary>
                        /// This is the distance calculated for dashed. Not be confused with distanceDashed. This doesn't care if the motor has
                        /// hit a wall.
                        /// </summary>
                        public float DashDistanceCalculated
                        {
                            get
                            {
                                return IsDashing ? this.dashState.distanceCalculated : 0;
                            }
                        }

                        /// <summary>
                        /// If the motor is currently able to dash.
                        /// </summary>
                        public bool CanDash
                        {
                            get { return this.dashState.cooldownFrames < 0; }
                        }

                        /// <summary>
                        /// Returns the amount of distance the motor has fallen fast.
                        /// </summary> 
                        public float AmountFastFallen
                        {
                            get
                            {
                                return this.amountFastFallen;
                            }
                        }

                        /// <summary>
                        /// Returns the amount of distance the motor has fallen. Includes fallen fast distance.
                        /// </summary>
                        public float AmountFallen
                        {
                            get
                            {
                                return this.amountFallen;
                            }
                        }

                        /// <summary>
                        /// Returns the amount the motor has jumped. This ceases to keep calculating after the motor starts to come down.
                        /// </summary>
                        public float AmountJumpedFor
                        {
                            get
                            {
                                return this.amountJumpedFor;
                            }
                        }

                        /// <summary>
                        /// Set this true to have the motor fall faster. Set to false to fall at normal speeds.
                        /// </summary>
                        public bool IsFallFast
                        {
                            get
                            {
                                return this.isFallFast;
                            }

                            set
                            {
                                this.isFallFast = value;
                            }
                        }

                        /// <summary>
                        /// If jumpingHeld is set to true then the motor will jump further. Set to false if jumping isn't 'isHeld'.
                        /// </summary>
                        public bool IsJumpingHeld
                        {
                            get
                            {
                                return this.jumpState.isHeld;
                            }

                            set
                            {
                                // Since we set isHeld to true on isPressed, we only set to false here. This prevent isHeld from being set after a release.
                                if (!value)
                                {
                                    this.jumpState.isHeld = false;
                                }

                            }
                        }

                        /// <summary>
                        /// Setting frozen to true will put the motor in a 'frozen' state. All information will be saved and set once unfrozen
                        /// (the motor also reduces gravity to 0).
                        ///
                        /// Note: This isn't a way to turn off the motor. To turn off the motor, simply set the script to disabled.
                        /// Note: No delegate (onXXX) will be called
                        /// </summary>
                        public bool IsFrozen
                        {
                            get
                            {
                                return this.isFrozen;
                            }
                            set
                            {
                                if (this.isFrozen != value)
                                {
                                    this.isFrozen = value;

                                    // do not use ChangeState, because delegates will be called
                                    if (this.isFrozen)
                                    {
                                        this.prevState = this.currentMotorState;
                                        this.currentMotorState = MotorState.Frozen;
                                    }
                                    else
                                    {
                                        this.currentMotorState = this.prevState;
                                    }
                                }
                            }
                        }

                        /// <summary>
                        /// Returns the moving platform that the motor is coupled with. If null then no moving platform.
                        /// </summary>
                        public RaycastPhysicsMovingPlatformMotorComponent ConnectedPlatform
                        {
                            get { return this.movingPlatformState.platform; }
                        }

                        /// <summary>
                        /// Whether or not the motor is on a slope. This can be simply walking on a slope or slipping.
                        /// </summary>
                        public bool IsOnSlope
                        {
                            get
                            {
                                return this.isOnSlope;
                            }
                        }

                        ///<summary>
                        /// Is the motor Dashing?
                        ///</summary>
                        public bool IsDashing
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.Dashing;
                            }
                        }

                        ///<summary>
                        /// Is the motor Jumping? include walljumps and airjumps.
                        ///</summary>
                        public bool IsJumping
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.Jumping;
                            }
                        }

                        ///<summary>
                        /// is the motor Jumping? include walljumps and airjumps.
                        ///</summary>
                        public bool IsUserHandled
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.FreedomState;
                            }
                        }

                        ///<summary>
                        /// is the motor falling? do not include failling fast.
                        ///</summary>
                        public bool IsFalling
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.Falling;
                            }
                        }
                        ///<summary>
                        /// is the motor falling fast? do not include failling (normal).
                        ///</summary>
                        public bool IsFallingFast
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.FallingFast;
                            }
                        }
                        ///<summary>
                        /// is the motor stick to a wall?
                        /// Use PressingIntoLeftWall, PressingIntoRightWall to know what wall.
                        ///</summary>
                        public bool IsWallSticking
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.WallSticking;
                            }
                        }
                        ///<summary>
                        /// Motor is standing onto something?
                        ///</summary>
                        public bool IsOnGround
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.OnGround;
                            }
                        }
                        ///<summary>
                        /// Motor is Slipping?
                        ///</summary>
                        public bool IsSlipping
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.Slipping;
                            }
                        }
                        ///<summary>
                        /// Motor is on corner?
                        ///</summary>
                        public bool IsOnCorner
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.OnCorner;
                            }
                        }
                        ///<summary>
                        /// Motor is sliding on wall?
                        ///</summary>
                        public bool IsWallSliding
                        {
                            get
                            {
                                return this.currentMotorState == MotorState.WallSliding;
                            }
                        }
                        ///<summary>
                        /// Motor is in mid air?
                        ///</summary>
                        public bool IsInAir
                        {
                            get
                            {
                                // TODO dashState on mid-air should be considered?
                                return IsJumping || IsFalling || IsFallingFast;
                            }
                        }
                        ///<summary>
                        /// Motor is sticking on a wall or on corner or slidding on a wall.
                        ///</summary>
                        public bool IsOnWall
                        {
                            get
                            {
                                return IsWallSliding || IsOnCorner || IsWallSticking;
                            }
                        }
                        ///<summary>
                        /// Motor is standing on the floor. Does not include Sliding
                        ///</summary>
                        public bool IsGrounded
                        {
                            get
                            {
                                return (HasFlag(CollidedSurface.Ground) || this.isOnSlope) &&
                                       !IsJumping &&
                                       (this.isOnSlope && UnityEngine.Vector2.Dot(this.velocity, this.slopeNormal) <= NEAR_ZERO ||
                                       this.velocity.y <= NEAR_ZERO);
                            }
                        }

                        ///<summary>
                        // On slope that cannot walk motor will be forced to slip down.
                        ///</summary>
                        public bool IsForceSlipping
                        {
                            get
                            {
                                return this.isOnSlope && UnityEngine.Vector3.Dot(UnityEngine.Vector3.up, this.slopeNormal) < this.dotAllowedForSlopes;
                            }
                        }

                        public bool IsOnLadder
                        {
                            get
                            {
                                return (this.inArea & CollidedArea.Ladder) == CollidedArea.Ladder;
                            }
                        }

                        #endregion

                        #region Public Functions

                        #region Jump

                        /// <summary>
                        /// Call this to have the GameObject try to jump, once called it will be handled in the FixedUpdate tick. The y axis is
                        /// considered jump.
                        /// </summary>
                        public virtual void Jump()
                        {
                            this.jumpState.isPressed = true;
                            this.jumpState.activatedGraceFrames = GetFrameCount(this.jumpWindowWhenActivated);
                            this.jumpState.height = this.jumpHeight;

                            // Consider jumping isHeld in case there are multiple fixed ticks before the next update tick.
                            // This is useful as jumpingHeld may not be set to true with a GetButton() call.
                            this.jumpState.isHeld = true;
                        }

                        /// <summary>
                        /// Jump that allows a custom height.
                        /// </summary>
                        /// <param name="customHeight">The height the motor should jump to. The extraJumpHeight is still applicable.</param>
                        public void Jump(float customHeight)
                        {
                            Jump();
                            this.jumpState.height = customHeight;
                        }

                        /// <summary>
                        /// This will isForce a jump to occur even if the motor doesn't think a jump is valid. This function will not work if the motor
                        /// is dashState.
                        /// </summary>
                        public virtual void ForceJump()
                        {
                            Jump();
                            this.jumpState.isForce = true;
                        }

                        /// <summary>
                        /// Force a jump with a custom height.
                        /// </summary>
                        /// <param name="customHeight">The height the motor should jump to. The extraJumpHeight is still applicable.</param>
                        public virtual void ForceJump(float customHeight)
                        {
                            ForceJump();
                            this.jumpState.height = customHeight;
                        }

                        /// <summary>
                        /// Call to end a jump. Causes the motor to stop calculated isHeld speed for a jump.
                        /// </summary>
                        public virtual void EndJump()
                        {
                            if (IsJumping)
                            {
                                this.jumpState.isPressed = false;
                                this.jumpState.activatedGraceFrames = -1;
                                this.jumpState.numAirJumps = 0;
                                ChangeState(MotorState.Falling);
                            }
                        }

                        /// <summary>
                        /// Resets the state for air jumps by setting the counter to 0.
                        /// </summary>
                        public void ResetAirJump()
                        {
                            this.jumpState.numAirJumps = 0;
                        }

                        #endregion

                        #region Dash

                        /// <summary>
                        /// Reset the cooldown for dash.
                        /// </summary>
                        public virtual void ResetDashCooldown()
                        {
                            this.dashState.cooldownFrames = -1;
                        }

                        /// <summary>
                        /// Decouples the motor from the platform. This could be useful for a platform that throw the motor in the air. Call this
                        /// when when the motor should disconnect then set the appropriate velocity.
                        /// </summary>
                        public virtual void DisconnectFromPlatform()
                        {
                            this.movingPlatformState.platform = null;
                        }

                        /// <summary>
                        /// Call this to have the motor try to dash, once called it will be handled in the FixedUpdate tick.
                        /// This causes the object to dash along their facing (if left or right for side scrollers).
                        /// </summary>
                        public virtual void Dash()
                        {
                            this.dashState.isPressed = true;
                            this.dashState.isDashWithDirection = false;
                        }

                        /// <summary>
                        /// Forces the motor to dash regardless if the motor thinks it is valid or not.
                        /// </summary>
                        public virtual void ForceDash()
                        {
                            Dash();
                            this.dashState.isForce = true;
                        }

                        /// <summary>
                        /// Send a direction vector to dash allow dashState in a specific direction.
                        /// </summary>
                        /// <param name="dir">The normalized direction of the dash.</param>
                        public virtual void Dash(UnityEngine.Vector2 dir)
                        {
                            this.dashState.isPressed = true;
                            this.dashState.isDashWithDirection = true;
                            this.dashState.dashDir = dir;
                        }

                        /// <summary>
                        /// Forces a dash along a specified direction.
                        /// </summary>
                        /// <param name="dir">The normalized direction of the dash.</param>
                        public virtual void ForceDash(UnityEngine.Vector2 dir)
                        {
                            Dash(dir);
                            this.dashState.isForce = true;
                        }

                        /// <summary>
                        /// Call to end dash immediately.
                        /// </summary>
                        public virtual void EndDash()
                        {
                            // If dashState then end now.
                            if (IsDashing)
                            {
                                this.dashState.cooldownFrames = GetFrameCount(this.dashCooldown);
                                this.dashState.isPressed = false;
                                this.dashState.gravityEnabledFrames = GetFrameCount(this.endDashNoGravityDuration);

                                this.velocity = this.dashState.dashDir * GetDashSpeed();

                                ChangeState(IsGrounded ? MotorState.OnGround : MotorState.Falling);
                            }
                        }

                        #endregion

                        #region Freedom Area

                        public virtual void FreedomAreaEnter()
                        {
                            this.inArea |= CollidedArea.FreedomArea;
                        }

                        public virtual bool IsOnFreedomArea()
                        {
                            return (this.inArea & CollidedArea.FreedomArea) == CollidedArea.FreedomArea;
                        }

                        public virtual void FreedomAreaExit()
                        {
                            this.inArea &= ~CollidedArea.FreedomArea;
                            // leaving a freedom area also implies that cannot be a ladder
                            this.inArea &= ~CollidedArea.Ladder;

                            // leave freedom state, and start falling
                            if (IsUserHandled)
                            {
                                FreedomStateExit();
                            }
                        }

                        // When enter freedom state velocity is reset, so no inertia
                        public virtual void FreedomStateEnter()
                        {
                            this.velocity.y = 0;
                            this.velocity.x = 0;
                            this.isIgnoreGravity = true;
                            ChangeState(MotorState.FreedomState);
                        }

                        public virtual void FreedomStateExit()
                        {
                            this.isIgnoreGravity = false;
                            ChangeState(MotorState.Falling);
                        }

                        #endregion

                        #region Ladder
                        
                        public virtual void LadderAreaEnter(Bounds area, float topHeight = 0, float bottomHeight = 0)
                        {
                            this.inArea |= CollidedArea.Ladder;
                            this.inArea |= CollidedArea.FreedomArea;

                            this.ladderArea = area;

                            // top - 8
                            // bottom + 8

                            this.ladderTopArea = new Bounds(
                                new UnityEngine.Vector3(area.center.x, area.center.y + area.extents.y - topHeight * 0.5f, 0),
                                new UnityEngine.Vector3(area.size.x, topHeight, 100)
                            );

                            this.ladderBottomArea = new Bounds(
                                new UnityEngine.Vector3(area.center.x, area.center.y - area.extents.y + bottomHeight * 0.5f, 0),
                                new UnityEngine.Vector3(area.size.x, bottomHeight, 100)
                            );
                        }

                        public virtual void LadderAreaExit()
                        {
                            this.inArea &= ~CollidedArea.Ladder;
                            // I may leave a freedomArea, i may not... exitFreedomArea must be called

                            this.ladderBottomArea = new Bounds(UnityEngine.Vector3.zero, UnityEngine.Vector3.zero);
                            this.ladderTopArea = new Bounds(UnityEngine.Vector3.zero, UnityEngine.Vector3.zero);
                            this.ladderArea = new Bounds(UnityEngine.Vector3.zero, UnityEngine.Vector3.zero);
                        }

                        public virtual void SetLadderZone(LadderZone z)
                        {
                            this.currentLadderZone = z;
                        }

                        #endregion

                        #region Restricted Area

                        public virtual void SetRestrictedArea(Bounds b, bool disableTop = false)
                        {
                            this.restrictedArea = b;

                            this.restrictedAreaTR = b.center + b.extents;
                            this.restrictedAreaBL = b.center - b.extents;

                            if (disableTop)
                            {
                                this.restrictedAreaTR.y = Mathf.Infinity;
                            }
                        }

                        public virtual void ClearRestrictedArea()
                        {
                            this.restrictedAreaTR.x = Mathf.Infinity;
                            this.restrictedAreaTR.y = Mathf.Infinity;
                            this.restrictedAreaBL.x = -Mathf.Infinity;
                            this.restrictedAreaBL.y = -Mathf.Infinity;
                        }


                        public virtual void EnableRestrictedArea()
                        {
                            if (!(this.restrictedAreaTR.x == Mathf.Infinity &&
                                  this.restrictedAreaTR.y == Mathf.Infinity &&
                                  this.restrictedAreaBL.x == -Mathf.Infinity &&
                                  this.restrictedAreaBL.y == -Mathf.Infinity))
                            {
                                this.inArea |= CollidedArea.Restricted;
                            }
                        }

                        public virtual bool IsRestricted()
                        {
                            return (this.inArea & CollidedArea.Restricted) == CollidedArea.Restricted;
                        }

                        public virtual void DisableRestrictedArea()
                        {
                            this.inArea &= ~CollidedArea.Restricted;
                        }

                        #endregion

                        #region Queries

                        ///<summary>
                        // Given a game object return if this motor consider the object a moving platform.
                        ///</summary>
                        public virtual bool IsMovingPlatform(UnityEngine.GameObject obj)
                        {
                            return ((0x1 << obj.layer) & this.movingPlatformLayerMask) != 0;
                        }
                        ///<summary>
                        // Given a game object return if this motor consider the object as static.
                        ///</summary>
                        public virtual bool IsStatic(UnityEngine.GameObject obj)
                        {
                            return ((0x1 << obj.layer) & this.staticEnvLayerMask) != 0;
                        }

                        #endregion

                        #endregion

                        #region Lifecycle Functions

                        protected virtual void Awake()
                        {
                            SetDashFunctions();
                            this.collider2D = GetComponent<Collider2D>();
                            this.rigidbody2D = GetComponent<Rigidbody2D>();

                            if (this.enableIterationDebug)
                            {
                                this.iterationBounds = new Bounds[2 + this.numOfIterations];
                            }
                        }

                        protected virtual void OnEnable()
                        {
                            if (this.rigidbody2D != null)
                            {
                                this.velocity = this.rigidbody2D.velocity;
                                this.isOriginalKinematic = this.rigidbody2D.isKinematic;
                                this.rigidbody2D.isKinematic = true;
                            }
                        }

                        protected virtual void Start()
                        {
                            this.previousLoc = this.collider2D.bounds.center;
                            // initial set, do not use ChangeState
                            this.currentMotorState = MotorState.Falling;
                            this.wallJumpVector = Quaternion.AngleAxis(this.wallJumpAngle, UnityEngine.Vector3.forward) * UnityEngine.Vector3.right;
                            this.wallJumpAwayVector = Quaternion.AngleAxis(this.wallJumpAwayAngle, UnityEngine.Vector3.forward) * UnityEngine.Vector3.right;
                            this.currentWallJumpDegree = this.wallJumpAngle;

                            this._cornerDistanceCheck = Mathf.Sqrt(2 * this.envCheckDistance * this.envCheckDistance);
                            this.distanceFromEnvCorner = Mathf.Sqrt(2 * this.minDistanceFromEnv * this.minDistanceFromEnv);

                            this.distanceToBoundsCorner = (this.collider2D.bounds.max - this.collider2D.bounds.center).magnitude;

                            this.bottomRight = new UnityEngine.Vector2(1, -1).normalized;

                            SetSlopeDegreeAllowed();

                            this.currentLadderZone = LadderZone.Bottom;
                            this.collisionMask = this.staticEnvLayerMask | this.movingPlatformLayerMask;
                        }

                         protected virtual void FixedUpdate()
                        {
                            // Frozen?
                            if (IsFrozen || TimeScale == 0)
                            {
                                return;
                            }

                            UpdateTimers();

                            // update this.collisionMask in case it's updated by user
                            this.collisionMask = this.staticEnvLayerMask | this.movingPlatformLayerMask;

                            float time = Time.fixedDeltaTime;
                            int iterations = 0;
                            this.iterationsUsed = 0;

                            while (time > 0 && iterations < this.numOfIterations)
                            {
                                time = UpdateMotor(time);
                                iterations++;

                                if (this.enableIterationDebug)
                                {
                                    this.iterationBounds[(this.iterationsUsed++) + 2] = this.collider2D.bounds;
                                }
                            }

                            // check ladder zone
                            if (this.ladderBottomArea.Contains(this.collider2D.bounds.center))
                            {
                                this.currentLadderZone = LadderZone.Bottom;
                            }
                            else if (this.ladderTopArea.Contains(this.collider2D.bounds.center))
                            {
                                this.currentLadderZone = LadderZone.Top;
                            }
                            else if (this.ladderArea.Contains(this.collider2D.bounds.center))
                            {
                                this.currentLadderZone = LadderZone.Middle;
                            }
                        }

                        protected virtual void OnDisable()
                        {
                            if (this.rigidbody2D != null)
                            {
                                this.rigidbody2D.velocity = this.velocity;
                                this.rigidbody2D.isKinematic = this.isOriginalKinematic;
                            }
                        }

                        #endregion

                        #region Private Helpers

                        protected virtual void SetSlopeDegreeAllowed()
                        {
                            UnityEngine.Vector3 v = Quaternion.AngleAxis(this.angleAllowedForMoving, UnityEngine.Vector3.back) * UnityEngine.Vector3.up;
                            this.dotAllowedForSlopes = UnityEngine.Vector3.Dot(UnityEngine.Vector3.up, v);
                            this.currentSlopeDegreeAllowed = this.angleAllowedForMoving;
                        }

                        protected static UnityEngine.Vector2 GetPointOnBounds(Bounds bounds, UnityEngine.Vector3 toPoint)
                        {
                            // From http://stackoverflow.com/questions/4061576/finding-points-on-a-rectangle-at-a-given-angle
                            float angle = UnityEngine.Vector3.Angle(UnityEngine.Vector3.right, toPoint);

                            if (toPoint.y < 0)
                            {
                                angle = 360f - angle;
                            }

                            float multiplier = 1f;

                            if ((angle >= 0f && angle < 45f) ||
                                angle > 315f ||
                                (angle >= 135f && angle < 225f))
                            {

                                if (angle >= 135f && angle < 225f)
                                {
                                    multiplier = -1f;
                                }

                                return new UnityEngine.Vector2(
                                    multiplier * bounds.size.x / 2 + bounds.center.x,
                                    bounds.center.y + multiplier * ((bounds.size.x / 2) * Mathf.Tan(angle * Mathf.Deg2Rad)));
                            }

                            if (angle >= 225f)
                            {
                                multiplier = -1f;
                            }

                            return new UnityEngine.Vector2(
                                bounds.center.x + multiplier * bounds.size.y / (2 * Mathf.Tan(angle * Mathf.Deg2Rad)),
                                multiplier * bounds.size.y / 2 + bounds.center.y);
                        }

                        protected virtual void UpdateProperties()
                        {
                            if (this.angleAllowedForMoving != this.currentSlopeDegreeAllowed)
                            {
                                SetSlopeDegreeAllowed();
                            }

                            if (this.collider2D.bounds.size != this.prevColliderBounds.size)
                            {
                                this.distanceToBoundsCorner = this.collider2D.bounds.extents.magnitude;
                            }

                            this.toTransform = transform.position - this.collider2D.bounds.center;
                        }

                        protected virtual void UpdateTimers()
                        {
                            // All timers in the motor are countdowns and are considered valid so long as the timer is >= 0
                            this.dashState.cooldownFrames--;
                            this.dashState.gravityEnabledFrames--;
                            this.dashState.dashingFrames--;
                            this.wallState.cornerHangFrames--;
                            this.wallState.stickFrames--;
                            this.wallState.wallInteractionCooldownFrames--;
                            this.ignoreMovementFrames--;
                            this.jumpState.jumpAllowedAfterFallingGraceFrame--;
                            this.jumpState.activatedGraceFrames--;
                            this.jumpState.allowExtraFrames--;
                        }

                        protected virtual void ReadjustTimers(float multiplier)
                        {
                            this.dashState.cooldownFrames = Mathf.RoundToInt(this.dashState.cooldownFrames * multiplier);
                            this.dashState.gravityEnabledFrames = Mathf.RoundToInt(this.dashState.gravityEnabledFrames * multiplier);
                            this.dashState.dashingFrames = Mathf.RoundToInt(this.dashState.dashingFrames * multiplier);
                            this.wallState.cornerHangFrames = Mathf.RoundToInt(this.wallState.cornerHangFrames * multiplier);
                            this.wallState.stickFrames = Mathf.RoundToInt(this.wallState.stickFrames * multiplier);
                            this.wallState.wallInteractionCooldownFrames = Mathf.RoundToInt(this.wallState.wallInteractionCooldownFrames * multiplier);
                            this.ignoreMovementFrames = Mathf.RoundToInt(this.ignoreMovementFrames * multiplier);
                            this.jumpState.jumpAllowedAfterFallingGraceFrame = Mathf.RoundToInt(this.jumpState.jumpAllowedAfterFallingGraceFrame * multiplier);
                            this.jumpState.activatedGraceFrames = Mathf.RoundToInt(this.jumpState.activatedGraceFrames * multiplier);
                            this.jumpState.allowExtraFrames = Mathf.RoundToInt(this.jumpState.allowExtraFrames * multiplier);
                        }

                        protected virtual void UpdateVelocity()
                        {
                            // First, are we trying to dash?
                            if (this.enableDashes &&
                                (this.dashState.isPressed &&
                                this.dashState.cooldownFrames < 0 &&
                                !IsDashing ||
                                this.dashState.isForce))
                            {
                                StartDash();
                            }

                            this.dashState.isPressed = false;

                            if (!IsDashing)
                            {
                                // If we have standard control then facing can change any frame.
                                SetFacing();

                                ApplyMovement();

                                if (IsForceSlipping)
                                {
                                    UnityEngine.Vector2 slopeDir = GetDownSlopeDir();

                                    if (this.isFallFast)
                                    {
                                        float increaseBy = -slopeDir.y * this.fastFallGravityMultiplier *
                                            Mathf.Abs(Physics2D.gravity.y) * GetDeltaTime();

                                        this.velocity += slopeDir * increaseBy;

                                        if (UnityEngine.Vector2.Dot(GetMovementDir(this.velocity.x), slopeDir) > NEAR_ZERO)
                                        {
                                            this.velocity = UnityEngine.Vector2.ClampMagnitude(this.velocity, -slopeDir.y * this.fastFallSpeed);
                                        }
                                    }
                                    else
                                    {
                                        float increaseBy = -slopeDir.y * this.gravityMultiplier *
                                            Mathf.Abs(Physics2D.gravity.y) * GetDeltaTime();

                                        this.velocity += slopeDir * increaseBy;

                                        if (UnityEngine.Vector2.Dot(GetMovementDir(this.velocity.x), slopeDir) > NEAR_ZERO)
                                        {
                                            this.velocity = UnityEngine.Vector2.ClampMagnitude(this.velocity, -slopeDir.y * this.fallSpeed);
                                        }
                                    }
                                }

                                // Handle jumping.
                                HandlePreJumping();

                                // Any wall interactions.
                                HandlePreWallInteraction();
                            }
                        }

                        protected virtual float MoveMotor()
                        {
                            if (IsDashing)
                            {
                                float normalizedTime = (float)(GetFrameCount(this.dashDuration) - this.dashState.dashingFrames) /
                                    GetFrameCount(this.dashDuration);

                                if (this.currentDashEasingFunction != this.dashEasingFunction)
                                {
                                    // This allows the easing function to change during runtime and cut down on unnecessary allocations.
                                    SetDashFunctions();
                                }

                                float distance = this.dashFunction(0, this.dashDistance, normalizedTime);

                                this.velocity = this.dashState.dashDir * GetDashSpeed();
                                MovePosition(this.collider2D.bounds.center + (UnityEngine.Vector3)this.dashState.dashDir * (distance - this.dashState.distanceCalculated));
                                this.dashState.distanceCalculated = distance;
                                // Right now dash only moves along a line, doesn't ever need to adjust. We don't need multiple iterations for that.
                                return 0;
                            }

                            UnityEngine.Vector3 curPos = this.collider2D.bounds.center;
                            UnityEngine.Vector3 targetPos = this.collider2D.bounds.center + (UnityEngine.Vector3)this.velocity * GetDeltaTime();

                            if (this.enableIterationDebug && this.currentDeltaTime == Time.fixedDeltaTime)
                            {
                                this.iterationBounds[0] = this.collider2D.bounds;

                                Bounds b = this.collider2D.bounds;
                                b.center = targetPos;

                                this.iterationBounds[1] = b;
                            }

                            MovePosition(this.collider2D.bounds.center + (UnityEngine.Vector3)this.velocity * GetDeltaTime());

                            if (this.numOfIterations == 1 || (targetPos - this.collider2D.bounds.center).sqrMagnitude < DISTANCE_TO_END_ITERATION * DISTANCE_TO_END_ITERATION)
                            {
                                return 0;
                            }

                            return Mathf.Lerp(this.currentDeltaTime, 0, (this.collider2D.bounds.center - curPos).magnitude / (targetPos - curPos).magnitude);
                        }

                        protected virtual void UpdateSurroundings(bool forceCheck)
                        {
                            bool currentOnSlope = this.isOnSlope;
                            UnityEngine.Vector2 currentSlopeNormal = this.slopeNormal;
                            bool wasGrounded = IsGrounded;
                            bool wasSlipping = IsForceSlipping;

                            if (forceCheck)
                            {
                                this.collidingAgainst = UpdateSurroundingStates(true);
                            }
                            else
                            {
                                // We may not need to check at all!
                                if (Velocity == UnityEngine.Vector2.zero && this.normalizedXMovement == 0)
                                {
                                    // No state change but if we are next to a slope we can't walk up then we should reset it to be handled.
                                    if (this.disallowedSlopeNormal != UnityEngine.Vector2.zero)
                                    {
                                        // we update to the last disallowed slope normal then as well. This will be worked out.
                                        this.isOnSlope = true;
                                        this.slopeNormal = this.disallowedSlopeNormal;
                                    }
                                }
                                else
                                {
                                    this.collidingAgainst = UpdateSurroundingStates(false);
                                }
                            }

                            // If we are dashState then we can bail.
                            if (IsDashing)
                            {
                                return;
                            }

                            // If we've moved on to a new slope (or to no slope from a slope) then we update our velocity vector to match
                            if (wasGrounded &&
                                Velocity != UnityEngine.Vector2.zero &&
                                (HasFlag(CollidedSurface.Ground) || HasFlag(CollidedSurface.SlopeLeft) || HasFlag(CollidedSurface.SlopeRight)) &&
                                !IsJumping &&
                                ((currentOnSlope && ((currentSlopeNormal != this.disallowedSlopeNormal) || !this.isOnSlope)) ||
                                currentOnSlope != this.isOnSlope))
                            {
                                // With our velocity pointing upwards, IsGrounded will return false. Since we want to stick to environments
                                // this will fix our velocity.
                                float speed;
                                float maxSpeed;

                                GetSpeedAndMaxSpeedOnGround(out speed, out maxSpeed);
                                    Debug.Log("b");
                                    Debug.Log(this.velocity);

                                this.velocity = GetMovementDir(Velocity.x) * Mathf.Abs(speed);
                                    Debug.Log(this.velocity);
                            }

                            this.disallowedSlopeNormal = UnityEngine.Vector2.zero;

                            if (wasGrounded && !wasSlipping && IsForceSlipping)
                            {
                                UnityEngine.Vector2 moveDir = this.previousMoveDir;

                                if (Velocity != UnityEngine.Vector2.zero)
                                {
                                    moveDir = GetMovementDir(Velocity.x);
                                    this.previousMoveDir = moveDir;
                                }

                                // We weren't slipping but now we are, are we trying to go up a slope?
                                if (UnityEngine.Vector2.Dot(GetDownSlopeDir(), moveDir) < 0 &&
                                    Velocity.sqrMagnitude < this.minimumSpeedToMoveUpSlipperySlope * this.minimumSpeedToMoveUpSlipperySlope)
                                {
                                    Debug.Log("a");
                                    // Don't allow.
                                    this.velocity = UnityEngine.Vector3.Project(Velocity, this.slopeNormal);
                                    this.disallowedSlopeNormal = this.slopeNormal;
                                    this.slopeNormal = currentSlopeNormal;
                                    this.isOnSlope = currentOnSlope;
                                }
                            }
                        }

                        protected virtual void UpdateState(bool forceSurroundingsCheck)
                        {
                            // Since this is in UpdateState, we can end dashState if the timer is at 0.
                            if (IsDashing && this.dashState.dashingFrames <= 0)
                            {
                                EndDash();
                            }

                            UpdateSurroundings(forceSurroundingsCheck);

                            if (IsDashing)
                            {
                                // Still dashState, nothing else matters.
                                this.dashState.distanceDashed += (this.collider2D.bounds.center - this.previousLoc).magnitude;
                                return;
                            }

                            CheckWallInteractionValidity();

                            if (HasFlag(CollidedSurface.Ground))
                            {
                                if (IsFalling || IsFallingFast)
                                {
                                    if (this.OnLanded != null)
                                    {
                                        this.OnLanded();
                                    }
                                }
                            }

                            AttachToMovingPlatforms();

                            UpdateInformationFromMovement();

                            HandlePostWallInteraction();

                            // If our state is not in the air then open up the possibility of air jumps (we need to be able to air jump if
                            // we walk off an edge so it can't be based of when a jump occurred).
                            if (!IsInAir)
                            {
                                this.jumpState.numAirJumps = 0;
                            }

                            if (this.velocity.y > 0 && HasFlag(CollidedSurface.Ceiling))
                            {
                                this.velocity.y = 0;
                            }

                            if (IsGrounded)
                            {
                                UnityEngine.Vector3 slopeDir = GetDownSlopeDir();

                                if (IsForceSlipping && this.velocity != UnityEngine.Vector2.zero && Mathf.Sign(this.velocity.x) == Mathf.Sign(slopeDir.x))
                                {
                                    float speed = this.velocity.magnitude;

                                    this.velocity = slopeDir * UnityEngine.Vector2.Dot(this.velocity / speed, slopeDir) * speed;
                                    ChangeState(MotorState.Slipping);
                                }
                                else
                                {
                                    ChangeState(MotorState.OnGround);

                                    if (this.isOnSlope)
                                    {
                                        this.velocity = UnityEngine.Vector3.Project(Velocity, GetDownSlopeDir());
                                    }
                                    else
                                    {
                                        this.velocity.y = 0;
                                    }
                                }
                            }
                            else if (IsOnGround || IsSlipping)
                            {
                                ChangeState(MotorState.Falling);
                            }

                            if (this.movingPlatformState.IsOnPlatform)
                            {
                                this.movingPlatformState.previousPos = this.movingPlatformState.platform.Position;
                            }

                            SetLastJumpType();
                        }

                        protected virtual float UpdateMotor(float deltaTime)
                        {
                            this.currentDeltaTime = deltaTime;

                            UpdateProperties();

                            // The update phase is broken up into four phases each with certain responsibilities.

                            // Phase One: Update internal state if something forced the motor into an unexpected state. This can be because
                            //            a moving platform moved into us or our collider changed. This could means three UpdateStates, which is
                            //            expensive, but we need to know our state.

                            if (this.prevColliderBounds != this.collider2D.bounds)
                            {
                                UpdateState(true);
                            }

                            if (UpdateMovingPlatform())
                            {
                                UpdateState(true);
                            }
                            // Phase Two: Update internal representation of velocity
                            UpdateVelocity();

                            // Phase Three: Move the motor to the new location (and well update falling)
                            HandleFalling();

                            deltaTime = MoveMotor();

                            // Phase Four: Update internal state so it can be accurately queried and ready for next step. We have to isForce all sides
                            //             if there is moving platforms. They can move next to us, or away from us, without us knowing.
                            UpdateState(this.movingPlatformLayerMask != 0);

                            this.prevColliderBounds = this.collider2D.bounds;

                            return deltaTime;
                        }

                        protected virtual bool UpdateMovingPlatform()
                        {
                            if (this.movingPlatformLayerMask == 0)
                            {
                                // No moving platforms, early bail.
                                return false;
                            }

                            if (this.enableMovingPlatformDebug)
                            {
                                this.point = new UnityEngine.Vector3();
                                this.point2 = new UnityEngine.Vector3();
                                this.prevPosPlat = new Bounds();
                                this.movedPosMotor = new Bounds();
                                this.startPosMotor = new Bounds();
                            }

                            // Update location if on a moving platform.
                            if (!IsDashing && this.movingPlatformState.IsOnPlatform)
                            {
                                UnityEngine.Vector3 toNewPos = this.movingPlatformState.platform.Position - this.movingPlatformState.previousPos;
                                transform.position += toNewPos;

                                this.prevColliderBounds = this.collider2D.bounds;
                            }

                            Bounds checkBounds = this.collider2D.bounds;
                            checkBounds.extents += new UnityEngine.Vector3(this.minDistanceFromEnv, this.minDistanceFromEnv);

                            Collider2D col = Physics2D.OverlapArea(checkBounds.min, checkBounds.max, this.collisionMask);

                            if (col != null)
                            {
                                SeparateFromEnvirionment();
                                return true;
                            }

                            return false;
                        }

                        protected virtual void SeparateFromEnvirionment()
                        {
                            // We'll start with the corners
                            RaycastAndSeparate(
                                (this.collider2D.bounds.max - this.collider2D.bounds.center) / this.distanceToBoundsCorner,
                                this.distanceToBoundsCorner + this.distanceFromEnvCorner);

                            RaycastAndSeparate(
                                (new UnityEngine.Vector3(this.collider2D.bounds.max.x, this.collider2D.bounds.min.y) - this.collider2D.bounds.center) /
                                this.distanceToBoundsCorner,
                                this.distanceToBoundsCorner + this.distanceFromEnvCorner);

                            RaycastAndSeparate(
                                (this.collider2D.bounds.min - this.collider2D.bounds.center) / this.distanceToBoundsCorner,
                                this.distanceToBoundsCorner + this.distanceFromEnvCorner);

                            RaycastAndSeparate(
                                (new UnityEngine.Vector3(this.collider2D.bounds.min.x, this.collider2D.bounds.max.y) - this.collider2D.bounds.center) /
                                this.distanceToBoundsCorner,
                                this.distanceToBoundsCorner + this.distanceFromEnvCorner);

                            // These are more expensive...they only need to be done if piece of environment smaller (width/height) than the motor
                            // can move into it. Otherwise just set additionalRaycastsPerSide to 0.

                            // Top/Bottom
                            for (int i = 0; i < this.additionalRaycastsPerSide; i++)
                            {
                                UnityEngine.Vector2 dir = new UnityEngine.Vector2(
                                    Mathf.Lerp(this.collider2D.bounds.min.x, this.collider2D.bounds.max.x, (float)(i + 1) / (this.additionalRaycastsPerSide + 1)),
                                    this.collider2D.bounds.max.y) - (UnityEngine.Vector2)this.collider2D.bounds.center;

                                float distance = dir.magnitude;

                                RaycastAndSeparate(dir / distance, distance + this.minDistanceFromEnv);
                                RaycastAndSeparate(-dir / distance, distance + this.minDistanceFromEnv);
                            }

                            // Right/Left
                            for (int i = 0; i < this.additionalRaycastsPerSide; i++)
                            {
                                UnityEngine.Vector2 dir = new UnityEngine.Vector2(
                                    this.collider2D.bounds.max.x,
                                    Mathf.Lerp(this.collider2D.bounds.min.y, this.collider2D.bounds.max.y,
                                    (float)(i + 1) / (this.additionalRaycastsPerSide + 1))) - (UnityEngine.Vector2)this.collider2D.bounds.center;

                                float distance = dir.magnitude;

                                RaycastAndSeparate(dir / distance, distance + this.minDistanceFromEnv);
                                RaycastAndSeparate(-dir / distance, distance + this.minDistanceFromEnv);
                            }
                        }

                        protected virtual void RaycastAndSeparate(UnityEngine.Vector2 dir, float distance)
                        {
                            RaycastHit2D hit = GetClosestHit(this.collider2D.bounds.center, dir, distance, false, true);

                            if (hit.collider != null)
                            {
                                UnityEngine.Vector2 pointOnCol = GetPointOnBounds(this.collider2D.bounds, -hit.normal);

                                UnityEngine.Vector3 toPointOnCol = pointOnCol - hit.point;
                                UnityEngine.Vector3 pointToSepFrom = (UnityEngine.Vector3)hit.point + UnityEngine.Vector3.Project(toPointOnCol, -hit.normal);

                                if (this.enableMovingPlatformDebug)
                                {
                                    this.point = hit.point;
                                    this.point2 = pointToSepFrom;
                                    this.startPosMotor = this.collider2D.bounds;
                                }

                                this.transform.position += ((UnityEngine.Vector3)hit.point - pointToSepFrom) + (UnityEngine.Vector3)hit.normal * this.minDistanceFromEnv;

                                if (this.enableMovingPlatformDebug)
                                {
                                    this.movedPosMotor = this.collider2D.bounds;
                                }
                            }
                        }

                        protected virtual void CheckWallInteractionValidity()
                        {
                            this.isValidWallInteraction = false;

                            if (!this.enableWallSlides && !this.enableCornerGrabs && !this.enableWallSticks)
                            {
                                // Don't need the unnecessary check!
                                return;
                            }

                            UnityEngine.Vector2 min = this.collider2D.bounds.min;
                            UnityEngine.Vector2 max = this.collider2D.bounds.max;

                            if (HasFlag(CollidedSurface.LeftWall) &&
                                this.collidedNormals[DIRECTION_LEFT] == UnityEngine.Vector2.right &&
                                this.normalizedXMovement < 0)
                            {
                                max.x = this.collider2D.bounds.center.x;
                                min.x = min.x - this.envCheckDistance;
                            }
                            else if (HasFlag(CollidedSurface.RightWall) &&
                                     this.collidedNormals[DIRECTION_RIGHT] == UnityEngine.Vector2.left &&
                                     this.normalizedXMovement > 0)
                            {
                                min.x = this.collider2D.bounds.center.x;
                                max.x = max.x + this.envCheckDistance;
                            }
                            else
                            {
                                return;
                            }

                            min.y = max.y - this.collider2D.bounds.size.y * this.normalizedValidWallInteraction;

                            this.isValidWallInteraction = Physics2D.OverlapArea(min, max, this.collisionMask) != null;
                        }

                        protected virtual void SetLastJumpType()
                        {
                            if (IsOnGround)
                            {
                                this.jumpState.LastValidJump = JumpState.JumpType.Normal;
                            }
                            else if (this.enableWallJumps)
                            {
                                if (PressingIntoLeftWall())
                                {
                                    this.jumpState.LastValidJump = JumpState.JumpType.LeftWall;
                                }
                                else if (PressingIntoRightWall())
                                {
                                    this.jumpState.LastValidJump = JumpState.JumpType.RightWall;
                                }
                            }
                            else if (IsOnCorner)
                            {
                                this.jumpState.LastValidJump = JumpState.JumpType.Corner;
                            }

                            // We don't track air jumps as they are always valid in the air.
                            if (this.jumpState.isJumpTypeChanged && this.jumpState.LastValidJump != JumpState.JumpType.None)
                            {
                                this.jumpState.isJumpTypeChanged = false;
                                this.jumpState.jumpAllowedAfterFallingGraceFrame = GetFrameCount(this.jumpWindowWhenFalling);
                            }
                        }

                        protected virtual void UpdateInformationFromMovement()
                        {
                            float diffInPositions = Mathf.Abs(this.collider2D.bounds.center.y - this.previousLoc.y);

                            if (IsFalling || IsFallingFast)
                            {
                                this.amountFallen += diffInPositions;

                                if (IsFallingFast && this.velocity.y <= -this.fallSpeed)
                                {
                                    this.amountFastFallen += diffInPositions;
                                }
                                else
                                {
                                    this.amountFastFallen = 0;
                                }
                            }
                            else
                            {
                                this.amountFallen = 0;
                            }

                            // Jumps
                            if (IsJumping)
                            {
                                if (this.velocity.y <= 0)
                                {
                                    ChangeState(MotorState.Falling);
                                }
                                else
                                {
                                    this.amountJumpedFor += diffInPositions;
                                }
                            }
                        }

                        protected virtual bool HasFlag(CollidedSurface cs)
                        {
                            return (this.collidingAgainst & cs) != CollidedSurface.None;
                        }

                        protected virtual float GetDeltaTime()
                        {
                            return this.currentDeltaTime * TimeScale;
                        }

                        protected virtual void SetDashFunctions()
                        {
                            this.dashFunction = Easing.GetEasingFunction(this.dashEasingFunction);
                            this.dashDerivativeFunction = Easing.GetEasingFunctionDerivative(this.dashEasingFunction);
                            this.currentDashEasingFunction = this.dashEasingFunction;
                        }

                        protected virtual void AttachToMovingPlatforms()
                        {
                            if (this.movingPlatformLayerMask == 0)
                            {
                                // No platforms, no reason to do this
                                return;
                            }

                            RaycastPhysicsMovingPlatformMotorComponent previous = this.movingPlatformState.platform;
                            this.movingPlatformState.platform = null;
                            this.movingPlatformState.stuckToWall = CollidedSurface.None;

                            if (HasFlag(CollidedSurface.Ground) && IsMovingPlatform(this.collidersUpAgainst[DIRECTION_DOWN].gameObject))
                            {
                                this.movingPlatformState.platform = this.collidersUpAgainst[DIRECTION_DOWN].GetComponent<RaycastPhysicsMovingPlatformMotorComponent>();

                                if (this.isFallFast)
                                {
                                    if (this.movingPlatformState.platform.Velocity.y < -this.fastFallSpeed)
                                    {
                                        this.movingPlatformState.platform = null;
                                        this.velocity.y = -this.fastFallSpeed;
                                        this.collidingAgainst &= ~CollidedSurface.Ground;
                                    }
                                }
                                else
                                {
                                    if (this.movingPlatformState.platform.Velocity.y < -this.fallSpeed)
                                    {
                                        this.movingPlatformState.platform = null;
                                        this.velocity.y = -this.fallSpeed;
                                        this.collidingAgainst &= ~CollidedSurface.Ground;
                                    }
                                }
                            }
                            else if ((IsOnWall || IsGrounded) && PressingIntoLeftWall() && IsMovingPlatform(this.collidersUpAgainst[DIRECTION_LEFT].gameObject))
                            {
                                // We allow the motor to attach when pressing into a moving platform. This prevent jitter as it's moving away from
                                // the motor.
                                this.movingPlatformState.platform = this.collidersUpAgainst[DIRECTION_LEFT].GetComponent<RaycastPhysicsMovingPlatformMotorComponent>();
                                this.movingPlatformState.stuckToWall = CollidedSurface.LeftWall;
                            }
                            else if ((IsOnWall || IsGrounded) &&
                                PressingIntoRightWall() &&
                                IsMovingPlatform(this.collidersUpAgainst[DIRECTION_RIGHT].gameObject))
                            {
                                this.movingPlatformState.platform = this.collidersUpAgainst[DIRECTION_RIGHT].GetComponent<RaycastPhysicsMovingPlatformMotorComponent>();
                                this.movingPlatformState.stuckToWall = CollidedSurface.RightWall;
                            }

                            if (this.movingPlatformState.platform != null && this.movingPlatformState.platform != previous)
                            {
                                if (this.movingPlatformState.platform.onPlatformerMotorContact != null)
                                {
                                    this.movingPlatformState.platform.onPlatformerMotorContact.Invoke(this);
                                }
                            }
                        }

                        protected virtual void HandlePreJumping()
                        {
                            if (this.jumpState.activatedGraceFrames >= 0)
                            {
                                this.jumpState.isPressed = true;
                            }

                            if (this.currentWallJumpDegree != this.wallJumpAngle)
                            {
                                this.wallJumpVector = Quaternion.AngleAxis(this.wallJumpAngle, UnityEngine.Vector3.forward) * UnityEngine.Vector3.right;
                                this.currentWallJumpDegree = this.wallJumpAngle;
                            }

                            // If we're currently jumping and the jump button is still isHeld down ignore gravity to allow us to achieve the extra
                            // height.
                            if (IsJumping && this.jumpState.isHeld && this.jumpState.allowExtraFrames > 0)
                            {
                                this.isIgnoreGravity = true;
                            }
                            else if (this.isIgnoreGravity)
                            {
                                this.isIgnoreGravity = false;
                            }

                            // Jump?
                            if (this.jumpState.isPressed)
                            {
                                bool jumped = true;

                                // Jump might mean different things depending on the state.
                                if ((this.jumpState.LastValidJump == JumpState.JumpType.Normal && this.jumpState.jumpAllowedAfterFallingGraceFrame >= 0) ||
                                    IsOnGround ||
                                    IsSlipping ||
                                    this.jumpState.isForce)
                                {
                                    // Normal jump.
                                    if (IsForceSlipping)
                                    {
                                        this.velocity = this.slopeNormal * CalculateSpeedNeeded(this.jumpState.height);
                                    }
                                    else
                                    {
                                        this.velocity.y = CalculateSpeedNeeded(this.jumpState.height);
                                    }
                                }
                                else if (IsOnCorner || this.jumpState.LastValidJump == JumpState.JumpType.Corner && this.jumpState.jumpAllowedAfterFallingGraceFrame >= 0)
                                {
                                    // If we are on a corner then jump up.
                                    this.velocity = UnityEngine.Vector2.up * CalculateSpeedNeeded(this.jumpState.height) * this.cornerJumpMultiplier;
                                    this.ignoreMovementFrames = GetFrameCount(this.ignoreMovementAfterJump);

                                    if (this.OnCornerJump != null)
                                    {
                                        this.OnCornerJump();
                                    }
                                }
                                else if (this.enableWallJumps &&
                                    ((this.jumpState.LastValidJump == JumpState.JumpType.LeftWall && this.jumpState.jumpAllowedAfterFallingGraceFrame >= 0) ||
                                    (this.isValidWallInteraction && PressingIntoLeftWall())))
                                {
                                    // If jump was isPressed as we or before we entered the wall then just jump away.
                                    if (this.normalizedXMovement > 0)
                                    {
                                        // Jump away the wall
                                        this.velocity = this.wallJumpAwayVector * CalculateSpeedNeeded(this.jumpState.height) * this.wallJumpMultiplier;
                                    }
                                    else
                                    {
                                        // Regular wall jump
                                        this.velocity = this.wallJumpVector * CalculateSpeedNeeded(this.jumpState.height) * this.wallJumpMultiplier;
                                    }

                                    // It's likely the player is still pressing into the wall, ignore movement for a little amount of time.
                                    // TODO: Only ignore left movement?
                                    this.ignoreMovementFrames = GetFrameCount(this.ignoreMovementAfterJump);

                                    // If wall jump is allowed but not wall slide then double jump will not be allowed earlier, allow it now.
                                    this.jumpState.numAirJumps = 0;

                                    if (this.OnWallJump != null)
                                    {
                                        this.OnWallJump(UnityEngine.Vector2.right);
                                    }
                                }
                                else if (this.enableWallJumps &&
                                    ((this.jumpState.LastValidJump == JumpState.JumpType.RightWall && this.jumpState.jumpAllowedAfterFallingGraceFrame >= 0) ||
                                    (this.isValidWallInteraction && PressingIntoRightWall())))
                                {
                                    // If jump was isPressed as we or before we entered the wall then just jump away.
                                    if (this.normalizedXMovement < 0)
                                    {
                                        // Jump away the wall
                                        this.velocity = this.wallJumpAwayVector * CalculateSpeedNeeded(this.jumpState.height) * this.wallJumpMultiplier;
                                    }
                                    else
                                    {
                                        // Regular wall jump
                                        this.velocity = this.wallJumpVector * CalculateSpeedNeeded(this.jumpState.height) * this.wallJumpMultiplier;
                                    }

                                    this.velocity.x *= -1;

                                    this.ignoreMovementFrames = GetFrameCount(this.ignoreMovementAfterJump);
                                    this.jumpState.numAirJumps = 0;

                                    if (this.OnWallJump != null)
                                    {
                                        this.OnWallJump(UnityEngine.Vector2.left);
                                    }
                                }
                                else if (this.jumpState.numAirJumps < this.numOfAirJumps)
                                {
                                    this.velocity.y = CalculateSpeedNeeded(this.jumpState.height);
                                    this.jumpState.numAirJumps++;

                                    if (this.OnAirJump != null)
                                    {
                                        this.OnAirJump();
                                    }
                                }
                                else
                                {
                                    // Guess we aren't jumping!
                                    jumped = false;
                                }

                                if (jumped)
                                {
                                    this.jumpState.isPressed = false;
                                    this.jumpState.isForce = false;
                                    this.jumpState.allowExtraFrames = GetFrameCount(this.extraJumpHeight / CalculateSpeedNeeded(this.jumpState.height));
                                    this.amountJumpedFor = 0;
                                    this.movingPlatformState.platform = null;
                                    this.jumpState.LastValidJump = JumpState.JumpType.None;
                                    this.jumpState.activatedGraceFrames = -1;

                                    ChangeState(MotorState.Jumping);
                                }
                            }

                            this.jumpState.isPressed = false;
                        }

                        protected virtual bool PressingIntoLeftWall()
                        {
                            if (this.movingPlatformState.IsOnPlatform &&
                                this.movingPlatformState.stuckToWall == CollidedSurface.LeftWall &&
                                this.normalizedXMovement < -this.wallInteractionThreshold)
                            {
                                return true;
                            }

                            return HasFlag(CollidedSurface.LeftWall) && this.normalizedXMovement < -this.wallInteractionThreshold;
                        }

                        protected virtual bool PressingIntoRightWall()
                        {
                            if (this.movingPlatformState.IsOnPlatform &&
                                this.movingPlatformState.stuckToWall == CollidedSurface.RightWall &&
                                this.normalizedXMovement > this.wallInteractionThreshold)
                            {
                                return true;
                            }

                            return HasFlag(CollidedSurface.RightWall) && this.normalizedXMovement > this.wallInteractionThreshold;
                        }

                        protected virtual void HandlePostWallInteraction()
                        {
                            // We can grab corners or walls again.
                            if (!PressingIntoLeftWall() && !PressingIntoRightWall())
                            {
                                if (!this.wallState.canHangAgain)
                                {
                                    // Debounce time, mostly for platforms that are moving down and constantly triggering sticks
                                    this.wallState.wallInteractionCooldownFrames = GetFrameCount(this.wallInteractionCooldown);
                                }

                                this.wallState.canHangAgain = true;

                                if (IsOnCorner ||
                                    IsWallSticking ||
                                    IsWallSliding)
                                {
                                    ChangeState(MotorState.Falling);
                                }
                            }
                            else
                            {
                                if (IsOnCorner)
                                {
                                    if (!this.isValidWallInteraction || !CheckIfAtCorner())
                                    {
                                        ChangeState(MotorState.Falling);
                                    }
                                    else if (this.wallState.cornerHangFrames < 0)
                                    {
                                        ChangeState(this.enableWallSlides ? MotorState.WallSliding : MotorState.Falling);
                                    }
                                }

                                if (IsWallSticking)
                                {
                                    if (!this.isValidWallInteraction || !(PressingIntoLeftWall() || PressingIntoRightWall()))
                                    {
                                        ChangeState(MotorState.Falling);
                                    }
                                    else if (this.wallState.stickFrames < 0)
                                    {
                                        ChangeState(this.enableWallSlides ? MotorState.WallSliding : MotorState.Falling);
                                    }
                                }
                            }


                            if (HasFlag(CollidedSurface.Ground))
                            {
                                if (IsWallSliding || this.isIgnoreGravity)
                                {
                                    this.velocity.y = 0;
                                }

                                ChangeState(MotorState.OnGround);
                            }
                        }

                        /// <summary>
                        /// HandlePreWalInteraction is responsible for saying we are now on a wall and setting velocity to 0.
                        /// </summary>
                        protected virtual void HandlePreWallInteraction()
                        {
                            if (IsJumping || this.wallState.wallInteractionCooldownFrames >= 0 || HasFlag(CollidedSurface.Ground))
                            {
                                return;
                            }

                            // Corner grab?
                            if (this.enableCornerGrabs)
                            {
                                if (this.isValidWallInteraction && this.velocity.y <= 0 && CheckIfAtCorner() && this.wallState.canHangAgain)
                                {
                                    this.wallState.cornerHangFrames = GetFrameCount(this.cornerGrabDuration);
                                    this.wallState.canHangAgain = false;
                                    this.velocity = UnityEngine.Vector2.zero;
                                    ChangeState(MotorState.OnCorner);
                                    return;
                                }
                            }

                            // Wall Sticks
                            if (this.enableWallSticks)
                            {
                                if (this.isValidWallInteraction && this.velocity.y <= 0 && (PressingIntoLeftWall() || PressingIntoRightWall()) && this.wallState.canHangAgain)
                                {
                                    this.wallState.stickFrames = GetFrameCount(this.wallSticksDuration);
                                    this.velocity = UnityEngine.Vector2.zero;
                                    this.wallState.canHangAgain = false;
                                    ChangeState(MotorState.WallSticking);
                                    return;
                                }
                            }

                            // Wall slide?
                            if (this.isValidWallInteraction &&
                                this.enableWallSlides &&
                                !IsWallSticking &&
                                !IsOnCorner)
                            {
                                if (this.velocity.y <= 0 && (PressingIntoLeftWall() || PressingIntoRightWall()) && !IsGrounded)
                                {
                                    ChangeState(MotorState.WallSliding);
                                }
                            }

                            if (IsWallSliding)
                            {
                                if (this.velocity.y != -this.wallSlideSpeed)
                                {
                                    if (this.timeToWallSlideSpeed != 0)
                                    {
                                        if (this.velocity.y > -this.wallSlideSpeed)
                                        {
                                            this.velocity = UnityEngine.Vector2.up * Accelerate(this.velocity.y, -this.wallSlideSpeed / this.timeToWallSlideSpeed, -this.wallSlideSpeed);
                                        }
                                        else
                                        {
                                            this.velocity = UnityEngine.Vector2.up * Decelerate(
                                                this.velocity.y,
                                                Mathf.Abs(this.wallSlideSpeed / this.timeToWallSlideSpeed),
                                                -this.wallSlideSpeed);
                                        }
                                    }
                                    else
                                    {
                                        this.velocity = UnityEngine.Vector2.down * this.wallSlideSpeed;
                                    }
                                }
                            }
                        }

                        protected virtual void HandleFalling()
                        {
                            if (this.isIgnoreGravity)
                            {
                                return;
                            }

                            if (IsInAir && !this.isIgnoreGravity)
                            {
                                // If we are falling fast then multiply the gravityMultiplier.
                                if (this.isFallFast)
                                {
                                    if (this.velocity.y == -this.fastFallSpeed)
                                    {
                                        return;
                                    }

                                    if (this.velocity.y > -this.fastFallSpeed)
                                    {
                                        this.velocity.y = Accelerate(
                                            this.velocity.y,
                                            this.fastFallGravityMultiplier * Physics2D.gravity.y,
                                            -this.fastFallSpeed);
                                    }
                                    else
                                    {
                                        this.velocity.y = Decelerate(
                                            this.velocity.y,
                                            Mathf.Abs(this.fastFallGravityMultiplier * Physics.gravity.y),
                                            -this.fastFallSpeed);
                                    }

                                    if (this.velocity.y <= 0)
                                    {
                                        ChangeState(MotorState.FallingFast);
                                    }
                                }
                                else
                                {
                                    if (this.dashState.gravityEnabledFrames < 0)
                                    {
                                        if (this.velocity.y == -this.fallSpeed)
                                        {
                                            return;
                                        }

                                        if (this.velocity.y > -this.fallSpeed)
                                        {
                                            this.velocity.y = Accelerate(
                                                this.velocity.y,
                                                this.gravityMultiplier * Physics2D.gravity.y,
                                                -this.fallSpeed);
                                        }
                                        else
                                        {
                                            this.velocity.y = Decelerate(
                                                this.velocity.y,
                                                Mathf.Abs(this.gravityMultiplier * Physics2D.gravity.y),
                                                -this.fallSpeed);
                                        }
                                    }

                                    if (this.velocity.y <= 0)
                                    {
                                        ChangeState(MotorState.Falling);
                                    }
                                }
                            }
                        }

                        protected virtual void ApplyMovement()
                        {
                            // Apply movement only if we're not ignoring it.
                            if (this.ignoreMovementFrames >= 0)
                            {
                                return;
                            }

                            float speed;
                            float maxSpeed;

                            // ladder check
                            if (IsOnLadder && IsUserHandled)
                            {
                                this.velocity.x = this.normalizedXMovement * this.ladderSpeed;
                                this.velocity.y = this.normalizedYMovement * this.ladderSpeed;
                            }
                            // PERF: Optimal math out the window in favor of ease of figuring out, can resolve later if a problem.
                            else if (Mathf.Abs(this.normalizedXMovement) > 0)
                            {
                                if (IsGrounded)
                                {
                                    UnityEngine.Vector2 moveDir = GetMovementDir(this.normalizedXMovement);

                                    if (IsForceSlipping && UnityEngine.Vector2.Dot(moveDir, GetDownSlopeDir()) <= NEAR_ZERO)
                                    {
                                        // Don't allow walking up a slope that we slide down.
                                        this.velocity = GetMovementDir(this.velocity.x) * this.velocity.magnitude;
                                        return;
                                    }

                                    GetSpeedAndMaxSpeedOnGround(out speed, out maxSpeed);

                                    if (this.timeToGroundSpeed > 0)
                                    {
                                        // If we're moving faster than our normalizedXMovement * groundSpeed then decelerate rather than
                                        // accelerate.
                                        //
                                        // Or if we are trying to move in the direction opposite of where we are facing.

                                        if (speed > 0 &&
                                            this.normalizedXMovement > 0 &&
                                            speed > this.normalizedXMovement * maxSpeed ||
                                            speed < 0 &&
                                            this.normalizedXMovement < 0 &&
                                            speed < this.normalizedXMovement * maxSpeed ||
                                            speed < 0 &&
                                            this.normalizedXMovement > 0 ||
                                            speed > 0 &&
                                            this.normalizedXMovement < 0)
                                        {
                                            float deceleration = (maxSpeed * maxSpeed) / (2 * this.groundStopDistance);

                                            if (this.isOnSlope && this.enableChangeSpeedOnSlopes)
                                            {
                                                Debug.Log("1");
                                                float factor = (this.speedMultiplierOnSlope * (1 - this.slopeNormal.y));

                                                if (moveDir.y > 0)
                                                {
                                                    deceleration /= factor;
                                                }
                                                else
                                                {
                                                    deceleration *= factor;
                                                }
                                            }

                                            speed = Decelerate(
                                                speed,
                                                deceleration,
                                                this.normalizedXMovement * maxSpeed);
                                        }
                                        else
                                        {
                                            float acceleration = this.normalizedXMovement * (maxSpeed / this.timeToGroundSpeed);

                                            if (this.isOnSlope && this.enableChangeSpeedOnSlopes)
                                            {
                                                Debug.Log("2");
                                                float factor = (this.speedMultiplierOnSlope * (1 - this.slopeNormal.y));

                                                if (moveDir.y < 0)
                                                {
                                                    acceleration /= factor;
                                                }
                                                else
                                                {
                                                    acceleration *= factor;
                                                }
                                            }

                                            speed = Accelerate(
                                                speed,
                                                acceleration,
                                                this.normalizedXMovement * maxSpeed);
                                        }
                                    }
                                    else
                                    {
                                        // We can overwrite y if we're on the ground, it's fine.
                                        speed = this.normalizedXMovement * maxSpeed;
                                    }

                                    this.velocity = GetMovementDir(speed) * Mathf.Abs(speed);
                                }
                                else if (this.canChangeDirectionInAir)
                                {
                                    // Air doesn't have to change how it represents speed.
                                    if (this.timeToAirSpeed > 0)
                                    {
                                        if (this.velocity.x > 0 &&
                                            this.normalizedXMovement > 0 &&
                                            this.velocity.x > this.normalizedXMovement * this.airSpeed ||
                                            this.velocity.x < 0 &&
                                            this.normalizedXMovement < 0 &&
                                            this.velocity.x < this.normalizedXMovement * this.airSpeed)
                                        {
                                            speed = Decelerate(
                                                this.velocity.x,
                                                (this.airSpeed * this.airSpeed) / (2 * this.airStopDistance),
                                                this.normalizedXMovement * this.airSpeed);
                                        }
                                        else
                                        {
                                            speed = Accelerate(
                                                this.velocity.x,
                                                this.normalizedXMovement * (this.airSpeed / this.timeToAirSpeed),
                                                this.normalizedXMovement * this.airSpeed);
                                        }
                                    }
                                    else
                                    {
                                        speed = this.normalizedXMovement * this.airSpeed;
                                    }

                                    this.velocity.x = speed;
                                }
                            }
                            else if (this.velocity.x != 0)
                            {
                                if (IsGrounded)
                                {
                                    if (!IsSlipping)
                                    {
                                        GetSpeedAndMaxSpeedOnGround(out speed, out maxSpeed);

                                        if (this.groundStopDistance > 0)
                                        {
                                            float deceleration = (this.groundSpeed * this.groundSpeed) / (2 * this.groundStopDistance);

                                            if (this.isOnSlope && this.enableChangeSpeedOnSlopes)
                                            {
                                                Debug.Log("3");
                                                float factor = (this.speedMultiplierOnSlope * (1 - this.slopeNormal.y));

                                                if (GetMovementDir(this.velocity.x).y > 0)
                                                {
                                                    deceleration /= factor;
                                                }
                                                else
                                                {
                                                    deceleration *= factor;
                                                }
                                            }

                                            speed = Decelerate(speed, deceleration, 0);
                                        }
                                        else
                                        {
                                            speed = 0;
                                        }

                                        this.velocity = GetMovementDir(speed) * Mathf.Abs(speed);
                                    }
                                }
                                else
                                {
                                    if (this.airStopDistance > 0)
                                    {
                                        speed = Decelerate(this.velocity.x, (this.airSpeed * this.airSpeed) / (2 * this.airStopDistance), 0);
                                    }
                                    else
                                    {
                                        speed = 0;
                                    }

                                    this.velocity.x = speed;
                                }
                            }

                            // These mean we can't progress forward. Either a wall or a slope
                            if (HasFlag(CollidedSurface.LeftWall) &&
                                this.velocity.x < 0 &&
                                this.collidedNormals[DIRECTION_LEFT] == UnityEngine.Vector2.right ||
                                HasFlag(CollidedSurface.RightWall) &&
                                this.velocity.x > 0 &&
                                this.collidedNormals[DIRECTION_RIGHT] == UnityEngine.Vector2.left)
                            {
                                this.velocity.x = 0;
                            }

                            if (IsGrounded &&
                                this.disallowedSlopeNormal != UnityEngine.Vector2.zero &&
                                (this.disallowedSlopeNormal.x < 0 && this.velocity.x > 0 ||
                                    this.disallowedSlopeNormal.x > 0 && this.velocity.x < 0) &&
                                Velocity.sqrMagnitude < this.minimumSpeedToMoveUpSlipperySlope * this.minimumSpeedToMoveUpSlipperySlope)
                            {
                                Velocity = UnityEngine.Vector2.zero;
                            }
                        }

                        protected virtual void GetSpeedAndMaxSpeedOnGround(out float speed, out float maxSpeed)
                        {
                            UnityEngine.Vector3 moveDir = GetMovementDir(this.velocity.x);

                            if (this.isOnSlope)
                            {
                                speed = Velocity.magnitude * Mathf.Sign(this.velocity.x);
                                UnityEngine.Vector3 slopeDir = GetDownSlopeDir();

                                if (IsForceSlipping && UnityEngine.Vector2.Dot(moveDir, slopeDir) > NEAR_ZERO)
                                {
                                    if (!this.enableChangeSpeedOnSlopes)
                                    {
                                        maxSpeed = this.isFallFast ? this.fastFallSpeed : this.fallSpeed;
                                    }
                                    else
                                    {
                                        maxSpeed = -slopeDir.y * (this.isFallFast ? this.fastFallSpeed : this.fallSpeed);
                                    }
                                }

                                else if (this.enableChangeSpeedOnSlopes)
                                {
                                    if (moveDir.y > 0)
                                    {
                                        maxSpeed = this.groundSpeed *
                                            UnityEngine.Vector3.Dot(UnityEngine.Vector3.right * Mathf.Sign(moveDir.x), moveDir) *
                                            this.speedMultiplierOnSlope;
                                    }
                                    else
                                    {
                                        maxSpeed = this.groundSpeed *
                                            (2f - UnityEngine.Vector3.Dot(UnityEngine.Vector3.right * Mathf.Sign(moveDir.x), moveDir) *
                                            this.speedMultiplierOnSlope);
                                    }
                                }
                                else
                                {
                                    maxSpeed = this.groundSpeed;
                                }
                            }
                            else
                            {
                                speed = this.velocity.x;
                                maxSpeed = this.groundSpeed;
                            }
                        }

                        protected virtual float Accelerate(float speed, float acceleration, float limit)
                        {
                            // acceleration can be negative or positive to note acceleration in that direction.
                            speed += acceleration * GetDeltaTime();

                            if (acceleration > 0)
                            {
                                if (speed > limit)
                                {
                                    speed = limit;
                                }
                            }
                            else
                            {
                                if (speed < limit)
                                {
                                    speed = limit;
                                }
                            }

                            return speed;
                        }

                        protected virtual float Decelerate(float speed, float deceleration, float limit)
                        {
                            // deceleration is always positive but assumed to take the velocity backwards.
                            if (speed < 0)
                            {
                                speed += deceleration * GetDeltaTime();

                                if (speed > limit)
                                {
                                    speed = limit;
                                }
                            }
                            else if (speed > 0)
                            {
                                speed -= deceleration * GetDeltaTime();

                                if (speed < limit)
                                {
                                    speed = limit;
                                }
                            }

                            return speed;
                        }

                        protected virtual void StartDash()
                        {
                            // Set facing now and it won't be set again during dash.
                            SetFacing();

                            if (!this.dashState.isDashWithDirection)
                            {
                                // We dash depending on our direction.
                                this.dashState.dashDir = this.isFacingLeft ? UnityEngine.Vector2.left : UnityEngine.Vector2.right;
                            }

                            this.dashState.distanceDashed = 0;
                            this.dashState.distanceCalculated = 0;
                            this.previousLoc = this.collider2D.bounds.center;

                            // This will begin the dash this frame.
                            this.dashState.dashingFrames = GetFrameCount(this.dashDuration) - 1;
                            this.dashState.isForce = false;

                            ChangeState(MotorState.Dashing);
                        }

                        protected virtual float GetDashSpeed()
                        {
                            float normalizedTime = (float)(GetFrameCount(this.dashDuration) - this.dashState.dashingFrames) / GetFrameCount(this.dashDuration);

                            float speed = this.dashDerivativeFunction(0, this.dashDistance, normalizedTime) / this.dashDuration;

                            // Some of the easing functions may result in infinity, we'll uh, lower our expectations and make it maxfloat.
                            // This will almost certainly be clamped.
                            if (float.IsNegativeInfinity(speed))
                            {
                                speed = float.MinValue;
                            }
                            else if (float.IsPositiveInfinity(speed))
                            {
                                speed = float.MaxValue;
                            }

                            return speed;
                        }

                        protected virtual void MovePosition(UnityEngine.Vector3 newPos)
                        {
                            if (newPos == this.collider2D.bounds.center)
                            {
                                this.previousLoc = this.collider2D.bounds.center;
                                return;
                            }

                            UnityEngine.Vector3 toNewPos = newPos - this.collider2D.bounds.center;
                            float distance = toNewPos.magnitude;

                            RaycastHit2D hit = GetClosestHit(this.collider2D.bounds.center, toNewPos / distance, distance);

                            this.previousLoc = this.collider2D.bounds.center;

                            if (hit.collider != null)
                            {
                                transform.position = this.toTransform + (UnityEngine.Vector3)hit.centroid + (UnityEngine.Vector3)hit.normal * this.minDistanceFromEnv;
                            }
                            else
                            {
                                transform.position = this.toTransform + newPos;
                            }

                            // at the end if there is a restricted area, isForce the motor inside
                            // TODO handle rotation, unrotate transform.position, check, rotate
                            if (IsRestricted())
                            {
                                UnityEngine.Vector2 pos;
                                pos.x = Mathf.Clamp(transform.position.x, this.restrictedAreaBL.x, this.restrictedAreaTR.x);
                                pos.y = Mathf.Clamp(transform.position.y, this.restrictedAreaBL.y, this.restrictedAreaTR.y);
                                transform.position = pos;
                            }
                        }

                        protected virtual UnityEngine.Vector3 GetMovementDir(float speed)
                        {
                            UnityEngine.Vector3 moveDir = UnityEngine.Vector3.zero;

                            float multiplier = Mathf.Sign(speed);

                            if (speed == 0)
                            {
                                multiplier = Mathf.Sign(this.normalizedXMovement);
                                speed = this.normalizedXMovement;
                            }

                            if (!this.isOnSlope &&
                                ((!HasFlag(CollidedSurface.SlopeLeft) && !HasFlag(CollidedSurface.SlopeRight)) ||
                                (this.isFacingLeft && HasFlag(CollidedSurface.SlopeRight)) ||
                                (!this.isFacingLeft && HasFlag(CollidedSurface.SlopeLeft)) ||
                                (UnityEngine.Vector3.Dot(UnityEngine.Vector3.up, this.slopeNormal) < this.dotAllowedForSlopes)))
                            {
                                return Mathf.Sign(speed) * UnityEngine.Vector3.right;
                            }

                            moveDir.x = multiplier * this.slopeNormal.y;

                            if (this.slopeNormal.x * speed > 0)
                            {
                                moveDir.y = -Mathf.Abs(this.slopeNormal.x);
                            }
                            else
                            {
                                moveDir.y = Mathf.Abs(this.slopeNormal.x);
                            }

                            return moveDir;
                        }

                        protected virtual UnityEngine.Vector3 GetDownSlopeDir()
                        {
                            if (!this.isOnSlope)
                            {
                                return UnityEngine.Vector3.zero;
                            }

                            UnityEngine.Vector3 downDir = UnityEngine.Vector3.zero;
                            downDir.x = Mathf.Sign(this.slopeNormal.x) * this.slopeNormal.y;
                            downDir.y = -Mathf.Abs(this.slopeNormal.x);

                            return downDir;
                        }

                        protected virtual void SetFacing()
                        {
                            if (this.normalizedXMovement < 0)
                            {
                                this.isFacingLeft = true;
                            }
                            else if (this.normalizedXMovement > 0)
                            {
                                this.isFacingLeft = false;
                            }
                        }

                        protected bool CheckIfAtCorner()
                        {
                            Bounds box = this.collider2D.bounds;

                            UnityEngine.Vector2 min = box.min;
                            UnityEngine.Vector2 max = box.max;

                            // New min y is always at the current max y.
                            min.y = max.y;
                            max.y += this.cornerDistanceCheck;

                            if (PressingIntoLeftWall())
                            {
                                max.x = min.x;
                                min.x -= this.cornerDistanceCheck;
                            }
                            else if (PressingIntoRightWall())
                            {
                                min.x = max.x;
                                max.x += this.cornerDistanceCheck;
                            }
                            else
                            {
                                return false;
                            }

                            Collider2D col = Physics2D.OverlapArea(min, max, this.collisionMask);

                            return (col == null);
                        }

                        protected virtual int GetNearbyHitsBox(
                            UnityEngine.Vector2 direction,
                            float distance)
                        {
                            int num = Physics2D.BoxCastNonAlloc(
                                this.collider2D.bounds.center,
                                this.collider2D.bounds.size,
                                0f,
                                direction,
                                RaycastPhysicsMotorComponent.hits,
                                distance,
                                this.collisionMask);

                            if (num <= RaycastPhysicsMotorComponent.hits.Length)
                            {
                                return num;
                            }

                            RaycastPhysicsMotorComponent.hits = new RaycastHit2D[(int)(INCREASE_ARRAY_SIZE_MULTIPLIER * num)];

                            num = Physics2D.BoxCastNonAlloc(
                                this.collider2D.bounds.center,
                                this.collider2D.bounds.size,
                                0f,
                                direction,
                                RaycastPhysicsMotorComponent.hits,
                                distance,
                                this.collisionMask);

                            return num;
                        }

                        protected virtual int GetNearbyHitsRay(
                            UnityEngine.Vector2 origin,
                            UnityEngine.Vector2 direction,
                            float distance)
                        {
                            int num = Physics2D.RaycastNonAlloc(
                                origin,
                                direction,
                                RaycastPhysicsMotorComponent.hits,
                                distance,
                                this.collisionMask);

                            if (num <= RaycastPhysicsMotorComponent.hits.Length)
                            {
                                return num;
                            }

                            RaycastPhysicsMotorComponent.hits = new RaycastHit2D[(int)(INCREASE_ARRAY_SIZE_MULTIPLIER * num)];

                            return Physics2D.RaycastNonAlloc(
                                origin,
                                direction,
                                 RaycastPhysicsMotorComponent.hits,
                                distance,
                                this.collisionMask);
                        }

                        protected virtual int GetOverlappingColliders()
                        {
                            int num = Physics2D.OverlapAreaNonAlloc(
                                this.collider2D.bounds.min + new UnityEngine.Vector3(CHECK_TOUCHING_TRIM, CHECK_TOUCHING_TRIM),
                                this.collider2D.bounds.max + new UnityEngine.Vector3(-CHECK_TOUCHING_TRIM, -CHECK_TOUCHING_TRIM),
                                RaycastPhysicsMotorComponent.overlappingColliders,
                                this.collisionMask);

                            if (num <= RaycastPhysicsMotorComponent.overlappingColliders.Length)
                            {
                                return num;
                            }

                            RaycastPhysicsMotorComponent.overlappingColliders = new Collider2D[(int)(INCREASE_ARRAY_SIZE_MULTIPLIER) * num];

                            return Physics2D.OverlapAreaNonAlloc(
                                this.collider2D.bounds.min + new UnityEngine.Vector3(CHECK_TOUCHING_TRIM, CHECK_TOUCHING_TRIM),
                                this.collider2D.bounds.max + new UnityEngine.Vector3(-CHECK_TOUCHING_TRIM, -CHECK_TOUCHING_TRIM),
                                RaycastPhysicsMotorComponent.overlappingColliders,
                                this.collisionMask);
                        }

                        protected virtual RaycastHit2D GetClosestHit(
                            UnityEngine.Vector2 origin,
                            UnityEngine.Vector3 direction,
                            float distance,
                            bool useBox = true,
                            bool checkWereTouching = false)
                        {
                            if (!this.enableOneWayPlatforms && this.isOneWayPlatformsAreWalls)
                            {
                                // This is much easier if we don't care about one way platforms.
                                if (useBox)
                                {
                                    return Physics2D.BoxCast(
                                        origin,
                                        this.collider2D.bounds.size,
                                        0f,
                                        direction,
                                        distance,
                                        this.collisionMask);
                                }

                                return Physics2D.Raycast(origin, direction, distance, this.collisionMask);
                            }

                            // For one way platforms, things get interesting!

                            int numOfHits;

                            if (useBox)
                            {
                                numOfHits = GetNearbyHitsBox(
                                    direction,
                                    distance);
                            }
                            else
                            {
                                numOfHits = GetNearbyHitsRay(
                                    origin,
                                    direction,
                                    distance);
                            }

                            RaycastHit2D closestHit = new RaycastHit2D();
                            float closeBy = float.MaxValue;

                            bool haveGotOverlapping = false;
                            int numOfNoDistanceHits = 0;

                            for (int i = 0; i < numOfHits; i++)
                            {
                                if (RaycastPhysicsMotorComponent.hits[i].collider.usedByEffector &&
                                    RaycastPhysicsMotorComponent.hits[i].collider.GetComponent<PlatformEffector2D>().useOneWay)
                                {
                                    // ignore OWP ?
                                    if (!this.isOneWayPlatformsAreWalls)
                                        continue;

                                    bool isTouching = false;

                                    if (!haveGotOverlapping)
                                    {
                                        numOfNoDistanceHits = GetOverlappingColliders();
                                        haveGotOverlapping = true;
                                    }

                                    for (int j = 0; j < numOfNoDistanceHits; j++)
                                    {
                                        if (RaycastPhysicsMotorComponent.overlappingColliders[j] == RaycastPhysicsMotorComponent.hits[i].collider)
                                        {
                                            isTouching = true;
                                            break;
                                        }
                                    }

                                    if (isTouching)
                                    {
                                        if (checkWereTouching && ((1 << RaycastPhysicsMotorComponent.hits[i].collider.gameObject.layer) & this.movingPlatformLayerMask) != 0)
                                        {
                                            // If it's a moving platform then we need to know if we were touching.
                                            RaycastPhysicsMovingPlatformMotorComponent mpMotor = RaycastPhysicsMotorComponent.hits[i].collider.GetComponent<RaycastPhysicsMovingPlatformMotorComponent>();
                                            UnityEngine.Vector3 curPos = mpMotor.transform.position;
                                            mpMotor.transform.position = mpMotor.PreviousPosition;
                                            bool wasTouching = false;

                                            numOfNoDistanceHits = GetOverlappingColliders();
                                            haveGotOverlapping = false;

                                            mpMotor.transform.position = curPos;

                                            for (int j = 0; j < numOfNoDistanceHits; j++)
                                            {
                                                if (RaycastPhysicsMotorComponent.overlappingColliders[j] == RaycastPhysicsMotorComponent.hits[i].collider)
                                                {
                                                    wasTouching = true;
                                                    break;
                                                }
                                            }

                                            if (wasTouching)
                                            {
                                                continue;
                                            }

                                        }
                                        else
                                        {
                                            continue;
                                        }
                                    }

                                    UnityEngine.Vector3 oneWayPlatformForward = RaycastPhysicsMotorComponent.hits[i].collider.transform.TransformDirection(UnityEngine.Vector3.up);
                                    float dot = 0;

                                    if (this.velocity != UnityEngine.Vector2.zero)
                                    {
                                        dot = UnityEngine.Vector3.Dot(
                                            oneWayPlatformForward,
                                            this.velocity);
                                    }
                                    else if (((1 << RaycastPhysicsMotorComponent.hits[i].collider.gameObject.layer) & this.movingPlatformLayerMask) != 0)
                                    {
                                        // If we aren't moving then it's interesting if it's a moving platform.
                                        RaycastPhysicsMovingPlatformMotorComponent mpMotor = RaycastPhysicsMotorComponent.hits[i].collider.GetComponent<RaycastPhysicsMovingPlatformMotorComponent>();

                                        if (this.movingPlatformState.platform != mpMotor)
                                        {
                                            // This might break for more complicated one way platform moving platforms but it'll have to do.
                                            if (mpMotor.Velocity != UnityEngine.Vector2.zero)
                                            {
                                                dot = UnityEngine.Vector3.Dot(oneWayPlatformForward, -Velocity);
                                            }
                                            else
                                            {
                                                UnityEngine.Vector2 toNewPlatformPos = mpMotor.Position - mpMotor.PreviousPosition;
                                                dot = UnityEngine.Vector3.Dot(oneWayPlatformForward, -toNewPlatformPos);
                                            }
                                        }
                                    }

                                    if (dot > NEAR_ZERO)
                                    {
                                        continue;
                                    }
                                }

                                UnityEngine.Vector3 toHit = this.collider2D.bounds.center - (UnityEngine.Vector3)RaycastPhysicsMotorComponent.hits[i].centroid;

                                if (toHit.sqrMagnitude < closeBy)
                                {
                                    closeBy = toHit.sqrMagnitude;
                                    closestHit = RaycastPhysicsMotorComponent.hits[i];
                                }
                            }

                            return closestHit;
                        }

                        protected virtual CollidedSurface CheckGround(float distance, bool forceDistance = false)
                        {
                            CollidedSurface surfaces = CollidedSurface.None;

                            RaycastHit2D closestHit = GetClosestHit(this.collider2D.bounds.center, UnityEngine.Vector3.down, distance);

                            this.collidersUpAgainst[DIRECTION_DOWN] = closestHit.collider;
                            this.collidedNormals[DIRECTION_DOWN] = closestHit.normal;

                            if (closestHit.collider != null)
                            {
                                surfaces |= CollidedSurface.Ground;

                                if (IsUserHandled)
                                {
                                    FreedomStateExit();
                                    DisableRestrictedArea();
                                }

                                if (this.collider2D.bounds.center.y - closestHit.centroid.y < this.minDistanceFromEnv || forceDistance)
                                {
                                    transform.position += (this.minDistanceFromEnv - (this.collider2D.bounds.center.y - closestHit.centroid.y)) * UnityEngine.Vector3.up;
                                }
                            }

                            return surfaces;
                        }

                        protected virtual CollidedSurface UpdateSurroundingStates(bool forceCheck)
                        {
                            CollidedSurface surfaces = CollidedSurface.None;

                            UnityEngine.Vector2 vecToCheck = this.velocity;

                            if (!forceCheck)
                            {
                                if (vecToCheck == UnityEngine.Vector2.zero)
                                {
                                    vecToCheck = UnityEngine.Vector3.right * this.normalizedXMovement;
                                }
                            }

                            RaycastHit2D closestHit;

                            // Left
                            if (forceCheck || -vecToCheck.x >= -NEAR_ZERO)
                            {
                                closestHit = GetClosestHit(this.collider2D.bounds.center, UnityEngine.Vector3.left, this.envCheckDistance);

                                this.collidersUpAgainst[DIRECTION_LEFT] = closestHit.collider;
                                this.collidedNormals[DIRECTION_LEFT] = closestHit.normal;

                                if (closestHit.collider != null)
                                {
                                    surfaces |= CollidedSurface.LeftWall;

                                    if (this.collider2D.bounds.center.x - closestHit.centroid.x < this.minDistanceFromEnv)
                                    {
                                        transform.position += (this.minDistanceFromEnv - (this.collider2D.bounds.center.x - closestHit.centroid.x)) * UnityEngine.Vector3.right;
                                    }
                                }

                            }

                            // Ceiling
                            if (forceCheck || vecToCheck.y >= -NEAR_ZERO)
                            {
                                closestHit = GetClosestHit(this.collider2D.bounds.center, UnityEngine.Vector3.up, this.envCheckDistance);

                                this.collidersUpAgainst[DIRECTION_UP] = closestHit.collider;
                                this.collidedNormals[DIRECTION_UP] = closestHit.normal;

                                if (closestHit.collider != null)
                                {
                                    surfaces |= CollidedSurface.Ceiling;

                                    if (closestHit.centroid.y - this.collider2D.bounds.center.y < this.minDistanceFromEnv)
                                    {
                                        transform.position += (this.minDistanceFromEnv - (closestHit.centroid.y - this.collider2D.bounds.center.y)) * UnityEngine.Vector3.down;
                                    }
                                }
                            }

                            if (forceCheck || vecToCheck.x >= -NEAR_ZERO)
                            {
                                // Right
                                closestHit = GetClosestHit(this.collider2D.bounds.center, UnityEngine.Vector3.right, this.envCheckDistance);

                                this.collidersUpAgainst[DIRECTION_RIGHT] = closestHit.collider;
                                this.collidedNormals[DIRECTION_RIGHT] = closestHit.normal;

                                if (closestHit.collider != null)
                                {
                                    surfaces |= CollidedSurface.RightWall;

                                    if (closestHit.centroid.x - this.collider2D.bounds.center.x < this.minDistanceFromEnv)
                                    {
                                        transform.position += (this.minDistanceFromEnv - (closestHit.centroid.x - this.collider2D.bounds.center.x)) * UnityEngine.Vector3.left;
                                    }
                                }
                            }

                            if (forceCheck ||
                                -vecToCheck.y >= -NEAR_ZERO ||
                                this.isOnSlope ||
                                (HasFlag(CollidedSurface.Ground) && IsJumping))
                            {
                                // Ground
                                surfaces |= CheckGround(this.envCheckDistance);

                                if (this.enableSlopes &&
                                    this.enableStickOnGround &&
                                    (IsOnGround || IsSlipping) &&
                                    surfaces == CollidedSurface.None)
                                {
                                    surfaces |= CheckGround(this.distanceToCheckToStick, true);
                                }
                            }

                            this.isOnSlope = false;

                            if (this.enableSlopes)
                            {
                                // Slopes check
                                if ((surfaces & (CollidedSurface.Ground | CollidedSurface.RightWall | CollidedSurface.LeftWall)) != CollidedSurface.None)
                                {
                                    // We only check for slopes if we are on the ground or colliding with left/right wall
                                    UnityEngine.Vector2 dir = this.bottomRight;
                                    UnityEngine.Vector2 origin = new UnityEngine.Vector2(this.collider2D.bounds.max.x, this.collider2D.bounds.min.y);
                                    UnityEngine.Vector2 rightNormal = UnityEngine.Vector2.zero;
                                    UnityEngine.Vector2 leftNormal = UnityEngine.Vector2.zero;
                                    this.slopeNormal = UnityEngine.Vector2.up;

                                    closestHit = GetClosestHit(origin, dir, this._cornerDistanceCheck, false);

                                    if (closestHit.collider != null && closestHit.normal.x < -NEAR_ZERO && Mathf.Abs(closestHit.normal.y) > NEAR_ZERO)
                                    {
                                        surfaces |= CollidedSurface.SlopeRight;
                                        rightNormal = closestHit.normal;
                                    }

                                    dir.x *= -1;
                                    origin = this.collider2D.bounds.min;
                                    closestHit = GetClosestHit(origin, dir, this._cornerDistanceCheck, false);

                                    if (closestHit.collider != null && closestHit.normal.x > NEAR_ZERO && Mathf.Abs(closestHit.normal.y) > NEAR_ZERO)
                                    {
                                        surfaces |= CollidedSurface.SlopeLeft;
                                        leftNormal = closestHit.normal;
                                    }

                                    if ((surfaces & CollidedSurface.SlopeLeft) != CollidedSurface.None &&
                                        (surfaces & CollidedSurface.SlopeRight) != CollidedSurface.None)
                                    {
                                        // Both sides are sloping if we can stand on a slope then we consider the least steep slope.
                                        float leftDot = leftNormal.y;
                                        float rightDot = rightNormal.y;

                                        if (leftDot < this.dotAllowedForSlopes && rightDot < this.dotAllowedForSlopes)
                                        {
                                            // Would slip down both, consider just standing up.
                                            surfaces &= ~(CollidedSurface.SlopeLeft | CollidedSurface.SlopeRight);
                                            surfaces |= CollidedSurface.Ground;
                                            this.collidedNormals[DIRECTION_DOWN] = UnityEngine.Vector2.up;
                                        }
                                        else if (leftDot >= this.dotAllowedForSlopes && rightDot >= this.dotAllowedForSlopes)
                                        {
                                            this.isOnSlope = true;

                                            if (this.isFacingLeft)
                                            {
                                                this.slopeNormal = leftNormal;
                                                surfaces &= ~CollidedSurface.SlopeRight;
                                            }
                                            else
                                            {
                                                this.slopeNormal = rightNormal;
                                                surfaces &= ~CollidedSurface.SlopeLeft;
                                            }
                                        }
                                        else if (leftDot >= this.dotAllowedForSlopes)
                                        {
                                            this.isOnSlope = true;
                                            this.slopeNormal = leftNormal;
                                            surfaces &= ~CollidedSurface.SlopeRight;
                                        }
                                        else
                                        {
                                            this.isOnSlope = true;
                                            this.slopeNormal = rightNormal;
                                            surfaces &= ~CollidedSurface.SlopeLeft;
                                        }
                                    }
                                    else if ((surfaces & CollidedSurface.SlopeRight) != CollidedSurface.None)
                                    {
                                        this.slopeNormal = rightNormal;
                                        this.isOnSlope = !((surfaces & CollidedSurface.Ground) != CollidedSurface.None && (this.collidedNormals[DIRECTION_DOWN] == UnityEngine.Vector2.up));
                                    }
                                    else if ((surfaces & CollidedSurface.SlopeLeft) != CollidedSurface.None)
                                    {
                                        this.slopeNormal = leftNormal;
                                        this.isOnSlope = !((surfaces & CollidedSurface.Ground) != CollidedSurface.None && (this.collidedNormals[DIRECTION_DOWN] == UnityEngine.Vector2.up));
                                    }
                                }
                            }

                            return surfaces;
                        }

                        protected virtual float CalculateSpeedNeeded(float height)
                        {
                            return Mathf.Sqrt(-2 * height * this.gravityMultiplier * Physics2D.gravity.y);
                        }

                        protected virtual int GetFrameCount(float time)
                        {
                            return Mathf.RoundToInt(FrameService.GetFrameCount(time) / (TimeScale != 0 ? TimeScale : this.savedTimeScale));
                        }

                        protected virtual void OnDrawGizmosSelected()
                        {
                            UnityEngine.Gizmos.color = Color.yellow;
                            UnityEngine.Gizmos.DrawWireCube(this.ladderTopArea.center, this.ladderTopArea.size);
                            UnityEngine.Gizmos.DrawWireCube(this.ladderBottomArea.center, this.ladderBottomArea.size);

                            UnityEngine.Gizmos.color = Color.red;
                            UnityEngine.Gizmos.DrawWireCube(this.restrictedArea.center, this.restrictedArea.size);

                            UnityEngine.Gizmos.color = Color.white;

                            // Ground check.
                            Bounds box = GetComponent<Collider2D>().bounds;
                            UnityEngine.Vector2 min;
                            UnityEngine.Vector2 max;

                            // Ground check box
                            min = box.min;
                            max = box.max;
                            min.y -= this.envCheckDistance;
                            max.y = box.min.y;
                            UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                            // Left check box
                            min = box.min;
                            max = box.max;
                            min.x -= this.envCheckDistance;
                            max.x = box.min.x;
                            UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                            // Right check box
                            min = box.min;
                            max = box.max;
                            min.x = box.max.x;
                            max.x += this.envCheckDistance;
                            UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                            if (this.enableCornerGrabs)
                            {
                                min = box.min;
                                max = box.max;
                                min.y = max.y;
                                max.y += this.cornerDistanceCheck;
                                max.x = min.x;
                                min.x -= this.cornerDistanceCheck;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                                min = box.min;
                                max = box.max;
                                min.y = max.y;
                                max.y += this.cornerDistanceCheck;
                                min.x = max.x;
                                max.x += this.cornerDistanceCheck;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                                // Draw valid corner grab area
                                UnityEngine.Gizmos.color = Color.yellow;
                                min = box.min;
                                max = box.max;
                                min.y = max.y - box.size.y * this.normalizedValidWallInteraction;
                                min.x = max.x;
                                max.x += this.cornerDistanceCheck;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));

                                min = box.min;
                                max = box.max;
                                min.y = max.y - box.size.y * this.normalizedValidWallInteraction;
                                max.x = min.x;
                                min.x -= this.cornerDistanceCheck;
                                UnityEngine.Gizmos.DrawWireCube(new UnityEngine.Vector2((min.x + max.x) / 2, (min.y + max.y) / 2), new UnityEngine.Vector2(max.x - min.x, min.y - max.y));
                            }

                            // Show the distance that it will take for the motor to stop on the ground and air.
                            UnityEngine.Vector2 from = new UnityEngine.Vector2(box.max.x, box.min.y);
                            UnityEngine.Gizmos.color = Color.green;
                            UnityEngine.Gizmos.DrawLine(from, from + UnityEngine.Vector2.right * this.groundStopDistance);

                            from = box.max;
                            UnityEngine.Gizmos.color = Color.blue;
                            UnityEngine.Gizmos.DrawLine(from, from + UnityEngine.Vector2.right * this.airStopDistance);

                            if (this.enableMovingPlatformDebug)
                            {
                                UnityEngine.Gizmos.color = Color.red;
                                UnityEngine.Gizmos.DrawSphere(this.point, 0.01f);
                                UnityEngine.Gizmos.DrawWireCube(this.prevPosPlat.center, this.prevPosPlat.size);

                                UnityEngine.Gizmos.color = Color.blue;
                                UnityEngine.Gizmos.DrawWireCube(this.startPosMotor.center, this.startPosMotor.size);

                                UnityEngine.Gizmos.color = Color.green;
                                UnityEngine.Gizmos.DrawSphere(this.point2, 0.01f);
                                UnityEngine.Gizmos.DrawWireCube(this.movedPosMotor.center, this.movedPosMotor.size);
                            }

                            if (this.enableIterationDebug && this.iterationBounds != null)
                            {
                                UnityEngine.Gizmos.color = Color.red;
                                UnityEngine.Gizmos.DrawWireCube(this.iterationBounds[0].center, this.iterationBounds[0].size);

                                UnityEngine.Gizmos.color = Color.blue;

                                for (int i = 2; i < this.iterationsUsed + 2; i++)
                                {
                                    UnityEngine.Gizmos.DrawWireCube(this.iterationBounds[i].center, this.iterationBounds[i].size);
                                }

                                UnityEngine.Gizmos.color = Color.green;
                                UnityEngine.Gizmos.DrawWireCube(this.iterationBounds[1].center, this.iterationBounds[1].size);
                            }
                        }

                        protected virtual void ChangeState(MotorState newState)
                        {
                            // no change...
                            if (this.currentMotorState == newState)
                            {
                                return;
                            }

                            if (IsSlipping)
                            {
                                if (this.OnSlippingEnd != null)
                                {
                                    this.OnSlippingEnd();
                                }
                            }
                            if (IsJumping)
                            {
                                if (this.OnJumpEnd != null)
                                {
                                    this.OnJumpEnd();
                                }
                            }
                            if (IsDashing)
                            {
                                if (this.OnDashEnd != null)
                                {
                                    this.OnDashEnd();
                                }
                            }

                            // set
                            this.currentMotorState = newState;

                            if (IsSlipping)
                            {
                                if (this.OnSlipping != null)
                                {
                                    this.OnSlipping();
                                }
                            }
                            if (IsJumping)
                            {
                                if (this.OnJump != null)
                                {
                                    this.OnJump();
                                }
                            }
                            if (IsDashing)
                            {
                                if (this.OnDash != null)
                                {
                                    this.OnDash();
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
        }
    }
}