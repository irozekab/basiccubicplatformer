﻿using Kameosa.Enumerables;
using Kameosa.GameObject.Gameplay2D.Platformer.Common;
using UnityEngine;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Joystick
                {
                    [RequireComponent(typeof(RaycastPhysicsMotorComponent))]
                    public class RaycastPhysicsJoystickComponent : MonoBehaviour//Kameosa.GameObject.Gameplay2D.Common.Joystick.BaseJoystickComponent
                    {
                        [SerializeField]
                        private RaycastPhysicsMotorComponent raycastPhysicsMotorComponent;

                        private bool isRestored = true;
                        private bool enableOneWayPlatforms;
                        private bool isOneWayPlatformsAreWalls;
                        private UnityEngine.Vector2 input;

                        #region properties
                        public UnityEngine.Vector2 Input
                        {
                            get
                            {
                                return this.input;
                            }

                            set
                            {
                                this.input = value;
                            }
                        }
                        #endregion

                        //protected override void Awake()
                        void Awake()
                        {
                            //base.Awake();

                            if (this.raycastPhysicsMotorComponent == null)
                            {
                                this.raycastPhysicsMotorComponent = GetComponent<RaycastPhysicsMotorComponent>();
                            }
                        }

                        // Update is called once per frame
                        void Update()
                        {
                            this.input = new UnityEngine.Vector2(UnityEngine.Input.GetAxis(Kameosa.Constants.Input.HORIZONTAL), UnityEngine.Input.GetAxis((Kameosa.Constants.Input.VERTICAL)));

                            // use last state to restore some ladder specific values
                            if (this.raycastPhysicsMotorComponent.CurrentMotorState != RaycastPhysicsMotorComponent.MotorState.FreedomState)
                            {
                                // try to restore, sometimes states are a bit messy because change too much in one frame
                                FreedomStateRestore(this.raycastPhysicsMotorComponent);
                            }

                            // Jump?
                            // If you want to jump in ladders, leave it here, otherwise move it down
                            if (UnityEngine.Input.GetButtonDown(Kameosa.Constants.Input.JUMP))
                            {
                                this.raycastPhysicsMotorComponent.Jump();
                                this.raycastPhysicsMotorComponent.DisableRestrictedArea();
                            }

                            this.raycastPhysicsMotorComponent.IsJumpingHeld = UnityEngine.Input.GetButton(Kameosa.Constants.Input.JUMP);

                            // XY freedom movement
                            if (this.raycastPhysicsMotorComponent.CurrentMotorState == RaycastPhysicsMotorComponent.MotorState.FreedomState)
                            {
                                this.raycastPhysicsMotorComponent.NormalizedXMovement = this.input.x;
                                this.raycastPhysicsMotorComponent.NormalizedYMovement = this.input.y;

                                return; // do nothing more
                            }

                            // X axis movement
                            if (UnityEngine.Input.GetAxis(Kameosa.Constants.Input.HORIZONTAL) != 0)
                            {
                                this.raycastPhysicsMotorComponent.NormalizedXMovement = this.input.x;
                            }
                            else
                            {
                                this.raycastPhysicsMotorComponent.NormalizedXMovement = 0;
                            }

                            if (UnityEngine.Input.GetAxis(Kameosa.Constants.Input.VERTICAL) != 0)
                            {
                                bool up_pressed = UnityEngine.Input.GetAxis(Kameosa.Constants.Input.VERTICAL) > 0;
                                if (this.raycastPhysicsMotorComponent.IsOnLadder)
                                {
                                    if ((up_pressed && this.raycastPhysicsMotorComponent.CurrentLadderZone == RaycastPhysicsMotorComponent.LadderZone.Top) || (!up_pressed && this.raycastPhysicsMotorComponent.CurrentLadderZone == RaycastPhysicsMotorComponent.LadderZone.Bottom))
                                    {
                                        // do nothing!
                                    }
                                    // if player hit up, while on the top do not enter in freeMode or a nasty short jump occurs
                                    else
                                    {
                                        // example ladder behaviour

                                        this.raycastPhysicsMotorComponent.FreedomStateEnter(); // enter freedomState to disable gravity
                                        this.raycastPhysicsMotorComponent.EnableRestrictedArea();  // movements is retricted to a specific sprite bounds

                                        // now disable OWP completely in a "trasactional way"
                                        FreedomStateSave(this.raycastPhysicsMotorComponent);
                                        this.raycastPhysicsMotorComponent.enableOneWayPlatforms = false;
                                        this.raycastPhysicsMotorComponent.isOneWayPlatformsAreWalls = false;

                                        // start XY movement
                                        this.raycastPhysicsMotorComponent.NormalizedXMovement = this.input.x;
                                        this.raycastPhysicsMotorComponent.NormalizedYMovement = this.input.y;
                                    }
                                }
                            }

                            if (UnityEngine.Input.GetButtonDown(Kameosa.Constants.Input.DASH))
                            {
                                this.raycastPhysicsMotorComponent.Dash();
                            }
                        }

                        // before enter en freedom state for ladders
                        void FreedomStateSave(RaycastPhysicsMotorComponent raycastPhysicsMotorComponent)
                        { 
                            // do not enter twice
                            if (!this.isRestored)
                            {
                                return;
                            }

                            this.isRestored = false;
                            this.enableOneWayPlatforms = this.raycastPhysicsMotorComponent.enableOneWayPlatforms;
                            this.isOneWayPlatformsAreWalls = this.raycastPhysicsMotorComponent.isOneWayPlatformsAreWalls;
                        }
                        // after leave freedom state for ladders
                        void FreedomStateRestore(RaycastPhysicsMotorComponent raycastPhysicsMotorComponent)
                        {
                             // do not enter twice
                            if (this.isRestored)
                            {
                                return;
                            }

                            this.isRestored = true;
                            this.raycastPhysicsMotorComponent.enableOneWayPlatforms = this.enableOneWayPlatforms;
                            this.raycastPhysicsMotorComponent.isOneWayPlatformsAreWalls = this.isOneWayPlatformsAreWalls;
                        }

                    }
                    ////[SerializeField]
                    ////private bool isFlipXScaleOnLeft = false;

                    ////[SerializeField]
                    ////private bool isSetAnimatorVerticalVelocity = false;

                    //protected override void Start()
                    //{
                    //}

                    //private void Update()
                    //{
                    //    if (this.canMove)
                    //    {
                    //        UnityEngine.Vector2 input = new UnityEngine.Vector2(UnityEngine.Input.GetAxis("Horizontal"), UnityEngine.Input.GetAxis("Vertical"));
                    //        this.raycastPhysicsMotorComponent.Input = input;

                    //        if (this.canJump && UnityEngine.Input.GetButtonDown("Jump"))
                    //        {
                    //            this.raycastPhysicsMotorComponent.Jump();
                    //        }
                    //        else if (this.canJump && UnityEngine.Input.GetButtonUp("Jump"))
                    //        {
                    //            this.raycastPhysicsMotorComponent.StopJump();
                    //        }
                    //    }
                    //    //    if (this.isSetAnimatorVerticalVelocity)
                    //    //    {
                    //    //        this.animator.SetFloat("verticalVelocity", this.rigidbody2D.velocity.y);
                    //    //    }
                    //}

                    //protected override void Initialize()
                    //{
                    //}

                    ////private void SetFaceDirection(float horizontalInput)
                    ////{
                    ////    if (horizontalInput == 0)
                    ////    {
                    ////        return;
                    ////    }

                    ////    this.faceDirection = horizontalInput > 0 ? RelativeDirection.Right : RelativeDirection.Left;

                    ////    if (this.isFlipXScaleOnLeft)
                    ////    {
                    ////        this.spriteRenderer.flipX = this.faceDirection == RelativeDirection.Left;
                    ////    }
                    ////}
                }
            }
        }
    }
}